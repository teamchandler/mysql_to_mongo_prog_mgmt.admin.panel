'use strict';

/**
 * @ngdoc overview
 * @name annectosadminApp
 * @description
 * # annectosadminApp
 *
 * Main module of the application.
 */
//
//angular.module('annectosadmin', ['ngDialog']);

angular.module('annectosadmin', [
//    ''ngDialog'',
//    'ngRoute',
//    'ngDialog',
    'LocalStorageModule',
    'ui.router',
    'ui.bootstrap'
])


    .config( function ($stateProvider, $urlRouterProvider) {

        $urlRouterProvider.otherwise('/login/login');

        $stateProvider
            .state('login', {
                url: "/login",
                abstract: true,
                templateUrl: "views/login_base.html",
                controller : "LoginBaseCtrl"
            })
            .state('login.login', {
                url: '/login',
                templateUrl: 'views/login_login.html',
                controller: 'login_loginCtrl',
                data: {
                    "header_label" : "Please Sign In"
                }

            })

            .state('main', {
                url: "/main",
                abstract: true,
                templateUrl: "views/main_base.html",
                controller : "MainBaseCtrl"
            })

            .state('main.home', {
                url: '/home',
                templateUrl: 'views/customer_main.html',
                controller: 'customerMainController',
                data: {
                    "header_label" : "Customer Maintenance"
                }
            })

            .state('main.dashboard', {
                url: '/dashboard',
                templateUrl: 'views/Dashboard.html',
                controller: '',
                data: {
                    "header_label" : "Dashboard"
                }
            })

            .state('main.category_maintenance', {
                url: '/category_maintenance',
                templateUrl: 'views/category_maintenance.html',
                controller: 'cat_maintenanceCtrl',
                data: {
                    "header_label" : "Category Maintenance"
                }
            })

            .state('main.upload_img_url', {
                url: '/upload_img_url',
                templateUrl: 'views/upload_img_url.html',
                controller: 'cat_maintenanceCtrl',
                data: {
                    "header_label" : "Upload Image URL"
                }
            })

            .state('main.prod_cat_association', {
                url: '/prod_cat_association',
                templateUrl: 'views/prod_cat_association.html',
                controller: 'prod_cat_assocCtrl',
                data: {
                    "header_label" : "Product Category Association"
                }
            })

            .state('main.cat_filter', {
                url: '/cat_filter',
                templateUrl: 'views/cat_filter.html',
                controller: 'catfilterCtrl',
                data: {
                    "header_label" : "Cat Filter"
                }
            })
            .state('main.prod_maintenance', {
                url: '/prod_maintenance',
                templateUrl: 'views/prod_maintenance.html',
                controller: 'prod_mainCtrl',
                data: {
                    "header_label" : "Product Maintenance"
                }
            })

            .state('main.child_prod_main', {
                url: '/child_prod_main',
                templateUrl: 'views/child_prod_main.html',
                controller: 'child_prod_mainCtrl',
                data: {
                    "header_label" : "Child Product Maintenance"
                }
            })

            .state('main.edit_child_prod', {
                url: '/edit_child_prod',
                templateUrl: 'views/child_prod_popup.html',
                controller: 'child_prod_mainCtrl',
                data: {
                    "header_label" : "Edit Child Product"
                }
            })
            .state('main.upload', {
                url: '/upload/{category_id},{category_name}',
                templateUrl: 'views/upld_img_vdo.html',
                controller: 'upload_imgCtrl',
                data: {
                    "header_label" : "Upload Image Video URL"
                }
            })
            .state('main.prod_upload', {
                url: '/prod_upload/{product_id},{product_name}',
                templateUrl: 'views/upld_img_vdo_prod.html',
                controller: 'upload_img_prodCtrl',
                data: {
                    "header_label" : "Upload Image URL"
                }
            })
            .state('main.prod_feature', {
                url: '/prod_feature',
                templateUrl: 'views/prod_feat_main.html',
                controller: 'productfeaturemgmntCtrl',
                data: {
                    "header_label" : "Product Feature Management"
                }
            })

            .state('main.brand', {
                url: '/brand',
                templateUrl: 'views/brand.html',
                controller: 'brandCtrl',
                data: {
                    "header_label" : "Brand"
                }
            })

//            .state('main.brand', {
//                url: '/brand',
//                templateUrl: 'views/brand.html',
//                controller: 'brandCtrl'
//
//            })
//            .state('main.category', {
//                url: '/category',
//                templateUrl: 'views/category.html',
//                controller: 'categoryctrl'
//
//            })
//            .state('main.product', {
//                url: '/product',
//                templateUrl: 'views/prod_maintain.html',
//                controller: 'prod_main_ctrl'
//
//            })
//            .state('main.child-product', {
//                url: '/child-product',
//                templateUrl: 'views/Child_Prod_Main.html',
//                controller: 'child_prod_main_ctrl'
//
//            })
//            .state('main.company', {
//                url: '/company',
//                templateUrl: 'views/Customer_Main.html',
//                controller: 'customer_main_ctrl'
//
//            })
//
//            .state('main.catMgmt', {
//                url: '/catMgmt',
//                templateUrl: 'views/catMgmt.html',
//                controller: 'catMgmtCtrl'
//
//            })
//
//            .state('main.prodmgmt', {
//                url: '/prodmgmt',
//                templateUrl: 'views/prodmgmt.html',
//                controller: 'prodmgmtCtrl'
//
//            })
//            .state('main.childprodmain', {
//                url: '/childprodmain',
//                templateUrl: 'views/childprodmain.html',
//                controller: 'childprodmainCtrl'
//
//            })
//
//            .state('main.prodcatassoc', {
//                url: '/prodcatassoc',
//                templateUrl: 'views/prodcatassoc.html',
//                controller: 'prodcatassocCtrl'
//
//            })
//
//
//            .state('main.productfeaturemgmnt', {
//                url: '/productfeaturemgmnt',
//                templateUrl: 'views/productfeaturemgmnt.html',
//                controller: 'productfeaturemgmntCtrl'
//
//            })
//            .state('main.dealsetup', {
//                url: '/dealsetup',
//                templateUrl: 'views/dealsetup.html',
//                controller: 'dealsetupCtrl'
//
//            })
//
//
//
            .state('main.orderview', {
                url: '/orderview',
                templateUrl: 'views/ordermanagement.html',
                controller: 'ordermanagementCtrl',
                data: {
                    "header_label" : "Order View"
                }

            })

            .state('main.viewOrderDetail', {
                url: '/viewOrderDetail/{order_id}',
                templateUrl: 'views/viewOrderDetail.html',
                controller: 'orderDetailCtrl',
                data: {
                    "header_label" : "Order Detail"
                }

            })



//            .state('main.viewOrderDetail', {
//                url: '/viewOrderDetail',
//                templateUrl: 'views/viewOrderDetail.html',
//                controller: 'orderDetailCtrl',
//                data: {
//                    "header_label" : "Dashboard"
//                }
//
//            })
//
//            .state('main.catfilter', {
//                url: '/catfilter',
//                templateUrl: 'views/catfilter.html',
//                controller: 'catfilterCtrl'
//
//            })
//
//
            .state('main.ordertracking', {
                url: '/ordertracking',
                templateUrl: 'views/ordertracking.html',
                controller: 'ordertrackingCtrl',
                data: {
                    "header_label" : "Order Tracking"
                }

            })

            .state('main.promo_mgmt', {
                url: '/promo_mgmt',
                templateUrl: 'views/promo_mgmt.html',
                controller: 'promo_mgmtCtrl',
                data: {
                    "header_label" : "Promotion Management"
                }

            })
//
//
//
//            .state('main.productapproval', {
//                url: '/productapproval',
//                templateUrl: 'views/productapproval.html',
//                controller: 'productapprovalCtrl'
//
//            })
//            .state('store-catalog', {
//                url: '/store-catalog',
//                templateUrl: 'views/store_catalog.html',
//                controller: ''
//
//            })
//            .state('main.cart', {
//                url: '/cart',
//                templateUrl: 'views/cart.html',
//                controller: 'CartCtrl',
//                data: {
//                    "header_label" : "Cart"
//                }
//            })
//            .state('main.cat', {
//                url: '/cat/:catid',
//                templateUrl: 'views/cat.html',
//                controller: 'CatCtrl',
//                data: {
//                    "header_label" : "Product List"
//                }
//            })
//
//            .state('main.cat1', {
//                url: '/cat1/:cat',
//                templateUrl: 'views/cat1.html',
//                controller: 'Cat1Ctrl',
//                data: {
//                    "header_label" : "Product List"
//                }
//            })
//
//            .state('main.prod', {
//                url: '/prod/:prodid',
//                templateUrl: 'views/prod.html',
//                controller: 'ProdCtrl',
//                data: {
//                    "header_label" : "Product List"
//                }
//            })
//
//
//
//
//            .state('main.enq', {
//                url: '/enq/:product_id',
//                templateUrl: 'views/enq.html',
//                controller: 'enqCtrl',
//                data: {
//                    "header_label" : "Enquire Now Details"
//                }
//            })
//            .state('main.ship', {
//                url: '/ship',
//                templateUrl: 'views/ship.html',
//                controller: 'shipCtrl',
//                data: {
//                    "header_label" : "Shipment Details"
//                }
//            })
//
//            .state('main.payment', {
//                url: '/payment/:order_id',
//                templateUrl: 'views/pp.html',
//                controller: 'ppController',
//                data: {
//                    "header_label" : "Payment Details"
//                }
//            })
//
//            .state('main.checkout', {
//                url: '/checkout/:order_id',
//                templateUrl: 'views/cc.html',
//                controller: 'checkCtrl',
//                data: {
//                    "header_label" : "Checkout "
//                }
//            })
//
//            .state('main.payment_details',
//            {
//                url:'/payment_details/:order_id',
//                templateUrl: 'views/cp.html',
//                controller: 'check_popupController',
//                data: {
//                    "header_label" : "Payment Details "
//                }
//
//            })
//
//            .state('main.bank_details',
//            {
//                url:'/bank_details/:order_id',
//                templateUrl: 'views/bank_details.html',
//                controller: 'bank_popupController',
//                data: {
//                    "header_label" : "Bank Details "
//                }
//
//            })
//
//
//            .state('main.contactus', {
//                url: '/contactus',
//                templateUrl: 'views/contactus.html',
//                controller: 'contactusCtrl',
//                data: {
//                    "header_label" : "Contact Us Details"
//                }
//            })
//
//
//            .state('main.myorder', {
//                url: '/myorder',
//                templateUrl: 'views/myorder.html',
//                controller: 'MyorderCtrl',
//                data: {
//                    "header_label" : "My Order"
//                }
//            })
//
//            .state('main.Store-Config', {
//                url: '/Store-Config',
//                templateUrl: 'views/store_config.html',
//                controller: 'str_cnfg_ctrl'
//
//            })
//
            .state('main.e_gift_voucher', {
                url: '/e_gift_voucher',
                templateUrl: 'views/e_gift_voucher.html',
                controller: 'e_gift_voucherCtrl',
                data: {
                    "header_label" : "Dashboard"
                }

            })
            .state('main.manage_egift_voucher', {
                url: '/manage_egift_voucher',
                templateUrl: 'views/manage_egift_voucher.html',
                controller: 'manage_egift_voucherCtrl',
                data: {
                    "header_label" : "Dashboard"
                }

            })
            .state('main.e_gift_voucher_report', {
                url: '/e_gift_voucher_report',
                templateUrl: 'views/e_gift_voucher_report.html',
                controller: 'e_gift_voucher_reportCtrl',
                data: {
                    "header_label" : "Dashboard"
                }

            })
//            .state('main.Upload-Customer-Point', {
//                url: '/Upload-Customer-Point',
//                templateUrl: 'views/Upload_Customer_Point.html',
//                controller: ''
//
//            })
//            .state('main.Upload-Migrate-Store', {
//                url: '/Upload-Migrate-Store',
//                templateUrl: 'views/Upload_Migrate_Store.html',
//                controller: ''
//
//            })
            .state('main.cust_communication', {
                url: '/cust_communication',
                templateUrl: 'views/cust_communication.html',
                controller: 'custCommunicationCtrl',
                data: {
                    "header_label" : "Customer Communication"
                }

            })
//            .state('main.Promo-Management', {
//                url: '/Promo-Management',
//                templateUrl: 'views/Promo_Management.html',
//                controller: ''
//
//            })
            .state('main.assign_user_rights', {
                url: '/assign_user_rights',
                templateUrl: 'views/assign_user_rights.html',
                controller: 'assign_user_rightsCtrl',
                data: {
                    "header_label" : "Assign User Rights"
                }

            })
            .state('main.productapproval', {
                url: '/product_approval',
                templateUrl: 'views/productapproval.html',
                controller: 'product_approvalCtrl',
                data: {
                    "header_label" : "Product Approval"
                }

            })

            .state('main.search_product', {
                url: '/search_product',
                templateUrl: 'views/prod_search.html',
                controller: 'prod_mainCtrl',
                data: {
                    "header_label" : "Product Search"
                }

            });
//            .state('main.Daily-sales-report', {
//                url: '/Daily-sales-report',
//                templateUrl: 'views/daily_sales_wise_report.html',
//                controller: ''
//
//            })
//            .state('main.Customer-Points-Balance-Report', {
//                url: '/Customer-Points-Balance-Report',
//                templateUrl: 'views/customer_point_balance.html',
//                controller: ''
//
//            })
//            .state('main.Warehouse-details', {
//                url: '/Warehouse-details',
//                templateUrl: 'views/warehouse.html',
//                controller: ''
//
//            })




    });
