/**
 * Created by developer7 on 9/9/2014.
 */
'use strict';

angular.module('annectosadmin')
    .service('orderService', function orderService() {

        this.get_Order_History= function($http,$q,OrdStore,OrdTyp,txtOrdFrom,txtOrdTo){

            var apiPath = cat_service_url + '/category/admin/Order_History?OrderStore=' + OrdStore + '&OrderType=' + OrdTyp + '&OrderFrmDt=' + txtOrdFrom + '&OrderToDt=' + txtOrdTo + '&json=true'

            var deferred = $q.defer();
            $http({
                method: 'GET',
                url: apiPath,

                ContentType: 'application/json'

            }).success(function (data) {

                deferred.resolve(data);

            }).error(function (data) {
                deferred.reject("Data Error");


            })
            return deferred.promise;


        };


        this.getOrder_Export_To_Excel = function($http,$q,OrdStore,OrdTyp,txtOrdFrom,txtOrdTo){

            var apiPath = cat_service_url + '/category/admin/getOrder_Export_To_Excel?OrderStore=' + OrdStore + '&OrderType=' + OrdTyp + '&OrderFrmDt=' + txtOrdFrom + '&OrderToDt=' + txtOrdTo + '&json=true';

            var deferred = $q.defer();
            $http({
                method: 'GET',
                url: apiPath,

                ContentType: 'application/json'

            }).success(function (data) {

                deferred.resolve(data);

            }).error(function (data) {
                deferred.reject("Data Error");


            })
            return deferred.promise;


        };

        this.getOrderTrackRecord = function($http,$q,OrdStore,OrdTyp){
            var apiPath = cat_service_url + '/category/admin/getOrderTrackRecord?OrderStore=' + OrdStore + '&OrderType=' + OrdTyp + '&json=true';

            var deferred = $q.defer();
            $http({
                method: 'GET',
                url: apiPath,
                ContentType: 'application/json'
            }).success(function (data) {
                deferred.resolve(data);
            }).error(function (data) {
                deferred.reject("Data Error");
            })
            return deferred.promise;

        };

        this.getWillShipTrackReport = function($http,$q,OrdStore,OrdTyp){
            var apiPath = cat_service_url + '/category/admin/getWillShipTrackReport?OrderStore=' + OrdStore + '&OrderType=' + OrdTyp + '&json=true';

            var deferred = $q.defer();
            $http({
                method: 'GET',
                url: apiPath,
                ContentType: 'application/json'
            }).success(function (data) {
                deferred.resolve(data);
            }).error(function (data) {
                deferred.reject("Data Error");
            })
            return deferred.promise;
        };

        this.getSendTrackReport = function($http,$q,OrdStore,OrdTyp){
            var apiPath = cat_service_url + '/category/admin/getSendTrackReport?OrderStore=' + OrdStore + '&OrderType=' + OrdTyp + '&json=true';

            var deferred = $q.defer();
            $http({
                method: 'GET',
                url: apiPath,
                ContentType: 'application/json'
            }).success(function (data) {
                deferred.resolve(data);
            }).error(function (data) {
                deferred.reject("Data Error");
            })
            return deferred.promise;
        };

        this.getWillShipReport = function($http,$q,OrdStore,OrdTyp){
            var apiPath = cat_service_url + '/category/admin/getWillShipReport?OrderStore=' + OrdStore + '&OrderType=' + OrdTyp + '&json=true';

            var deferred = $q.defer();
            $http({
                method: 'GET',
                url: apiPath,
                ContentType: 'application/json'
            }).success(function (data) {
                deferred.resolve(data);
            }).error(function (data) {
                deferred.reject("Data Error");
            })
            return deferred.promise;
        };

        this.getWillShipReportAfterMaxShip = function($http,$q,OrdStore,OrdTyp){
            var apiPath = cat_service_url + '/category/admin/getWillShipReportAfterMaxShip?OrderStore=' + OrdStore + '&OrderType=' + OrdTyp + '&json=true';

            var deferred = $q.defer();
            $http({
                method: 'GET',
                url: apiPath,
                ContentType: 'application/json'
            }).success(function (data) {
                deferred.resolve(data);
            }).error(function (data) {
                deferred.reject("Data Error");
            })
            return deferred.promise;
        };


        this.get_order_data = function($http,$q,order_id){
            var apiPath = cat_service_url + '/category/admin/get_order_data/' + order_id ;

            var deferred = $q.defer();
            $http({
                method: 'GET',
                url: apiPath,
                ContentType: 'application/json'
            }).success(function (data) {
                deferred.resolve(data);
            }).error(function (data) {
                deferred.reject("Data Error");
            })
            return deferred.promise;
        };



        this.update_Order = function($http,$q,orderCourierInfo){
            var apiPath =  cat_service_url + '/category/update_order/';
            var deferred = $q.defer();
            $http({
                method: 'POST',
                url: apiPath,
                data: orderCourierInfo,
                ContentType: 'application/json'
            }).success(function (data) {
                deferred.resolve(data);
            }).error(function (data)
            {
                deferred.reject("Data Error");
            })
            return deferred.promise;
        };


    })