/**
 * Created by gghh on 8/18/2014.
 */

'use strict';

angular.module('annectosadmin')
    .service('commonService', function commonService(localStorageService) {

        this.get_all_brand = function ($http, $q,Flag) {
            var apiPath = cat_service_url + '/category/brand_list/'+ Flag;

            var deferred = $q.defer();
            $http({
                method: 'GET',
                url: apiPath,
                ContentType: 'application/json'

            }).success(function (data) {
                deferred.resolve(data);
            }).error(function (data) {
                deferred.reject("Data Error");
            })
            return deferred.promise;

        }

        this.get_all_product = function ($http, $q) {
            var apiPath = cat_service_url + '/category/product_list';

            var deferred = $q.defer();
            $http({
                method: 'GET',
                url: apiPath,

                type: JSON
            }).success(function (data) {
                deferred.resolve(data);
            }).error(function (data) {
                deferred.reject("An error occured while validating User");
            })

            return deferred.promise;
        };

        this.getProductDetails = function ($http, $q,Product_ID) {
            var apiPath = cat_service_url + '/category/product_details/'+Product_ID;

            var deferred = $q.defer();
            $http({
                method: 'GET',
                url: apiPath,
                type: JSON
            }).success(function (data) {
                deferred.resolve(data);
            }).error(function (data) {
                deferred.reject("An error occured while validating User");
            })

            return deferred.promise;
        };

        this.postPopulateCompany= function ($http, $q) {
            var apiPath = cat_service_url+'/user/admin/postPopulateCompany';

            var deferred = $q.defer();
            $http({
                method: 'POST',
                url: apiPath,
                ContentType: 'application/json'

            }).success(function (data) {
                deferred.resolve(data);
            }).error(function (data)
            {
                deferred.reject("Data Error");
            })
            return deferred.promise;
        };
    })