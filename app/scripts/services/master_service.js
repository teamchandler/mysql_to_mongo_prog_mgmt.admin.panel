'use strict';


angular.module('annectosadmin')
  .service('MasterService', function MasterService(UtilService) {

        this.user_login = function($http,$q,credential){

            var apiPath =  UtilService.get_api() +  '/login/by';
            var deferred = $q.defer();
            $http({
                method: 'POST',
                url: apiPath,
                data:credential,
                ContentType: 'application/json'

            }).success(function (data) {
                deferred.resolve(data);

            }).error(function (data)
            {
                deferred.reject("Data Error");


            })
            return deferred.promise;


        }




    });

