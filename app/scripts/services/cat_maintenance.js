/**
 * Created by DRIPL_1 on 09/05/2014.
 */

/**
 * Created by developer1 on 7/3/2014.
 */
 'use strict';

angular.module('annectosadmin')
    .service('cat_maintenanceService', function cat_maintenanceService() {


        this.save_category= function($http,$q,s){

            var apiPath =  cat_service_url +'/save/category';

            var deferred = $q.defer();
            $http({
                method: 'POST',
                url: apiPath,
                data:s,
                ContentType: 'application/json'

            }).success(function (data) {
                deferred.resolve(data);

            }).error(function (data)
            {
                deferred.reject("Data Error");


            })
            return deferred.promise;


        }


        this.get_details_for_level2_name = function($http,$q)
        {
            var apiPath = cat_service_url +'/max/value';

            var deferred = $q.defer();
            $http({
                method: 'GET',
                url: apiPath,
                ContentType: 'application/json'

            }).success(function (data) {
                deferred.resolve(data);
            }).error(function (data)
            {
                deferred.reject("Data Error");
            })
            return deferred.promise;

        }



        this.get_all_category = function ($http, $q) {
            var apiPath = cat_service_url + '/category/cat_list';

            var deferred = $q.defer();
            $http({
                method: 'GET',
                url: apiPath,
                data: {},
                ContentType: 'application/json'

            }).success(function (data) {
                deferred.resolve(data);
            }).error(function (data) {
                deferred.reject("Data Error");
            })
            return deferred.promise;

        }


        this.get_max_value = function ($http, $q) {
            var apiPath = cat_service_url + '/max/value';

            var deferred = $q.defer();
            $http({
                method: 'GET',
                url: apiPath,
                ContentType: 'application/json'

            }).success(function (data) {
                deferred.resolve(data);
            }).error(function (data) {
                deferred.reject("Data Error");
            })
            return deferred.promise;

        }











        this.details_by_category_name = function ($http, $q,Category_ID) {
            var apiPath = cat_service_url + '/category/cat_details/' + Category_ID;

            var deferred = $q.defer();
            $http({
                method: 'GET',
                url: apiPath,
                ContentType: 'application/json'

            }).success(function (data) {
                deferred.resolve(data);
            }).error(function (data) {
                deferred.reject("Data Error");
            })
            return deferred.promise;

        }

        this.PopParentCatList = function ($http, $q,level_val) {
            var apiPath = cat_service_url + '/category/parent_cat_list/' + level_val;

            var deferred = $q.defer();
            $http({
                method: 'GET',
                url: apiPath,
                ContentType: 'application/json'

            }).success(function (data) {
                deferred.resolve(data);
            }).error(function (data) {
                deferred.reject("Data Error");
            })
            return deferred.promise;

        }

        this.Insert_Category = function($http,$q,category_details){

            var apiPath =  cat_service_url +
                '/category/admin/insert_catagory/';
            var deferred = $q.defer();
            $http({
                method: 'POST',
                url: apiPath,
                data:category_details,
                ContentType: 'application/json'

            }).success(function (data) {
                deferred.resolve(data);

            }).error(function (data)
            {
                deferred.reject("Data Error");


            })
            return deferred.promise;


        }

        this.Update_Category = function($http,$q,category_details){

            var apiPath =  cat_service_url +
                '/category/admin/update_catagory/';
            var deferred = $q.defer();
            $http({
                method: 'POST',
                url: apiPath,
                data:category_details,
                ContentType: 'application/json'

            }).success(function (data) {
                deferred.resolve(data);

            }).error(function (data)
            {
                deferred.reject("Data Error");


            })
            return deferred.promise;


        }





        this.id_generation_level2 = function ($http, $q,parentcat_id) {
            var apiPath = cat_service_url + '/get/detalis/level2/'+ parentcat_id;

            var deferred = $q.defer();
            $http({
                method: 'GET',
                url: apiPath,
                ContentType: 'application/json'

            }).success(function (data) {
                deferred.resolve(data);
            }).error(function (data) {
                deferred.reject("Data Error");
            })
            return deferred.promise;

        }

        this.get_details_for_level3_name = function ($http, $q) {
            var apiPath = cat_service_url + '/get/detalis/level3';

            var deferred = $q.defer();
            $http({
                method: 'GET',
                url: apiPath,
                ContentType: 'application/json'

            }).success(function (data) {
                deferred.resolve(data);
            }).error(function (data) {
                deferred.reject("Data Error");
            })
            return deferred.promise;

        }



        this.id_generation_level3 = function ($http, $q,parentcat_id) {
            var apiPath = cat_service_url + '/get/detalis/level2/'+ parentcat_id;

            var deferred = $q.defer();
            $http({
                method: 'GET',
                url: apiPath,
                ContentType: 'application/json'

            }).success(function (data) {
                deferred.resolve(data);
            }).error(function (data) {
                deferred.reject("Data Error");
            })
            return deferred.promise;

        }




        this.update_category_by_id= function($http,$q,s){

            var apiPath =  cat_service_url +'/update/category/by/id';

            var deferred = $q.defer();
            $http({
                method: 'POST',
                url: apiPath,
                data:s,
                ContentType: 'application/json'

            }).success(function (data) {
                deferred.resolve(data);

            }).error(function (data)
            {
                deferred.reject("Data Error");


            })
            return deferred.promise;


        }


        this.insert_audit_data = function($http,$q,info){

            var apiPath =  audit_service_url + '/audit/insert/request';

            var deferred = $q.defer();
            $http({
                method: 'POST',
                url: apiPath,
                data:info,
                ContentType: 'application/json'

            }).success(function (data) {
                deferred.resolve(data);

            }).error(function (data)
            {
                deferred.reject("Data Error");


            })
            return deferred.promise;


        }


    })
