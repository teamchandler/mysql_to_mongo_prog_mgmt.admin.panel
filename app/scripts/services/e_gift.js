/**
 * Created by developer7 on 9/10/2014.
 */
'use strict';

angular.module('annectosadmin')
    .service('eGiftService', function eGiftService() {

        this.SaveGift = function($http,$q,eGiftVou){
            var apiPath =  cat_service_url + '/gv/insert/';
            var deferred = $q.defer();
            $http({
                method: 'POST',
                url: apiPath,
                data: eGiftVou,
                ContentType: 'application/json'
            }).success(function (data) {
                deferred.resolve(data);
            }).error(function (data)
            {
                deferred.reject("Data Error");
            })
            return deferred.promise;
        }

        this.get_GV_Voucher_Details = function($http,$q,StoreName,fromdt,todt){
            var apiPath =  cat_service_url + '/gv/get_GV_Voucher_Details?store=' + StoreName + '&fromdt=' + fromdt + '&todt=' + todt + '&json=true';
            var deferred = $q.defer();
            $http({
                method: 'GET',
                url: apiPath,
                ContentType: 'application/json'
            }).success(function (data) {
                deferred.resolve(data);
            }).error(function (data) {
                deferred.reject("Data Error");
            })
            return deferred.promise;
        }

        this.get_find_voucher_code = function($http,$q,StoreName,gv_code){
            var apiPath =  cat_service_url + '/gv/get_find_voucher_code?gv_code=' + gv_code + '&store=' + StoreName + '&json=true';
            var deferred = $q.defer();
            $http({
                method: 'GET',
                url: apiPath,
                ContentType: 'application/json'
            }).success(function (data) {
                deferred.resolve(data);
            }).error(function (data) {
                deferred.reject("Data Error");
            })
            return deferred.promise;
        }

        this.redeem_voucher_code = function($http,$q,txtEVouCode,MoreValidity){
            var apiPath =  cat_service_url + '/gv/update/post_redeem_voucher_code/';
            var deferred = $q.defer();
            $http({
                method: 'POST',
                url: apiPath,
                data: { gift_voucher_code: txtEVouCode, active: 2, status: "redeemed", validity: MoreValidity },
                ContentType: 'application/json'
            }).success(function (data) {
                deferred.resolve(data);
            }).error(function (data)
            {
                deferred.reject("Data Error");
            })
            return deferred.promise;
        }
    })