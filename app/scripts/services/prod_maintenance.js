/**
 * Created by DRIPL_1 on 09/08/2014.
 */


'use strict';

angular.module('annectosadmin')
    .service('prod_mainService', function ProductService() {
        this.get_productDetailsById = function ($http, $q, product_id) {
            var apiPath = cat_service_url +  '/store/prod_details?prod_id=' + product_id + '&json=true';

            var deferred = $q.defer();
            $http({
                method: 'GET',
                url: apiPath,
                //data: data,
                type: JSON
            }).success(function (data) {
                deferred.resolve(data);
            }).error(function (data) {
                deferred.reject("An error occured while validating User");
            })

            return deferred.promise;
        };

        this.prod_view_insert = function ($http, $q, prod_details) {
            var apiPath = cat_service_url + '/category/admin/insert_product/';
            var deferred = $q.defer();

            $http({
                method: 'POST',
                url: apiPath,
                data: prod_details,
                type: JSON
            }).success(function (data) {
                deferred.resolve(data);

            }).error(function (data)
            {
                deferred.reject("An error occured while validating User");
            })
            return deferred.promise;
        };

        this.prod_view_update = function ($http, $q, prod_details) {
            var apiPath = cat_service_url + '/category/admin/update_product/';
            var deferred = $q.defer();

            $http({
                method: 'POST',
                url: apiPath,
                data: prod_details,
                type: JSON
            }).success(function (data) {
                deferred.resolve(data);

            }).error(function (data)
            {
                deferred.reject("An error occured while validating User");
            })
            return deferred.promise;
        };

        this.get_prod_ship = function ($http, $q, cat_id) {
            var apiPath = cat_service_url + '/store/shipping_details?cat_id=' + cat_id + '&json=true';

            var deferred = $q.defer();
            $http({
                method: 'GET',
                url: apiPath,
                //data: data,
                type: JSON
            }).success(function (data) {
                deferred.resolve(data);
            }).error(function (data) {
                deferred.reject("An error occured while validating User");
            })

            return deferred.promise;
        };


        this.PopulateBrandList = function ($http, $q, a) {
            var apiPath = cat_service_url + '/category/brand_list/'+Flag;

            var deferred = $q.defer();
            $http({
                method: 'GET',
                url: apiPath,
                data: { Flag: a },
                type: JSON
            }).success(function (data) {
                deferred.resolve(data);
            }).error(function (data) {
                deferred.reject("An error occured while validating User");
            })

            return deferred.promise;
        };



//        this.child_prod_exist = function ($http, $q,Product_ID) {
//            var apiPath = cat_service_url + '/category/chkChildProdExist'+Product_ID;
//
//            var deferred = $q.defer();
//            $http({
//                method: 'GET',
//                url: apiPath,
//                type: JSON
//            }).success(function (data) {
//                deferred.resolve(data);
//            }).error(function (data) {
//                deferred.reject("An error occured while validating User");
//            })
//
//            return deferred.promise;
//        };

        this.get_all_brand = function ($http, $q,Flag) {
            var apiPath = cat_service_url + '/category/brand_list/'+ Flag;

            var deferred = $q.defer();
            $http({
                method: 'GET',
                url: apiPath,
                ContentType: 'application/json'

            }).success(function (data) {
                deferred.resolve(data);
            }).error(function (data) {
                deferred.reject("Data Error");
            })
            return deferred.promise;

        }

        this.get_all_product = function ($http, $q) {
            var apiPath = cat_service_url + '/category/product_list';

            var deferred = $q.defer();
            $http({
                method: 'GET',
                url: apiPath,

                type: JSON
            }).success(function (data) {
                deferred.resolve(data);
            }).error(function (data) {
                deferred.reject("An error occured while validating User");
            })

            return deferred.promise;
        };

        this.get_product_details = function ($http, $q,Product_ID) {
            var apiPath = cat_service_url + '/category/product_details/'+Product_ID;

            var deferred = $q.defer();
            $http({
                method: 'GET',
                url: apiPath,
                type: JSON
            }).success(function (data) {
                deferred.resolve(data);
            }).error(function (data) {
                deferred.reject("An error occured while validating User");
            })

            return deferred.promise;
        };

        this.search_product_detail = function ($http, $q,c){
            var apiPath = cat_service_url +  '/category/admin/Prod_List_App/';

            var deferred = $q.defer();
            $http({
                method: 'POST',
                url: apiPath,
                data: c,
                type: JSON
            }).success(function (data) {
                deferred.resolve(data);
            }).error(function (data) {
                deferred.reject("An error occured while validating User");
            })

            return deferred.promise;
        };


        this.getProdSearchList = function ($http, $q,txtSrchProdSKU,txtSrchProdId,txtSrchProdName,ddlSrchBrand) {
            var apiPath =  cat_service_url + '/category/getProdSearchList?prodsku=' + txtSrchProdSKU + '&prodid=' + txtSrchProdId + '&prodName=' + txtSrchProdName + '&prodbrand=' + ddlSrchBrand + '&json=true';

            var deferred = $q.defer();
            $http({
                method: 'GET',
                url: apiPath,
//                data: c,
                type: JSON
            }).success(function (data) {
                deferred.resolve(data);
            }).error(function (data) {
                deferred.reject("An error occured while validating User");
            })

            return deferred.promise;
        };


        this.insert_audit_data = function($http,$q,info){

            var apiPath =  audit_service_url + '/audit/insert/request';

            var deferred = $q.defer();
            $http({
                method: 'POST',
                url: apiPath,
                data:info,
                ContentType: 'application/json'

            }).success(function (data) {
                deferred.resolve(data);

            }).error(function (data)
            {
                deferred.reject("Data Error");


            })
            return deferred.promise;


        }



        /* this.getProductDetails = function ($http, $q,Product_ID) {
         var apiPath = cat_service_url + '/category/product_details/'+Product_ID;

         var deferred = $q.defer();
         $http({
         method: 'GET',
         url: apiPath,
         type: JSON
         }).success(function (data) {
         deferred.resolve(data);
         }).error(function (data) {
         deferred.reject("An error occured while validating User");
         })

         return deferred.promise;
         };*/


    });

