/**
 * Created by Dipanjan on 9/9/2014.
 */


'use strict';

angular.module('annectosadmin')
    .service('brandService', function brandService(localStorageService) {
// AngularJS will instantiate a singleton by calling "new" on this function


        this.save_brand= function($http,$q,txtBrandName){

            var apiPath =  cat_service_url +'/category/admin/insert_brand/';

            var deferred = $q.defer();
            $http({
                method: 'POST',
                url: apiPath,
                data: {name:txtBrandName},
                ContentType: 'application/json'

            }).success(function (data) {
                deferred.resolve(data);

            }).error(function (data)
            {
                deferred.reject("Data Error");

            })
            return deferred.promise;

        }

        this.update_brand = function($http,$q,s){

            var apiPath =  cat_service_url +
                '/category/admin/update_brand/';
            var deferred = $q.defer();
            $http({
                method: 'POST',
                url: apiPath,
                data:s,
                ContentType: 'application/json'

            }).success(function (data) {
                deferred.resolve(data);

            }).error(function (data)
            {
                deferred.reject("Data Error");


            })
            return deferred.promise;

        }


        this.details_by_brand_name = function($http,$q,Brand_name)
        {
            var apiPath = cat_service_url+'/category/brand_details/'+ Brand_name;

            var deferred = $q.defer();
            $http({
                method: 'GET',
                url: apiPath,
                ContentType: 'application/json'

            }).success(function (data) {
                deferred.resolve(data);
            }).error(function (data)
            {
                deferred.reject("Data Error");
            })
            return deferred.promise;

        }

        this.insert_audit_data = function($http,$q,info){

            var apiPath =  audit_service_url + '/audit/insert/request';

            var deferred = $q.defer();
            $http({
                method: 'POST',
                url: apiPath,
                data:info,
                ContentType: 'application/json'

            }).success(function (data) {
                deferred.resolve(data);

            }).error(function (data)
            {
                deferred.reject("Data Error");


            })
            return deferred.promise;


        }

    })
