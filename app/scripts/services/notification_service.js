'use strict';

angular.module('annectosadmin')

    .service('NotificationService', function NotificationService($rootScope) {


        this.error_occured = function (broadcast_message, error_object, source, o) {
            this.error_object  = error_object;
            this.error_source = source;
            this.o = o;
            this.notify(broadcast_message)
        };
        this.change_header = function (broadcast_message, header_label) {
            this.header_label = header_label;
            this.notify("change header")
        };
        this.send_notification = function (broadcast_message, notification_data) {
            this.notification_data =  notification_data;
            this.notify(broadcast_message)
        };

        this.show_loader = function (broadcast_message, loader_condition) {
            this.loader_condition = loader_condition;
            this.notify("show loader")
        };

        this.hide_loader = function (broadcast_message, loader_condition) {
            this.loader_condition = loader_condition;
            this.notify("hide loader")
        };


        this.change_old_data = function (broadcast_message, old_data) {
            this.old_data = old_data;
            this.notify(broadcast_message)
        };

        //********** Generic Notification ********************
        this.notify = function(broadcast_message) {
            $rootScope.$broadcast(broadcast_message);
        };
        //*****************************************************




    });
