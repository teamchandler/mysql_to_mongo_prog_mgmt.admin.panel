/**
 * Created by Dipanjan on 9/8/2014.
 */

'use strict';

angular.module('annectosadmin')
    .service('upload_imgService', function upload_imgService() {

            this.get_img_url = function ($http, $q, id) {
                var apiPath = cat_service_url + '/category/get_cat_imgvidurls/'+ id ;

                var deferred = $q.defer();
                $http({
                    method: 'GET',
                    url: apiPath,
                   // data:{cat_id:id},
                    type: JSON
                }).success(function (data) {
                    deferred.resolve(data);
                }).error(function (data) {
                    deferred.reject("An error occured while validating User");
                })

                return deferred.promise;
            };



        this.upload_cat_img_vid = function ($http, $q, data) {
            var apiPath = cat_service_url +'/category/admin/upload_cat_imgvid/';

            var deferred = $q.defer();
            $http({
                method: 'POST',
                url: apiPath,
                data:data,
                type: JSON
            }).success(function (data) {
                deferred.resolve(data);
            }).error(function (data) {
                deferred.reject("An error occured while validating User");
            })

            return deferred.promise;
        };



        this.update_cat_img_vid = function ($http, $q, data) {
            var apiPath = cat_service_url +'/category/admin/update_cat_imgvid/';

            var deferred = $q.defer();
            $http({
                method: 'POST',
                url: apiPath,
                data:data,
                type: JSON
            }).success(function (data) {
                deferred.resolve(data);
            }).error(function (data) {
                deferred.reject("An error occured while validating User");
            })

            return deferred.promise;
        };


          })