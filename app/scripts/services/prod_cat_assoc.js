/**
 * Created by DRIPL_1 on 09/06/2014.
 */

'use strict';

angular.module('annectosadmin')
    .service('prod_cat_assocService', function ProductService() {
        this.get_category_tree = function ($http, $q) {
            var apiPath = cat_service_url + '/category/cat_list_tree';

            var deferred = $q.defer();
            $http({
                method: 'GET',
                url: apiPath,

                type: JSON
            }).success(function (data) {
                deferred.resolve(data);
            }).error(function (data) {
                deferred.reject("An error occured while validating User");
            })

            return deferred.promise;
        };

        this.get_category_list = function ($http, $q) {
            var apiPath = cat_service_url + '/category/cat_list_for_product';

            var deferred = $q.defer();
            $http({
                method: 'GET',
                url: apiPath,

                type: JSON
            }).success(function (data) {
                deferred.resolve(data);
            }).error(function (data) {
                deferred.reject("An error occured while validating User");
            })

            return deferred.promise;
        };

        /* this.get_product_assc_detail = function ($http, $q,Product_ID) {
         var apiPath = cat_service_url + '/category/cat_list_for_product';

         var deferred = $q.defer();
         $http({
         method: 'GET',
         url: apiPath,

         type: JSON
         }).success(function (data) {
         deferred.resolve(data);
         }).error(function (data) {
         deferred.reject("An error occured while validating User");
         })

         return deferred.promise;
         };*/

        this.insert_pca = function ($http, $q, pca_data) {
            var apiPath = cat_service_url + '/category/admin/update_prod_cat/';
            var deferred = $q.defer();

            $http({
                method: 'POST',
                url: apiPath,
                data: pca_data,
                type: JSON
            }).success(function (data) {
                deferred.resolve(data);

            }).error(function (data)
            {
                deferred.reject("An error occured while validating User");
            })
            return deferred.promise;
        };

        this.insert_audit_data = function($http,$q,info){

            var apiPath =  audit_service_url + '/audit/insert/request';

            var deferred = $q.defer();
            $http({
                method: 'POST',
                url: apiPath,
                data:info,
                ContentType: 'application/json'

            }).success(function (data) {
                deferred.resolve(data);

            }).error(function (data)
            {
                deferred.reject("Data Error");


            })
            return deferred.promise;


        }




    })