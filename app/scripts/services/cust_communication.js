/**
 * Created by developer7 on 9/11/2014.
 */

'use strict';

angular.module('annectosadmin')
    .service('custCommService', function custCommService() {

        this.getUserDatePageWise = function($http,$q,ddlUIComp,PgNo,SearchEmail){
            var apiPath =  cat_service_url + '/category/getUserDatePageWise?company=' + ddlUIComp + '&pagenum=' + PgNo + '&useremail=' + SearchEmail + '&json=true'
            var deferred = $q.defer();
            $http({
                method: 'GET',
                url: apiPath,
                ContentType: 'application/json'
            }).success(function (data) {
                deferred.resolve(data);
            }).error(function (data) {
                deferred.reject("Data Error");
            })
            return deferred.promise;
        }
        this.SendUserCredential = function($http,$q,Email_IDs){
            var apiPath =  cat_service_url + '/user/postSendUserCredential/';
            var deferred = $q.defer();
            $http({
                method: 'POST',
                url: apiPath,
                data: {emailids: Email_IDs},
                ContentType: 'application/json'
            }).success(function (data) {
                deferred.resolve(data);
            }).error(function (data)
            {
                deferred.reject("Data Error");
            })
            return deferred.promise;
        }
        this.postRegPassword = function($http,$q,Email_IDs){
            var apiPath =  cat_service_url + '/user/postRegPassword/';
            var deferred = $q.defer();
            $http({
                method: 'POST',
                url: apiPath,
                data: {emailids: Email_IDs},
                ContentType: 'application/json'
            }).success(function (data) {
                deferred.resolve(data);
            }).error(function (data)
            {
                deferred.reject("Data Error");
            })
            return deferred.promise;
        }

        this.postExportCustList = function($http,$q,Cust_Data){
            var apiPath =  cat_service_url + '/category/admin/postExportCustList/';
            var deferred = $q.defer();
            $http({
                method: 'POST',
                url: apiPath,
                data: {GridData: Cust_Data},
                ContentType: 'application/json'
            }).success(function (data) {
                deferred.resolve(data);
            }).error(function (data)
            {
                deferred.reject("Data Error");
            })
            return deferred.promise;
        }
        this.postSendUserPromoCredential = function($http,$q,Email_IDs){
            var apiPath =  cat_service_url + '/user/postSendUserPromoCredential/';
            var deferred = $q.defer();
            $http({
                method: 'POST',
                url: apiPath,
                data: {emailids: Email_IDs},
                ContentType: 'application/json'
            }).success(function (data) {
                deferred.resolve(data);
            }).error(function (data)
            {
                deferred.reject("Data Error");
            })
            return deferred.promise;
        }
        this.postSendUserPromo = function($http,$q,Email_IDs){
            var apiPath =  cat_service_url + '/user/postSendUserPromo/';
            var deferred = $q.defer();
            $http({
                method: 'POST',
                url: apiPath,
                data: {emailids: Email_IDs},
                ContentType: 'application/json'
            }).success(function (data) {
                deferred.resolve(data);
            }).error(function (data)
            {
                deferred.reject("Data Error");
            })
            return deferred.promise;
        }
        this.SendSmsTransactional = function($http,$q,Email_IDs){
            var apiPath =  cat_service_url + '/user/postSendSmsTransactional/';
            var deferred = $q.defer();
            $http({
                method: 'POST',
                url: apiPath,
                data: {emailids: Email_IDs},
                ContentType: 'application/json'
            }).success(function (data) {
                deferred.resolve(data);
            }).error(function (data)
            {
                deferred.reject("Data Error");
            })
            return deferred.promise;
        }


    })