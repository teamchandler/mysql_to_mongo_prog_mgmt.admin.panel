/**
 * Created by DRIPL_1 on 09/09/2014.
 */
'use strict';

angular.module('annectosadmin')

    .service('prodfeatureService', function prodfeatureService() {
        this.validate_discount_coupon = function($http, $q, coupon, store, user_id){
            var apiPath = cat_service_url + '/store/coupon/validate?coupon='+ coupon  + "&store=" + store + "&user="+ user_id + "&json=true" ;
            var deferred = $q.defer();
            $http({
                method: 'GET',
                url: apiPath,
                type: JSON
            }).success(function (data) {
                deferred.resolve(data);
            }).error(function (data)
            {
                deferred.reject("An error occured while validating User");
            })
            return deferred.promise;

        }
        this.RedeemUserPoint = function ($http, $q, dataobj) {


            //var da = $('#myform').serializeArray();/*Get All Form data */

//        var a = dataobj.total_amount;
//
//        if (a <= 0) {

            var apiPath = cat_service_url + '/gv/pay_by_gv_points/';
            var deferred = $q.defer();

            var user_pont = {
                EmailID:dataobj.billing_cust_email,
                OrderID:dataobj.Order_Id,
                PaidAmt:dataobj.total_amount,
                Points: dataobj.Points,
                Points_INR:dataobj.Points_Inr,
                strActualAmt:dataobj.total_amount,
                egiftVouNo:dataobj.EGift_VNO,
                egiftVouAmt: dataobj.EGift_Amt,
                discount_coupon: dataobj.discount_coupon,
                discount_amount: dataobj.discount_coupon_total,
                status:2
            }


            $http({
                method: 'POST',
                url: apiPath,
                data: user_pont,
                type: JSON
            }).success(function (data) {
                deferred.resolve(data);
//                    var confirm_order_url = root_url + "/payment.html#/payment/2";



            }).error(function (data) {
                deferred.reject("An error occured while Processing Payment");
            })
            return deferred.promise;
//        }
//        else {
//            $('#myform').submit();
//        }



        }





        this.add_transaction_details = function ($http, $q,dataobj){
            var apiPath = cat_service_url + '/checkout/insert_transaction_details/';

            var deferred = $q.defer();
            $http({
                method: 'POST',
                url: apiPath,
                data: dataobj,
                type: JSON
            }).success(function (data) {
                deferred.resolve(data);
            }).error(function (data) {
                deferred.reject("An error occured while validating User");
            })

            return deferred.promise;
        };


        this.add_check_details = function ($http, $q,dataobj){
            var apiPath = cat_service_url +  '/checkout/insert_check_details/';

            var deferred = $q.defer();
            $http({
                method: 'POST',
                url: apiPath,
                data: dataobj,
                type: JSON
            }).success(function (data) {
                deferred.resolve(data);
            }).error(function (data) {
                deferred.reject("An error occured while validating User");
            })

            return deferred.promise;
        };







        this.get_parent_list = function($http, $q){
            var apiPath = cat_service_url + '/category/parent_product_list' ;
            var deferred = $q.defer();
            $http({
                method: 'GET',
                url: apiPath,
                type: JSON
            }).success(function (data) {
                deferred.resolve(data);
            }).error(function (data)
            {
                deferred.reject("An error occured while validating User");
            })
            return deferred.promise;

        }

        this.get_prod_feature_detail = function($http, $q,Product_ID){
            var apiPath = cat_service_url +'/category/product_features/'+ Product_ID ;
            var deferred = $q.defer();
            $http({
                method: 'GET',
                url: apiPath,
                type: JSON
            }).success(function (data) {
                deferred.resolve(data);
            }).error(function (data)
            {
                deferred.reject("An error occured while validating User");
            })
            return deferred.promise;

        }

        this.get_features_grid_data = function($http, $q,Product_ID){
            var apiPath = cat_service_url +'/category/get_cat_features/' + Product_ID ;
            var deferred = $q.defer();
            $http({
                method: 'GET',
                url: apiPath,
                type: JSON
            }).success(function (data) {
                deferred.resolve(data);
            }).error(function (data)
            {
                deferred.reject("An error occured while validating User");
            })
            return deferred.promise;

        }
        this.insert_feature = function ($http, $q,a){
            var apiPath = cat_service_url +  '/category/admin/update_prod_features/';

            var deferred = $q.defer();
            $http({
                method: 'POST',
                url: apiPath,
                data: a,
                type: JSON
            }).success(function (data) {
                deferred.resolve(data);
            }).error(function (data) {
                deferred.reject("An error occured while validating User");
            })

            return deferred.promise;
        };

        this.delete_prod_feature = function ($http, $q,p){
            var apiPath = cat_service_url +  '/product/del_product_feature/';

            var deferred = $q.defer();
            $http({
                method: 'POST',
                url: apiPath,
                data: p,
                type: JSON
            }).success(function (data) {
                deferred.resolve(data);
            }).error(function (data) {
                deferred.reject("An error occured while validating User");
            })

            return deferred.promise;
        };

        this.insert_audit_data = function($http,$q,info){

            var apiPath =  audit_service_url + '/audit/insert/request';

            var deferred = $q.defer();
            $http({
                method: 'POST',
                url: apiPath,
                data:info,
                ContentType: 'application/json'

            }).success(function (data) {
                deferred.resolve(data);

            }).error(function (data)
            {
                deferred.reject("Data Error");


            })
            return deferred.promise;


        }




    });
