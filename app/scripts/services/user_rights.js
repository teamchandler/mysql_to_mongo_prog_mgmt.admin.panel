/**
 * Created by developer7 on 9/10/2014.
 */
'use strict';

angular.module('annectosadmin')
    .service('usersService', function usersService() {

        this.getUser_For_Rights = function($http,$q){
            var apiPath =  cat_service_url + '/category/getUser_For_Rights/{company}';
            var deferred = $q.defer();
            $http({
                method: 'GET',
                url: apiPath,
                data: { company: "Annectos" },
                ContentType: 'application/json'
            }).success(function (data) {
                deferred.resolve(data);
            }).error(function (data) {
                deferred.reject("Data Error");
            })
            return deferred.promise;
        };

        this.getUserRights = function($http,$q,USER_ID,Flag){
            var apiPath =  cat_service_url + '/category/getUserRights?USER_ID=' + USER_ID + '&FLAG=' + Flag + '&json=true';
            var deferred = $q.defer();
            $http({
                method: 'GET',
                url: apiPath,
                ContentType: 'application/json'
            }).success(function (data) {
                deferred.resolve(data);
            }).error(function (data) {
                deferred.reject("Data Error");
            })
            return deferred.promise;
        }

//        this.getUserRights = function($http,$q,USER_ID){
//            var apiPath =  cat_service_url + '/category/getUserRights?USER_ID=' + USER_ID + '&json=true';
//            var deferred = $q.defer();
//            $http({
//                method: 'GET',
//                url: apiPath,
//                ContentType: 'application/json'
//            }).success(function (data) {
//                deferred.resolve(data);
//            }).error(function (data) {
//                deferred.reject("Data Error");
//            })
//            return deferred.promise;
//        }

        this.postAssignRights= function($http,$q,Rights_ID,GlobalUserId){
            var apiPath =  cat_service_url + '/category/admin/postAssignRights/';
            var deferred = $q.defer();
            $http({
                method: 'POST',
                url: apiPath,
                data: { RIGHTS_IDS: Rights_ID, USER_ID: GlobalUserId },
                ContentType: 'application/json'
            }).success(function (data) {
                deferred.resolve(data);
            }).error(function (data)
            {
                deferred.reject("Data Error");
            })
            return deferred.promise;
        }


        this.postDeleteAssignRights= function($http,$q,Rights_ID,GlobalUserId){
            var apiPath =  cat_service_url + '/category/admin/postDeleteAssignRights/';
            var deferred = $q.defer();
            $http({
                method: 'POST',
                url: apiPath,
                data: { RIGHTS_IDS: Rights_ID, USER_ID: GlobalUserId},
                ContentType: 'application/json'
            }).success(function (data) {
                deferred.resolve(data);
            }).error(function (data)
            {
                deferred.reject("Data Error");
            })
            return deferred.promise;
        }

        this.SaveUserEntry = function($http,$q,user_entry){
            var apiPath =  cat_service_url + '/user/admin/postUserEntry/';
            var deferred = $q.defer();
            $http({
                method: 'POST',
                url: apiPath,
                data: user_entry,
                ContentType: 'application/json'
            }).success(function (data) {
                deferred.resolve(data);
            }).error(function (data)
            {
                deferred.reject("Data Error");
            })
            return deferred.promise;
        }

        this.UpdateUserEntry = function($http,$q,user_entry){
            var apiPath =  cat_service_url + '/user/admin/updateUserEntry/';
            var deferred = $q.defer();
            $http({
                method: 'POST',
                url: apiPath,
                data: user_entry,
                ContentType: 'application/json'
            }).success(function (data) {
                deferred.resolve(data);
            }).error(function (data)
            {
                deferred.reject("Data Error");
            })
            return deferred.promise;
        }



    })