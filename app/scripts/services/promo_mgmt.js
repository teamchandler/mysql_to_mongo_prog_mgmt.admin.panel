/**
 * Created by gghh on 9/11/2014.
 */
'use strict';

angular.module('annectosadmin')
    .service('promoMgmtService', function promoMgmtService() {

        this.get_all_promo = function ($http, $q,Promo_Id) {
            var apiPath = cat_service_url + '/admin/promo_list?promo_id='+Promo_Id;
            var deferred = $q.defer();
            $http({
                method: 'GET',
                url: apiPath,
                ContentType: 'application/json'

            }).success(function (data) {
                deferred.resolve(data);
            }).error(function (data) {
                deferred.reject("Data Error");
            })
            return deferred.promise;

        }

        //
        this.save_promo = function($http,$q,promo_data){
            var apiPath =  cat_service_url + '/admin/save_promo/';
            var deferred = $q.defer();
            $http({
                method: 'POST',
                url: apiPath,
                data: promo_data,
                ContentType: 'application/json'
            }).success(function (data) {
                deferred.resolve(data);
            }).error(function (data)
            {
                deferred.reject("Data Error");
            })
            return deferred.promise;
        }
        //
        this.transfer_promo = function($http,$q,ddlPromotion,Comp){
            var apiPath =  cat_service_url + '/admin/transfer_promo/';
            var deferred = $q.defer();
            $http({
                method: 'POST',
                url: apiPath,
                data: { promo_id: ddlPromotion, company: Comp },
                ContentType: 'application/json'
            }).success(function (data) {
                deferred.resolve(data);
            }).error(function (data)
            {
                deferred.reject("Data Error");
            })
            return deferred.promise;
        }
    })