/**
 * Created by DRIPL_1 on 09/08/2014.
 */

/**
 * Created by gghh on 8/19/2014.
 */

'use strict';

angular.module('annectosadmin')
    .service('child_prod_mainService', function child_prod_mainService() {
        this.get_parent_product = function ($http, $q) {
            var apiPath = cat_service_url +  '/category/get_parent_prod_list';

            var deferred = $q.defer();
            $http({
                method: 'GET',
                url: apiPath,
                type: JSON
            }).success(function (data) {
                deferred.resolve(data);
            }).error(function (data) {
                deferred.reject("An error occured while validating User");
            })

            return deferred.promise;
        };

        this.get_child_product = function ($http, $q,ddlParentProduct) {
            var apiPath = cat_service_url +  '/category/get_child_product/' + ddlParentProduct;

            var deferred = $q.defer();
            $http({
                method: 'GET',
                url: apiPath,
                type: JSON
            }).success(function (data) {
                deferred.resolve(data);
            }).error(function (data) {
                deferred.reject("An error occured while validating User");
            })

            return deferred.promise;
        };

        this.insert_child_product = function ($http, $q,child_data) {
            var apiPath = cat_service_url +  '/category/admin/insert_child_product/';

            var deferred = $q.defer();
            $http({
                method: 'POST',
                url: apiPath,
                data:child_data,
                type: JSON
            }).success(function (data) {
                deferred.resolve(data);
            }).error(function (data) {
                deferred.reject("An error occured while validating User");
            })

            return deferred.promise;
        };

        this.update_data = function ($http, $q,a) {
            var apiPath = cat_service_url +  '/category/upd_child_product/';

            var deferred = $q.defer();
            $http({
                method: 'POST',
                url: apiPath,
                data:a,
                type: JSON
            }).success(function (data) {
                deferred.resolve(data);
            }).error(function (data) {
                deferred.reject("An error occured while validating User");
            })

            return deferred.promise;
        };

        this.delete_child = function ($http, $q,data) {
            var apiPath = cat_service_url + '/category/del_child_product/';

            var deferred = $q.defer();
            $http({
                method: 'POST',
                url: apiPath,
                data:data,
                type: JSON
            }).success(function (data) {
                deferred.resolve(data);
            }).error(function (data) {
                deferred.reject("An error occured while validating User");
            })

            return deferred.promise;
        };

        this.insert_audit_data = function($http,$q,info){

            var apiPath =  audit_service_url + '/audit/insert/request';

            var deferred = $q.defer();
            $http({
                method: 'POST',
                url: apiPath,
                data:info,
                ContentType: 'application/json'

            }).success(function (data) {
                deferred.resolve(data);

            }).error(function (data)
            {
                deferred.reject("Data Error");


            })
            return deferred.promise;


        };


    })

