/**
 * Created by developer1 on 6/26/2014.
 */
'use strict';

angular.module('annectosadmin')
    .service('customerMainService', function customerMainService(localStorageService) {
// AngularJS will instantiate a singleton by calling "new" on this function


this.get_all_company = function($http,$q)
{
    var apiPath = cat_service_url+'/user/admin/postPopulateCompany';

    var deferred = $q.defer();
    $http({
        method: 'POST',
        url: apiPath,
        ContentType: 'application/json'

    }).success(function (data) {
        deferred.resolve(data);
    }).error(function (data)
    {
        deferred.reject("Data Error");
    })
    return deferred.promise;

}


this.details_by_company_name = function($http,$q,sel_company)
{
    var apiPath = cat_service_url+'/user/admin/postCompanyDetails/';

    var deferred = $q.defer();
    $http({
        method: 'POST',
        url: apiPath,
        data:{ name: sel_company },
        ContentType: 'application/json'

    }).success(function (data) {
        deferred.resolve(data);
    }).error(function (data)
    {
        deferred.reject("Data Error");
    })
    return deferred.promise;

}

        this.save_company = function($http,$q,company_details){

            var apiPath =  cat_service_url +
                '/user/admin/postCompanyEntry/';
            var deferred = $q.defer();
            $http({
                method: 'POST',
                url: apiPath,
                data:company_details,
                ContentType: 'application/json'

            }).success(function (data) {
                deferred.resolve(data);

            }).error(function (data)
            {
                deferred.reject("Data Error");


            })
            return deferred.promise;


        }


        this.GetUserRights = function($http,$q,username) {

            var apiPath = cat_service_url +
                '/admin/getuser_assign_rights?USER_NAME=' + username + '&Comp_ID=1&json=true';
            var deferred = $q.defer();
            $http({
                method: 'GET',
                url: apiPath,

                ContentType: 'application/json'

            }).success(function (data) {

                // var USER_RIGHTS = data;
                deferred.resolve(data);

            }).error(function (data) {
                deferred.reject("Data Error");


            })
            return deferred.promise;
        };


        this.update_company = function($http,$q,data){

            var apiPath =  cat_service_url + '/user/admin/updateCompanyEntry/';

            var deferred = $q.defer();
            $http({
                method: 'POST',
                url: apiPath,
                data:data,
                ContentType: 'application/json'

            }).success(function (data) {
                deferred.resolve(data);

            }).error(function (data)
            {
                deferred.reject("Data Error");


            })
            return deferred.promise;


        }

        this.insert_audit_data = function($http,$q,info){

            var apiPath =  audit_service_url + '/audit/insert/request';

            var deferred = $q.defer();
            $http({
                method: 'POST',
                url: apiPath,
                data:info,
                ContentType: 'application/json'

            }).success(function (data) {
                deferred.resolve(data);

            }).error(function (data)
            {
                deferred.reject("Data Error");


            })
            return deferred.promise;


        }












    })











