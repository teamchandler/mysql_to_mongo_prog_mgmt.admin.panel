/**
 * Created by Dipanjan on 9/10/2014.
 */

'use strict';

angular.module('annectosadmin')
    .service('prod_approvalService', function prod_approvalService(localStorageService) {

        this.get_all_brand = function ($http, $q,flag) {
            var apiPath = cat_service_url + '/category/brand_list/'+ flag;

            var deferred = $q.defer();
            $http({
                method: 'GET',
                url: apiPath,
                ContentType: 'application/json'

            }).success(function (data) {
                deferred.resolve(data);
            }).error(function (data) {
                deferred.reject("Data Error");
            })
            return deferred.promise;

        }


        this.search_product_detail = function ($http, $q, c){
            var apiPath = cat_service_url +  '/category/admin/Prod_List_App/';

            var deferred = $q.defer();
            $http({
                method: 'POST',
                url: apiPath,
                data: c,
                type: JSON
            }).success(function (data) {
                deferred.resolve(data);
            }).error(function (data) {
                deferred.reject("An error occured while validating User");
            })

            return deferred.promise;
        };


        this.product_for_approval = function ($http, $q,Products){
            var apiPath = cat_service_url + '/category/admin/Prod_For_Approval/';

            var deferred = $q.defer();
            $http({
                method: 'POST',
                url: apiPath,
                data: {prod_ids:Products},
                type: JSON
            }).success(function (data) {
                deferred.resolve(data);
            }).error(function (data) {
                deferred.reject("An error occured while validating User");
            })

            return deferred.promise;
        };



    })


