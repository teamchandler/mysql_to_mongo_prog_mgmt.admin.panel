/**
 * Created by developer7 on 9/11/2014.
 */
'use strict';

angular.module('annectosadmin')
    .controller('custCommunicationCtrl', function ($scope,$rootScope,$location,$http,$q,commonService,custCommService,UtilService,$state) {


        var RootStart = "http://";
        var RootEnd = ".annectos.net";
        $scope.selRowIdx = [];

        $scope.$watch('sel_company', function () {
            if ($scope.sel_company == "0") {
                //alert('Please Select A Company');
                return false;
            }
            getUserIntimate();
        });

        init();
        function init() {

            UtilService.change_header($state.current.data.header_label);

            PopCompanyUI();
        }




        $scope.UserIntimateSearch = function(){
            getUserIntimate();
        }
        function PopCompanyUI(){

          //  loading_add();

            UtilService.show_loader();
            commonService.postPopulateCompany($http,$q)

                .then(function (data) {
                    if(data.length>0){
                        data[0].name = "Select Company";
                    }
                    $scope.comp_list = data;
                    $scope.sel_company = 0;
                 //   loading_cut();

                    UtilService.hide_loader();
                },
                function (error) {
                    //Display an error message
                    $scope.error = error;

                });
        }

        $scope.CancelUser = function() {
            $scope.userIntimateList = null;
            $scope.sel_company = 0;
            $scope.user_email='';
            $scope.pagerInfo = false;
        }

        $scope.FirstCall = function() {
            getUserIntimateDtls(1);
        }

        $scope.LastCall = function() {
            var TotalPage = $scope.total_page;
            getUserIntimateDtls(TotalPage);
        }

        $scope.NextCall = function() {
            var pageNo = $scope.curr_page_no;
            var NextPage = pageNo * 1 + 1;
            var TotalPage = $scope.total_page;
            if (NextPage > TotalPage) {
                alert('No Next Page Available');
            }
            else {

                $scope.curr_page_no = NextPage;
                getUserIntimateDtls(NextPage);
            }
        }

        $scope.PrevCall = function() {
            var PageNo = $scope.curr_page_no;
            var PrevPage = PageNo * 1 - 1;
            if (PrevPage == 0) {
                alert('No Previous Page Available');
            }
            else {
                $scope.curr_page_no = PrevPage;
                getUserIntimateDtls(PrevPage);
            }
        }


        $scope.PageNo_Change= function() {
            var pageno = $scope.curr_page_no;
            getUserIntimateDtls(pageno);
        }

        function getUserIntimate() {
            $scope.curr_page_no = 1;
            getUserIntimateDtls(1);
        }

        function getUserIntimateDtls(PgNo) {
            var ddlUICompVal = $scope.sel_company;
            if (ddlUICompVal == "0") {
                alert('Please Select A Company');
                return false;
            }

            var ddlUIComp = '';
            for(var i=0; i<$scope.comp_list.length; i++){
                if($scope.comp_list[i].id == ddlUICompVal){
                    ddlUIComp = $scope.comp_list[i].name;
                    break;
                }
            }

            var SearchEmail = null;

            var txtSearchEmail = $scope.user_email;
            if (txtSearchEmail == null || txtSearchEmail == " ") {
                SearchEmail = '';
            }
            else {
                SearchEmail = txtSearchEmail;
            }

            custCommService.getUserDatePageWise($http, $q, ddlUIComp,PgNo,SearchEmail)
                .then(function (data) {
                    $scope.userIntimateList = data;
                    if(data != null && data.length){
                        $scope.pagerInfo = true;
                        $scope.total_page = data[0].Total_Page;
                    }else{
                        $scope.pagerInfo = false;
                        alert('No Records Found');
                        $scope.total_page = '';
                    }
                },
                function (error) {
                    alert('Error');
                    $scope.error = error;
                });



        }

        $scope.SendUserCredential = function() {
            var Email_IDs = GetSelectedEmailID();
            custCommService.SendUserCredential($http, $q,Email_IDs)
                .then(function (data) {
                    alert('Email Has Been Send');
                },
                function (error) {
                    alert('Error');
                    $scope.error = error;
                });
        }


        $scope.selUserIntimate = function(rowIdx){
            if($scope.selRowIdx.indexOf(rowIdx) == -1){
                $scope.selRowIdx[$scope.selRowIdx.length] = rowIdx;
            }

        }
        function GetSelectedEmailID() {

            try {
                var ids = $scope.selRowIdx;

                if (ids == null) {
                    alert('Please Populate The Grid First');
                    return false;
                }
                if (ids.length == 0) {
                    alert('Please Select Atleast One Email ID');
                    return false;

                }
                var count = ids.length;
                var EMAIL_ID = new Array();
                for (var i = 0; i < count; i++) {
                    EMAIL_ID[i] = $scope.userIntimateList[ids[i]].email_id;
                }
                return EMAIL_ID;
            }
            catch (e) {
                return 0;
            }

        }

        $scope.RegPassword = function() {
            var Email_IDs = GetSelectedEmailID();
            custCommService.postRegPassword($http, $q,Email_IDs)
                .then(function (data) {
                    alert('Regenerate Password Successfully');
                },
                function (error) {
                    alert('Error');
                    $scope.error = error;
                });
        }

        $

        $scope.ExportCustList = function() {

            var ids = $scope.selRowIdx;

            if (typeof ids === "undefined") {
                alert('Please Populate The Grid First');
                return false;
            }
            if (ids.length == 0) {
                alert('Please Select Atleast One Customer');
                return false;

            }
            var Cust_Data = new Array();
            var selRow = $scope.selRowIdx
            for (var i = 0; i < selRow.length; i++)  //iterate through array of selected rows
            {
                var ret = $scope.selRow[i];   //get the selected row
                Cust_Data[i] = ret;
            }
            custCommService.postRegPassword($http, $q,Cust_Data)
                .then(function (data) {
                    //window.open(siteurl + result + ".csv", "_blank");
                },
                function (error) {
                    alert('Error');
                    $scope.error = error;
                });

        }

        $scope.SendPromoWithCredentials = function() {
            var Email_IDs = GetSelectedEmailID();

            custCommService.postSendUserPromoCredential($http, $q,Email_IDs)
                .then(function (data) {
                    alert(data);
                },
                function (error) {
                    alert('Error');
                    $scope.error = error;
                });


        }

        $scope.SendPromo = function() {
            var Email_IDs = GetSelectedEmailID();

            custCommService.postSendUserPromo($http, $q,Email_IDs)
                .then(function (data) {
                    alert(data);
                },
                function (error) {
                    alert('Error');
                    $scope.error = error;
                });

        }

        $scope.SendSMSTransactional = function() {
            var Email_IDs = GetSelectedEmailID();

            custCommService.SendSmsTransactional($http, $q,Email_IDs)
                .then(function (data) {
                    alert(data);
                },
                function (error) {
                    alert('Error');
                    $scope.error = error;
                });

        }
    });