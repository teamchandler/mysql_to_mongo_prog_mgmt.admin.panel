'use strict';

angular.module('annectosadmin')
  .controller('login_loginCtrl', function ($scope,$state, $http, $q, $window, $location, $rootScope,loginService,localStorageService,UtilService)
    {

        function init() {

            UtilService.change_header($state.current.data.header_label);

        }
        init();

        function loading_add()
        {

            $scope.loading = true;

        }

        function loading_cut()
        {

            $scope.loading = false;

        }

        $scope.TrapKeyEvent=function(keyCode){

            console.log(keyCode);

            if(keyCode=='13'){
               $scope.UserValidate();
            }
        }


        $scope.UserValidate=function() {

            var username= ($scope.user_name);
            var password= ($scope.password);


           if(username=="" || username==null){
               alert('Please enter your username');
               return false;
           }
            if(password=="" || password==null){
                alert('Please enter your password');
                return false;
            }


           // loading_add();

            UtilService.show_loader();

            $scope.login = {};
            $scope.login.user_name = username;
            $scope.login.password = password;
            $scope.login.user_type = "ADM";


           // localStorageService.add('user_info', $scope.login.user_name);

          //  $scope.dat1={user_name:username,password:password,user_type:"ADM"}
            loginService.validate($http, $q, $scope.login)
                .then(function (data) {

//                    loading_cut();

                    UtilService.hide_loader();

                    $scope.list = data[0];

                  if($scope.list.message == "Log In Successfully")
                    {
                        $state.go('main.home');
                        localStorageService.add('admin_user_info',data[0]);
                    }
                    else{
                        alert('Invalid User Name Or Password ');
                        $scope.login.user_name = "";
                        $scope.login.password = "";
                        localStorageService.set('admin_user_info',{});

                    }

                },
                function () {
                    //Display an error message
                   // loading_cut();

                    UtilService.hide_loader();

                    $scope.error = error;

                   // $scope.divLoadBusy = false;
                });



        }


    });
