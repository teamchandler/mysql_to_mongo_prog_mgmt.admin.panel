/**
 * Created by developer6 on 9/4/2014.
 */
/**
 * Created by developer1 on 6/24/2014.
 */

'use strict';

angular.module('annectosadmin')
    .controller('customerMainController', function ($scope,$rootScope,$location,$http,$q,$state,customerMainService,localStorageService,NotificationService,UtilService) {


        var RootStart = "http://";
        var RootEnd = ".annectos.net";

        var user_info;
        var comapany_details;
        var details;
        var previous_data={};

        $scope.$watch('change_to_company', function() {

        });

        $scope.$watch('company_name', function () {



            if($scope.company_name != "New Company")
            {

                details_by_company_name();
            }
            else{

                CompanyNewView();

            }

        });


        init();
        function init() {
            // get_all_company1();

            UtilService.change_header($state.current.data.header_label);

            user_info=localStorageService.get("admin_user_info").user_name;
            var username = user_info;

           // getUserRights(username);
            get_all_company();

            //CompanyNewView();
            var initCompanyData = {id: "0",
                sales_tracking: "0",
                track_cycle: "MONT",
                validity: "365"};

            $scope.list_company = initCompanyData;
            $scope.ddlTrackCycle_disable = true;
            //setTimeout(function () { getUserRights(username); }, 2000);





        }





//        function getUserRights(username){
//
//            loading_add();
//            customerMainService.GetUserRights($http,$q,username)
//                .then(function(data){
//                    var USER_RIGHTS=data;
//                    if (USER_RIGHTS.length > 0) {
//                        for (var i = 0; i < USER_RIGHTS.length; i++) {
//                            var Rights_Val = USER_RIGHTS[i].user_right_id;
//                            if (Rights_Val == 1) {$rootScope.CustMaint=true }
//                            else if (Rights_Val == 2) { $rootScope.Product2=true }
//                            else if (Rights_Val == 3) { $rootScope.ProdMgmt=true }
//                            else if (Rights_Val == 4) { $rootScope.ChildProdMaint=true }
//                            else if (Rights_Val == 5) { $rootScope.ProdCatAssoc=true }
//                            //else if (Rights_Val == 6) { StoreConfig=true }
//                            else if (Rights_Val == 7) { $rootScope.ProdFeatureMgmt=true }
//                            else if (Rights_Val == 8) { $rootScope.CatFilter=true }
//                            else if (Rights_Val == 9) { $rootScope.DealSetup=true}
//                            else if (Rights_Val == 10) { $rootScope.Brand=true }
//                            else if (Rights_Val == 11) { $rootScope.ProdAppr=true }
//                            else if (Rights_Val == 12) { $rootScope.StoreConfig=true }
//                            else if (Rights_Val == 13) { $rootScope.EGiftVoucher=true }
//                            else if (Rights_Val == 14) { $rootScope.MangEGiftVoucher=true }
//                            //else if (Rights_Val == 15) { $("#EGiftVoucher").show(); }
//                            else if (Rights_Val == 16) { $rootScope.OrdView=true }
//                            else if (Rights_Val == 17) { $rootScope.UpldCustPoint=true }
//                            else if (Rights_Val == 18) { $rootScope.AssignUserRights=true }
//                            else if (Rights_Val == 19) { $rootScope.IntimateUsers=true }
//
//                            else if (Rights_Val == 20) { $rootScope.MigrateStore=true}
//                            else if (Rights_Val == 21) { $rootScope.EGiftVouReport=true }
//                            else if (Rights_Val == 22) { $rootScope.PromoMgmt=true }
//                            else if (Rights_Val == 23) { $rootScope.ChngPin=true}
//
//                            else if (Rights_Val == 24) { $rootScope.OrdTrack=true }
//                            else if (Rights_Val == 25) { $rootScope.liDailySalesWiseReport=true }
//                            else if (Rights_Val == 26) { $rootScope.liUserPointsDetail=true }
//                            else if (Rights_Val == 27) { $rootScope.wirehouse=true}
//                            // else if (Rights_Val == 28) { $scope.Store=true }
//                        }
//
//                    }
//
//                    loading_cut();
//                },
//                function () {
//                    //Display an error message
//                    $scope.error = error;
//
//                });
//
//        }

        function  get_all_company() {


          //  loading_add();

            UtilService.show_loader();
            customerMainService.get_all_company($http,$q)

                .then(function (data) {

                    $scope.list = data;

                  //  loading_cut();


                    UtilService.hide_loader();

//                    $scope.show_cond='s';
//                    alert(log);

                },
                function () {
                    //Display an error message
                    $scope.error = error;

                });

        }

        function details_by_company_name(){

          //  loading_add();


            UtilService.show_loader();

            $scope.company_name;

            customerMainService.details_by_company_name($http, $q, $scope.company_name)
                .then(function (data) {

                    NotificationService.change_old_data("change_to_company", data[0])
                    $scope.list_company =  data[0];
                    comapany_details = data[0] ;
                    localStorageService.set('comp_details',{});
                    localStorageService.add('comp_details',comapany_details);
                    if(data.length>0) {
                        if ($scope.list_company.sales_tracking == "1") {
                            $scope.ddlTrackCycle_disable = false;
                        }
                        else {
                            $scope.ddlTrackCycle_disable = true;
                        }
                    }

                   // loading_cut();

                    UtilService.hide_loader();

                },
                function () {
                    //Display an error message
                    $scope.error = error;

                });


        }


//        function company_details(company)
//        {
//
//            customerMainService.details_by_company_name($http, $q,company )
//                .then(function (data) {
//
//                 var details =  data[0];
//
//                },
//                function () {
//                    //Display an error message
//                    $scope.error = error;
//
//                });
//
//
//        }


        $scope.SalesTracking_click=function(val){
            $scope.ddlTrackCycle_MONT = true;
            if (val == '1') {
                $scope.ddlTrackCycle_disable = false;
            }
            else {
                $scope.ddlTrackCycle_disable = true;
            }
        }

        $scope.GenRootUrl=function(){
            var CMCompanyName=$scope.list_company.name;
            CMCompanyName=RootStart + CMCompanyName + RootEnd;
            $scope.list_company.root_url_1=CMCompanyName;
        }
        $scope.isNumber=function(charCode){

            if (charCode > 31 && (charCode <= 45 || charCode == 47 || charCode > 57)) {
                event.preventDefault();
            }
        }
        /*function company_validate(){
         var  company_name= $.trim($scope.list_company.name);
         var  email_id= $.trim($scope.list_company.emailid);
         var  inr_point_ratio= $.trim($scope.list_company.inr_point_ratio);
         var  max_point_range= $.trim($scope.list_company.point_range_max);
         var  min_point_range= $.trim($scope.list_company.point_range_min);
         var  root_url_1= $.trim($scope.list_company.root_url_1);

         if(company_name=="" || company_name==null){
         alert('Please enter comapny name');
         return false;
         }
         if(email_id=="" || email_id==null){
         alert('Please enter email_id');
         return false;
         }
         if(inr_point_ratio=="" || inr_point_ratio==null){
         alert('Please enter  inr point ratio');
         return false;
         }
         if(max_point_range=="" || max_point_range==null){
         alert('Please enter  max point range');
         return false;
         }
         if(min_point_range=="" || min_point_range==null){
         alert('Please enter  min point range');
         return false;
         }
         if(root_url_1=="" || root_url_1==null){
         alert('Please enter  root url');
         return false;
         }

         }*/


        $scope.save_company=function() {

            var  CMCompanyName= $scope.list_company.name;
          //  var  CMCompanyName= $scope.company_name;
            var  email_id= $scope.list_company.emailid;
            var  inr_point_ratio= $scope.list_company.inr_point_ratio;
            var  max_point_range= $scope.list_company.point_range_max;
            var  min_point_range= $scope.list_company.point_range_min;
            var  root_url_1= $scope.list_company.root_url_1;

            if(CMCompanyName=="" || CMCompanyName==null){
                alert('Please enter company name');
                return false;
            }


            var i = 0;
            for(var j=0; j<$scope.list.length; j++){
                if (CMCompanyName == $scope.list[j].name) {
                    i++;

                }
            }

           // var CompanyEntryType = $scope.list_company.id;

            var CompanyEntryType = $scope.company_name;


            if (CompanyEntryType == 'New Company') {
                if (i != 0) {
                    alert('You Have Already Entered This Company Name');
                    return false;
                }
            }

            if(email_id=="" || email_id==null){
                alert('Please enter email_id');
                return false;
            }
            if(inr_point_ratio=="" || inr_point_ratio==null){
                alert('Please enter  inr point ratio');
                return false;
            }
            if(max_point_range=="" || max_point_range==null){
                alert('Please enter  max point range');
                return false;
            }
            if(min_point_range=="" || min_point_range==null){
                alert('Please enter  min point range');
                return false;
            }
            if(root_url_1=="" || root_url_1==null){
                alert('Please enter  root url');
                return false;
            }

//            if(CompanyEntryType!=0){
              //  if(CompanyEntryType != 'New Company'){

            var collection_name = "";

            function get_audit_data(collection_name,previous_data,new_data) {

                var log_info = {
                    "app_id": "dalmia_admin_data",
                    "secret": "a1844750-accf-451b-bf6f-0bc997b974cc",
                    "db_name": "annectos_prod",
                    "collection_name": collection_name,
                    "field_name": "",
                    "business_id": "dalmia_admin",
                    "business_id_name": "dalmia",
                    "old_data":previous_data,
                    "new_data":new_data ,
                    "user": user_info
                };

                return log_info;
            }

              if(i == 1)
               {

                   var collection_name = "company";

                   var company_name = $scope.list_company.name;

                  var previous_data =  localStorageService.get('comp_details');

                    var new_data = $scope.list_company;


                  var info_data = get_audit_data(collection_name,previous_data,new_data);



                customerMainService.update_company($http, $q,$scope.list_company )
                    .then(function (data) {

                    customerMainService.insert_audit_data($http, $q,info_data)
                        .then(function (data) {

                        },
                        function () {
                            //Display an error message
                            $scope.error = error;

                        });


                        alert('Record Updated Successfully');
                        get_all_company();
                       // CompanyNewView();
                    },
                    function () {
                        //Display an error message
                        $scope.error = error;

                    });
            }
            else {

//                  var previous_data = "";
//                  var new_data = $scope.list_company;
//
//                  var log_info = get_audit_data(collection_name,previous_data,new_data);
//
//             customerMainService.insert_audit_data($http, $q,log_info)
//                 .then(function (data) {
////                             alert('Record Saved Successfully');
////                             get_all_company();
////                             CompanyNewView();
//                 },
//                 function () {
//                     //Display an error message
//                     $scope.error = error;
//
//                 });


                customerMainService.save_company($http, $q, $scope.list_company)
                    .then(function (data) {
                        alert('Record Saved Successfully');
                        get_all_company();
                        CompanyNewView();
                    },
                    function () {
                        //Display an error message
                        $scope.error = error;

                    });

            }

        }
        $scope.cancel_company=function(){
            CompanyNewView();
        }



        function CompanyNewView(){
            $scope.list_company = $scope.list_company.constructor();
            $scope.list_company.id = "0";
            $scope.list_company.sales_tracking = "0";
            $scope.list_company.track_cycle = "MONT";
            $scope.list_company.validity = "365";
            $scope.ddlTrackCycle_disable = true;
            $scope.SalesTracking_Checked = true;
        }






    });

