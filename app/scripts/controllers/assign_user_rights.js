/**
 * Created by developer7 on 9/10/2014.
 */

'use strict';

angular.module('annectosadmin')
    .controller('assign_user_rightsCtrl', function ($scope,$rootScope,$location,$state,$http,$q,usersService,UtilService) {


        var RootStart = "http://";
        var RootEnd = ".annectos.net";

        var UserData = {
            Id: '',
            user_name: '',
            company_id: '',
            password: '',
            user_type: ''
        };

        init();
        function init() {

            UtilService.change_header($state.current.data.header_label);
            getUserData();
        }

        function getUserData(){
            usersService.getUser_For_Rights($http,$q)
                .then(function (data) {
                    $scope.usrsList = data;
                },
                function () {
                    //Display an error message
                    $scope.error = error;

                });
        }

        $scope.PopulateRights = function(USER_ID, RowID){
            //console.log('USER_ID>>>' + USER_ID);
            //RowID =  RowID+1;
            $scope.user_id = USER_ID;
            populateRightLists(USER_ID);
            $scope.selectedRow = RowID;

        }

        function populateRightLists(USER_ID){
            usersService.getUserRights($http,$q,USER_ID, 1)
                .then(function (data) {
                    $scope.availableRights_list = data;
                    /*if($scope.availableRights_list != null && $scope.availableRights_list.length>0){
                        $scope.sel_availableRights = $scope.availableRights_list[0].user_right_id;
                    }*/
                },
                function () {
                    //Display an error message
                    $scope.error = error;

                });

//        function populateRightLists(USER_ID){
//            usersService.getUserRights($http,$q,USER_ID)
//                .then(function (data) {
//                    $scope.availableRights_list = data;
//                    /*if($scope.availableRights_list != null && $scope.availableRights_list.length>0){
//                     $scope.sel_availableRights = $scope.availableRights_list[0].user_right_id;
//                     }*/
//                },
//                function () {
//                    //Display an error message
//                    $scope.error = error;
//
//                });

            usersService.getUserRights($http,$q,USER_ID,2)
                .then(function (data) {
                    $scope.assignedRights_list = data;
                   /* if($scope.assignedRights_list != null && $scope.assignedRights_list.length>0){
                        $scope.sel_assignedRights = $scope.assignedRights_list[0].user_right_id;
                    }*/
                },
                function () {
                    //Display an error message
                    $scope.error = error;

                });
        }

        $scope.EditUserRecord = function(userSel){
            $scope.user_id = userSel.Id;
            $scope.user_name = userSel.user_name;
            $scope.password = userSel.password;
            $scope.repassword = userSel.password;
            $scope.sel_comp = userSel.company_id;
            $scope.user_type = userSel.user_type;
            $scope.adduserPage = true;
        }

        $scope.addUserDetail = function(){
            $scope.user_id = '';
            $scope.user_name = '';
            $scope.password = '';
            $scope.repassword = '';
            $scope.sel_comp = 82;
            $scope.user_type = '';
            $scope.adduserPage = true;
        }


        $scope.cancelUser= function(){
            $scope.adduserPage = false;
        }

        $scope.AssignRights = function(){
            var Rights_ID = $scope.sel_availableRights;
            if (Rights_ID == null || Rights_ID == '') {
                alert('Please Select Available Rights');
                return false;
            }
            $scope.sel_availableRights = '';
            usersService.postAssignRights($http,$q,Rights_ID,$scope.user_id)
                .then(function (data) {
                    alert('Rights Assigned Successfully.');
                    populateRightLists($scope.user_id);
                },
                function (error) {
                    //Display an error message
                    $scope.error = error;

                });
        }

        $scope.DeleteAssignRights = function(){
            var Rights_ID = $scope.sel_assignedRights;
            if (Rights_ID == null || Rights_ID == '') {
                alert('Please Select Assigned Rights');
                return false;

            }
            $scope.sel_assignedRights = '';
            usersService.postDeleteAssignRights($http,$q,Rights_ID,$scope.user_id)
                .then(function (data) {
                    alert('Rights Deleted Successfully.');
                    populateRightLists($scope.user_id);
                },
                function (error) {
                    //Display an error message
                    $scope.error = error;

                });
        }

        function UserValidation() {
            UserData.Id = $scope.user_id;
            var txtUserName = $scope.user_name;
            if (txtUserName == null || txtUserName == "") {
                alert('Please Enter User Name');
                return false;
            }
            UserData.user_name = txtUserName;
            var txtUserPwd = $scope.password;
            if (txtUserPwd == null || txtUserPwd == "") {
                alert('Please Enter Password');
                return false;
            }
            UserData.company_id = $scope.sel_comp;
            //UserData.company_id = txtUserPwd;
            var txtRePassword = $scope.repassword;
            if (txtRePassword == null || txtRePassword == "") {
                alert('Please Enter Re Password');
                return false;
            }

            if (txtUserPwd != txtRePassword) {
                alert('Both Password Should Be Same');
                return false;
            }

            UserData.password = txtRePassword;

            var txtUserType = $scope.user_type;
            /*if (txtUserType == null || txtUserType == "") {
                alert('Please Enter User Type');
                return false;
            }*/
            UserData.user_type = txtUserType;
        }

        $scope.SaveUser = function(){
            if (UserValidation() == false) {
                return false;
            };

          if(UserData.Id == null || UserData.Id == "")
           {

            usersService.SaveUserEntry($http,$q,UserData)
                .then(function (data) {
                    alert('Record Saved Successfully.');
                    getUserData();
                    $scope.adduserPage = false;
                },
                function (error) {
                    //Display an error message
                    $scope.error = error;

                });
           }


        else
        {
            usersService.UpdateUserEntry($http,$q,UserData)
                .then(function (data) {
                    alert('Record Updated Successfully.');
                    getUserData();
                    $scope.adduserPage = false;
                },
                function (error) {
                    //Display an error message
                    $scope.error = error;

                });


        }
        }

    });