/**
 * Created by developer7 on 9/9/2014.
 */
'use strict';

angular.module('annectosadmin')
    .controller('ordermanagementCtrl', function ($scope,$rootScope,$location,$http,$q,$stateParams,orderService,commonService,localStorageService,$state,UtilService) {


        var RootStart = "http://";
        var RootEnd = ".annectos.net";

        init();
        function init() {
            //'sel_status': $scope.sel_status, 'sel_order': $scope.sel_order, 'from_date': $scope.from_date, 'to_date': $scope.to_date

            UtilService.change_header($state.current.data.header_label);
            get_all_company();

        }


        $scope.selectcolumn = function(row) {
            $scope.selectedRow = row;
        };



        function getDateFormat(dateVal){

            var FistDate = new Date(dateVal);
            var dd = FistDate.getDate();
            var mm = FistDate.getMonth()+1; //January is 0!

            var yyyy = FistDate.getFullYear();
            if(dd<10){
                dd='0'+dd
            }
            if(mm<10){
                mm='0'+mm
            }
            var today =mm+'/' + dd+'/'+yyyy;
            return today;

        }



        ///////////// Disable weekend selection


        $scope.disabled = function(date, mode) {
            return ( mode === 'day' && ( date.getDay() === 0 || date.getDay() === 6 ) );
        };

        $scope.toggleMin = function() {
            $scope.minDate = $scope.minDate ? null : new Date();
        };
        $scope.toggleMin();

        $scope.openStart = function($event) {
            $event.preventDefault();
            $event.stopPropagation();
            $scope.opened_start = true;
        };

        $scope.openEnd = function($event) {
            $event.preventDefault();
            $event.stopPropagation();
            $scope.opened_end = true;
        };

        $scope.dateOptions = {
            formatYear: 'yy',
            startingDay: 1
        };

        $scope.initDate = new Date('2016-15-20');
        $scope.formats = ['dd-MMMM-yyyy', 'yyyy/MM/dd', 'dd.MM.yyyy', 'shortDate'];
        $scope.format = $scope.formats[0];



    /////////////////////////////////////////////////////////////


        function  get_all_company() {


            UtilService.show_loader();

            commonService.postPopulateCompany($http,$q)

                .then(function (data) {
                    if(data.length>0){
                        data[0].name = "Select Company";
                    }

                    UtilService.hide_loader();
                    $scope.store_list = data;



                    if(localStorageService.get('sel_status')){
                        $scope.sel_status = localStorageService.get('sel_status');
                        $scope.sel_order = localStorageService.get('sel_order');
                        $scope.from_date = localStorageService.get('from_date');
                        $scope.to_date = localStorageService.get('to_date');
                        localStorageService.set('sel_status',null);
                        localStorageService.set('sel_order',null);
                        localStorageService.set('from_date',null);
                        localStorageService.set('to_date',null);
                        PopOrderDetails();
                    }else{
                        $scope.sel_status = -1;
                        $scope.sel_order = 0;
                    }
                },
                function () {
                    //Display an error message
                    $scope.error = error;

                });

        }

        $scope.SearchOrder = function() {
            PopOrderDetails();
        }
        function PopOrderDetails() {

            var OrdStore = $scope.sel_order;

            if(OrdStore==0)
            {
                OrdStore=null;

            }
            var OrdTyp = $scope.sel_status;

            var txtOrdFrom = $scope.from_date;
            var from_date =null;
            if (txtOrdFrom == '' || txtOrdFrom == null || typeof txtOrdFrom ==='undefined' ) {
                txtOrdFrom = null;
            }
            else
            {
                from_date = getDateFormat(txtOrdFrom);
            }

            var txtOrdTo = $scope.to_date;
            var to_date =null;
            if (txtOrdTo == '' || txtOrdTo == null || typeof txtOrdTo ==='undefined') {
                txtOrdTo = null;
            }else{
                to_date = getDateFormat(txtOrdTo);
            }

            UtilService.show_loader();

            orderService.get_Order_History($http, $q, OrdStore, OrdTyp, from_date, to_date)
                .then(function (data) {
                    $scope.orderList = data;

                    UtilService.hide_loader();

                },
                function () {
                    //Display an error message

                    UtilService.hide_loader();

                    $scope.error = error;
                });
        }


        $scope.ClearSearchFields = function(){
            $scope.sel_order = 0;
            $scope.sel_status= -1;
            $scope.from_date = '';
            $scope.to_date = '';
            $scope.orderList = null;
        }

        $scope.ViewOrder = function(order_id){
            $state.go('main.viewOrderDetail',{'order_id':order_id});
            //, 'sel_status': $scope.sel_status, 'sel_order': $scope.sel_order, 'from_date': $scope.from_date, 'to_date': $scope.to_date
            localStorageService.set('sel_status',$scope.sel_status);
            localStorageService.set('sel_order',$scope.sel_order);
            localStorageService.set('from_date',$scope.from_date);
            localStorageService.set('to_date',$scope.to_date);
        }

        $scope.exportExcel = function(){
            var OrdStore = $scope.sel_order;
            if (OrdStore == 0) {
                OrdStore = null;
            }
            else {

                for(var i=0; i<$scope.store_list.length; i++){
                    if($scope.store_list[i].id == OrdStore){
                        OrdStore = $scope.store_list[i].name;
                        break;
                    }
                }
            }



            var OrdTyp = $scope.sel_status;
            var txtOrdFrom =  $scope.from_date;
            if (txtOrdFrom == '' || txtOrdFrom == null) {
                txtOrdFrom = null;
            }
            var txtOrdTo = $scope.to_date;
            if (txtOrdTo == '' || txtOrdTo == null) {
                txtOrdTo = null;
            }

            orderService.getOrder_Export_To_Excel($http, $q, OrdStore, OrdTyp, txtOrdFrom, txtOrdTo)
                .then(function (data) {
                    window.open('http://localhost:63342/annectos.admin/app/' + data + ".csv", "_blank");

                },
                function () {
                    //Display an error message
                    $scope.error = error;
                });

        }

    });