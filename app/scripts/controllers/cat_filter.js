/**
 * Created by DRIPL_1 on 09/06/2014.
 */

'use strict';


angular.module('annectosadmin')
    .controller('catfilterCtrl', function ($scope,$rootScope,$location,$http,$q,catfilterService,localStorageService,$state,UtilService) {
        $scope.awesomeThings = [
            'HTML5 Boilerplate',
            'AngularJS',
            'Karma'
        ];

         var user_info;
        $scope.filter_data = [];
        $scope.copyFilter_disabled = true;

        var rootCaturl = 'http://54.251.138.26/index.html#/cat/';
        init();
        function init() {

            UtilService.change_header($state.current.data.header_label);
            user_info = localStorageService.get("admin_user_info").user_name;
            PopCatFilterList();
            $scope.CFCategoryName = "";
            $scope.CFCatDisplayName = "";
            setTimeout(function () {
                getCategoryFilter();
            }, 1500);

        }




        function PopCatFilterList() {
            //var CFCategoryList=$scope.CFCategoryList;
          //  loading_add();

            UtilService.show_loader();
            catfilterService.get_cat_filter_list($http, $q)
                .then(function (data) {
                    $scope.list_filter = data;
//                    loading_cut();
                    UtilService.hide_loader();

                },
                function () {
                    //Display an error message
                    $scope.error = error;

                })

        }

        $scope.$watch('CFCategoryList', function () {
            getCategoryFilter();
        });

        function getCategoryFilter() {
            var Category_ID = $scope.CFCategoryList;
            if (Category_ID == 0 || Category_ID == null || typeof Category_ID ===  "undefined") {
                $scope.CatFilterLink_show = false;
                $scope.CFCategoryName = "";
                $scope.CFCatDisplayName = "";
                $scope.filter_data = [];
            }
            else {
                $scope.CatFilterLink_URL = rootCaturl + Category_ID;
                $scope.CatFilterLink_show = true;
            }

            if (Category_ID != 0 && Category_ID != null || typeof Category_ID !==  "undefined") {
                catfilterService.details_by_category_name($http, $q, Category_ID)

                    .then(function (data) {

                        //$scope.grid_disable=false;
                        $scope.filter_data = data[0].filters;
                        $scope.CFCategoryName = data[0].Name;
                        $scope.CFCatDisplayName = data[0].displayname;
                        //  $scope.grid_hide=false;

                    },
                    function () {
                        //Display an error message
                        $scope.error = error;

                    });
            }
        }

        $scope.back_to_store = function () {
            $state.go("main.catMgmt");
        }

        $scope.pop_cat_filter = function()
        {
            $scope.cat_edit_popup = false;

        }


        function populate_filter_value(ddldata, filter_name) {
            for (var i = 0; i < ddldata.length; i++) {
                if (ddldata[i].name == filter_name) {
                    var tblsrc = [];
                    for (var j = 0; j < ddldata[i].values.length; j++) {
                        var dataSrc = {
                            id: ddldata[i].values[j],
                            name: ddldata[i].values[j]
                        }
                        tblsrc[j] = dataSrc;


                    }
                }
            }
            $scope.cat_filter_name = ddldata[0].name;
            if (typeof tblsrc === "undefined") {

            }
            else {
                $scope.cat_list = tblsrc;


            }
        }

        function get_cat_filter_details(filter_name) {
            var Category_ID = $scope.CFCategoryList;
            catfilterService.get_cat_filter($http, $q, Category_ID)
                .then(function (data) {
                    populate_filter_value(data[0].filters, filter_name);


                },
                function () {
                    //Display an error message
                    $scope.error = error;

                });
        }

        $scope.add_filter_values = function () {

            var txtValue = $scope.filters_name;

            if (txtValue == "" || txtValue == null || typeof txtValue === "undefined") {
                alert('Please Enter Filter Value');
                return false;
            }

            var FilterValues = $scope.cat_filter_name;

            var list = $scope.cat_list;

            for(var i=0;i<list.length;i++)
            {
                var name = list[i].name;
                if(name == txtValue)
                {

                    alert('Same name is not allowed');

                    return false;

                }

            }


            var DataSrc = {
                id: txtValue,
                name: txtValue
            }
            var tableSrc =[];
            tableSrc = list;
            tableSrc.push(DataSrc);
            $scope.cat_list = tableSrc;

            $scope.filters_name ='';
        }

        $scope.add_filter_value = function (filter_name) {
            get_cat_filter_details(filter_name);
            $scope.cat_filter_name = filter_name;
        }

        $scope.delete_filter = function (FilterName) {
            var del_confirm_val = confirm("Are You Sure You Want To Delete?");

            if (del_confirm_val == true) {

                var FilterVal = [];
                FilterVal[0] = null;
                var Data = {
                    name: FilterName,
                    values: FilterVal
                }
                var FilterData = [];
                FilterData[0] = Data;

                var Category_ID = $scope.CFCategoryList;

              var q = { id: Category_ID, filters: FilterData };
                catfilterService.delete_cat_filter($http, $q, q)
                    .then(function (data) {
                        alert('Record Deleted Successfully');
                        getCategoryFilter();
                     //   get_cat_filter_details(FilterName);

                    },
                    function () {
                        //Display an error message
                        $scope.error = error;

                    });
            }
        }

//        $scope.copy_filter = function()
//        {
//
//        }


        $scope.selectcolumn = function(row) {
            $scope.selectedRow = row;
        };




        $scope.remove_cat_filter = function()
        {
           var filter_name = $scope.sel_cat[0];

            var filter_list = $scope.cat_list;

            for(var j=0;j<filter_list.length;j++)
            {

                if(filter_list[j].name == filter_name )
                {

                    filter_list.splice(j,1);

                }


            }

            $scope.cat_list  = filter_list;

        }

        $scope.category_popup = function()
        {
            $scope.cat_popup = true;

        }

        $scope.cancel = function()
        {
             $scope.cat_popup = false;

        }

        $scope.back = function()
        {
           $state.go('main.category_maintenance');

        }


        $scope.go_to_cat_popup = function(filter_name)
        {
            get_cat_filter_details(filter_name);
            $scope.cat_fil_name = filter_name;

            $scope.cat_edit_popup = true;
        }

        $scope.cancel_pop_up = function()
        {
            $scope.cat_edit_popup = false;

        }



        var collection_name = "";

        function get_audit_data(collection_name,previous_data,new_data) {

            var log_info = {
                "app_id": "dalmia_admin_data",
                "secret": "a1844750-accf-451b-bf6f-0bc997b974cc",
                "db_name": "annectos_prod",
                "collection_name": collection_name,
                "field_name": "",
                "business_id": "dalmia_admin",
                "business_id_name": "dalmia",
                "old_data":previous_data,
                "new_data":new_data ,
                "user": user_info
            };

            return log_info;
        }




        $scope.save_filter_data = function(filtername)
        {

             var filter_name = $scope.cat_fil_name;
             var filter_val = [];
             var fil_data = $scope.cat_list;

            for(var i=0;i<fil_data.length;i++)
            {

                var p = fil_data[i].name;
                filter_val.push(p);

            }

             var filter_data = [];

                    var Data = {
                    name: filter_name,
                    values: filter_val
                 }
                   filter_data.push(Data);


            var category_id = $scope.CFCategoryList;

            var a={id:category_id,filters:filter_data};


            var previous_data = "";
            var new_data = a;

            collection_name = "category";

            var log_info = get_audit_data(collection_name,previous_data,new_data);


            catfilterService.insert_cat_filter($http,$q,a)
                .then(function (data) {

                    catfilterService.insert_audit_data($http, $q,log_info)
                        .then(function (data) {

                        },
                        function () {
                            //Display an error message
                            $scope.error = error;

                        });

                    alert("Record Saved Successfully");

                },
                function () {
                    //Display an error message
                    $scope.error = error;

                });





        }


//        $scope.pop_filter_grid = function () {
//
//           var filter = $scope.filter ;
//
//              var filter_data = {
//
//                  name:filter
//
//              };
//
//           var data = $scope.filter_data;
//
//            if(filter == null || filter == "" || filter === "undefined") {
//
//                alert('Please give a filter name' );
//            }
//            else
//            {
//
//                var cat_data = [];
//
//                cat_data = data;
//                cat_data.push(filter_data);
//                $scope.filter_data = cat_data;
//
//
//            }
//
//
//        }


        $scope.pop_filter_grid = function () {


            var filter = $scope.filter ;

            var filter_data = {

                name:filter

            };

            // var data = $scope.filter_data;

            if(filter == null || filter == "" || typeof filter === "undefined") {

                alert('Please give a filter name' );
                return false;
            }

            for (var j = 0; j <  $scope.filter_data.length; j++) {
                if ( $scope.filter_data[j].name == filter) {
                    alert(' Filter Name Is Already Added ');
                    $scope.filter = "";
                    return false;
                }
            }


            // else
            //  {



            //  if(data != null ||  data != "" || data !==  typeof undefined )
            //   {

            // cat_data = $scope.filter_data;
            //   }

            var cat_data = [];
            cat_data = $scope.filter_data;
            cat_data.push(filter_data);
            $scope.filter_data = cat_data;
            $scope.filter = "";


            //  }






        }




    })




