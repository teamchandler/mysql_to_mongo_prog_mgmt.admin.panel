/**
 * Created by DRIPL_1 on 09/06/2014.
 */

'use strict';

/**
 * Created by developer1 on 7/1/2014.
 */
angular.module('annectosadmin')
    .controller('prod_cat_assocCtrl', function  ProductController($scope,$rootScope,$state,$http,$q,commonService,prod_cat_assocService,localStorageService,UtilService) {
        $scope.awesomeThings = [
            'HTML5 Boilerplate',
            'AngularJS',
            'Karma'
        ];

         var user_info;

        init();
        function init(){

            UtilService.change_header($state.current.data.header_label);
            user_info = localStorageService.get("admin_user_info").user_name;
            populate_product_list();
            pop_all_category_tree();
            pop_all_category_list();
            $scope.prod_name="";
            $scope.prod_desc="";
            $scope.prod_name_disable = true;

        }

        $scope.$watch("sel_prod_assoc",function(){

            if( $scope.sel_prod_assoc != "0" )
            {
               get_product_details();
            }
            else
            {
                $scope.sel_prim_cat="0";
                $scope.prod_name="";
                $scope.prod_desc="";

            }
        })

        function populate_product_list(){
          //  loading_add();

            UtilService.show_loader();

            commonService.get_all_product($http,$q)
                .then(function (data) {
                    $scope.prod_assc=data;
                   // loading_cut();

                    UtilService.hide_loader();

                },
                function () {
                    //Display an error message
                    $scope.error = error;

                });
        }

        function pop_all_category_tree(){
           // loading_add();


            UtilService.show_loader();

            prod_cat_assocService.get_category_tree($http,$q)
                .then(function (data) {
                    $scope.prod_cat_assc_list=data;
                  //  loading_cut();
                    UtilService.hide_loader();

                },
                function () {
                    //Display an error message
                    $scope.error = error;

                });
        }

        function pop_all_category_list(){



            UtilService.show_loader();

            prod_cat_assocService.get_category_list($http,$q)
                .then(function (data) {
                    $scope.prim_cat=data;


                    UtilService.hide_loader();
                },
                function () {
                    //Display an error message
                    $scope.error = error;

                });
        }
        function get_product_details(){
           // loading_add();

            UtilService.show_loader();

            var Product_ID=$scope.sel_prod_assoc;

            if(Product_ID!=0){
                commonService.getProductDetails($http,$q,Product_ID)
                    .then(function (data) {

                        localStorageService.set('get_product_details',{});
                        localStorageService.add('get_product_details',data[0]);

                        if(data[0] != null)
                        {
                        $scope.prod_name = data[0].Name;
                        $scope.prod_desc=data[0].description;
                        $scope.cat_id = data[0].cat_id;

                        var arr_cat_id = [];
                        for(var i=0;i< $scope.cat_id.length;i++)
                        {
                            var id = $scope.cat_id[i].cat_id;

                            arr_cat_id.push(id);
                        }

                         $scope.select_id=data[0].cat_id;


                        $scope.category_list = {ids: {}};

                        angular.forEach(arr_cat_id, function(category){
                            $scope.category_list.ids[category] = true;
                        });


                       // $scope.category_list = arr_cat_id ;

                        $scope.sel_prim_cat=data[0].parent_cat_id;

                        }


                    },
                    function () {
                        //Display an error message
                        $scope.error = error;

                    });
            }
            else{
                $scope.prod_name="";
                $scope.prod_desc="";
            }

           // loading_cut();

            UtilService.hide_loader();

        }

        $scope.cancel=function(){
            $scope.sel_prim_cat="0";
            $scope.prod_name="";
            $scope.prod_desc="";

        }
        $scope.back_store=function(){
            $state.go("main.catMgmt");
        }


        $scope.select_id = [];

        $scope.get_id = function(id)
        {
            var push_cond=true;


            var selected_id = $scope.select_id;
            if(selected_id!=null && selected_id.length>0) {

                for (var i = selected_id.length; i--;) {
                    if (selected_id[i].cat_id == id) {
                        push_cond=false;
                        selected_id.splice(i, 1);
                    }
                }

                if(push_cond==true){
                    $scope.select_id.push({"cat_id" : id });
                }


            }
            else{

                $scope.select_id.push({"cat_id" : id });
            }

        }

        function get_pca_data(){

            var Product_ID=$scope.sel_prod_assoc;

            var Parent_Cat_ID=$scope.sel_prim_cat;

            var arrID = [];

            for (var i = 0; i < $scope.select_id.length; i++) {

                var cat_id = {
                    cat_id: $scope.select_id[i].cat_id
                }
                arrID.push(cat_id);

            }

             $scope.category = {};
//            for(var i = 0;i<$scope.category.length ; i++)
//            {
//                var a = $scope.category_list[i];
//                cat_id.push(a);
//
//            }



            if (Parent_Cat_ID == "0" || Parent_Cat_ID == null) {
                alert('Please Select Primary Category');
                return false;
            }

            return {
                id: Product_ID,
                cat_id: arrID,
                parent_cat_id: Parent_Cat_ID
            };

        }


        var collection_name = "";

        function get_audit_data(collection_name,previous_data,new_data) {

            var log_info = {
                "app_id": "dalmia_admin_data",
                "secret": "a1844750-accf-451b-bf6f-0bc997b974cc",
                "db_name": "annectos_prod",
                "collection_name": collection_name,
                "field_name": "",
                "business_id": "dalmia_admin",
                "business_id_name": "dalmia",
                "old_data":previous_data,
                "new_data":new_data ,
                "user": user_info
            };

            return log_info;
        }


        $scope.save_prod_cat=function(){

           // loading_add();

            UtilService.show_loader();

            if(get_pca_data() == false)
            {
                 UtilService.hide_loader();
                return false;

            }


            var pca_data = get_pca_data();
//            if(pca_data.parent_cat_id == '' || pca_data.parent_cat_id == null || typeof pca_data.parent_cat_id === "undefined" )
//            {
//
//                alert('Please Select Primary Category');
//                return false;
//            }

            var previous_data = localStorageService.get('get_product_details');
            var new_data = pca_data;

            var log_info = get_audit_data(collection_name,previous_data,new_data);


            prod_cat_assocService.insert_pca($http,$q,pca_data)
                .then(function (data) {
                    prod_cat_assocService.insert_audit_data($http, $q,log_info)
                        .then(function (data) {
//                             alert('Record Saved Successfully');
//                             get_all_company();
//                             CompanyNewView();
                        },
                        function () {
                            //Display an error message
                            $scope.error = error;

                        });
                    $scope.pca_detail=data[0];

                   // loading_cut();


                    UtilService.hide_loader();

                    alert("Record Saved Successfully");

                },
                function () {
                    //Display an error message
                    $scope.error = error;

                });


        }

//        $scope.cancel = function()
//        {
//            $state.go('main.category_maintenance');
//        }

        $scope.get_cat_id = function(id)
        {

             alert(id);

        }



    })

