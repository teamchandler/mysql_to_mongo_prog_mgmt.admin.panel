/**
 * Created by gghh on 9/11/2014.
 */
'use strict';

angular.module('annectosadmin')
    .controller('promo_mgmtCtrl', function ($scope,$rootScope,$location,$http,$q,promoMgmtService, commonService,UtilService,$state) {


        var RootStart = "http://";
        var RootEnd = ".annectos.net";


        $scope.$watch('sel_promo', function () {
            Populatepromodetails($scope.sel_promo);
            $scope.disc_type = 0;
        });

        init();
        function init() {

            UtilService.change_header($state.current.data.header_label);
            getPromoDetails();
            get_all_company();
            $scope.btnTransferPromo_disable = true;
        }

        function getPromoDetails(){
            var Promo_Id = $scope.sel_promo;
            if (Promo_Id == null) {
                Promo_Id = "0";
            }

            promoMgmtService.get_all_promo($http,$q,Promo_Id)
                .then(function (data) {
                    if(data.length>0) {
                        data[0].name = "New Promotion";
                    }
                    $scope.listData = data;
                    $scope.sel_promo = 0;
                },
                function (error) {
                    //Display an error message
                    $scope.error = error;

                });


        }

        function  get_all_company() {

            commonService.postPopulateCompany($http,$q)

                .then(function (data) {
                    $scope.listcust = data;
                    $scope.sel_cust=0;
                },
                function () {
                    //Display an error message
                    $scope.error = error;

                });

        }

        function Populatepromodetails(Promotion_ID){
            if (Promotion_ID != null && Promotion_ID != "0") {

                promoMgmtService.get_all_promo($http,$q,Promotion_ID)
                    .then(function (result) {
                        $scope.promo_name = result[0].promo_name;
                        var CompVal = '';
                        for(var i=0; i<$scope.listcust.length; i++){
                            if($scope.listcust[i].name.indexOf(result[0].company)>-1){
                                CompVal = $scope.listcust[i].id;
                                break;
                            }
                        }
                        $scope.sel_cust = CompVal;
                        $scope.min_purchase = result[0].min_purchase;
                        $scope.disc_type = result[0].disc_type;
                        $scope.max_discount = result[0].max_discount;
                        $scope.start_date_val = result[0].start_date;
                        $scope.end_date_val = result[0].end_date;



                        var Curren_Date = new Date();
                        var End_Date = $scope.end_date_val;
                        End_Date = new Date(End_Date);
                        if (Curren_Date > End_Date) {
                            $scope.btnTransferPromo_disable = true;
                        }
                        else {
                            $scope.btnTransferPromo_disable = false;
                        }
                    },
                    function (error) {
                        alert('Error');
                        $scope.error = error;

                    });

            }
            else {
                //$(".Promo").val('');
                $scope.promo_name = '';
                $scope.min_purchase = '';
                $scope.max_discount = '';
                $scope.start_date_val='';
                $scope.end_date_val = '';
                $scope.sel_cust = 0;
                $scope.disc_type = 0;

            }

        }
        /*$scope.today = function() {
            $scope.start_date = new Date();
        };
        $scope.today();*/

        $scope.clear = function () {
            $scope.start_date_val = null;
        };

        // Disable weekend selection
        $scope.disabled = function(date, mode) {
            return ( mode === 'day' && ( date.getDay() === 0 || date.getDay() === 6 ) );
        };

        $scope.toggleMin = function() {
            $scope.minDate = $scope.minDate ? null : new Date();
        };
        $scope.toggleMin();

        $scope.openStart = function($event) {
            $event.preventDefault();
            $event.stopPropagation();
            $scope.opened_start = true;
        };

        $scope.openEnd = function($event) {
            $event.preventDefault();
            $event.stopPropagation();
            $scope.opened_end = true;
        };

        $scope.dateOptions = {
            formatYear: 'yy',
            startingDay: 1
        };

        $scope.initDate = new Date('2016-15-20');
        $scope.formats = ['dd-MMMM-yyyy', 'yyyy/MM/dd', 'dd.MM.yyyy', 'shortDate'];
        $scope.format = $scope.formats[0];

        $scope.SavePromotion = function(){
            var Promotion_ID = $scope.sel_promo;

            var txtPromoName = $scope.promo_name;
            if (txtPromoName == null || txtPromoName == "") {
                alert('Please Enter Promotion Name');
                return false;
            }

            var ddlCustomerStore = $scope.sel_cust;
            if (ddlCustomerStore == null || ddlCustomerStore == "0") {
                alert('Please Select Customer/Store');
                return false;
            }
            var Comp = '';//$.trim($('#ddlCustomerStore :selected').text());
            //comp_list
            for(var i=0; i<$scope.listcust.length; i++){
                if($scope.listcust[i].id == ddlCustomerStore){
                    Comp = $scope.listcust[i].name;
                    break;
                }
            }


            var txtMinPurchase = $scope.min_purchase;
            if (txtMinPurchase == null || txtMinPurchase == "") {
                alert('Please Enter Minimum Purchase');
                return false;
            }

            var ddlDiscType = $scope.disc_type;
            if (ddlDiscType == null || ddlDiscType == "0") {
                alert('Please Select Discount Type');
                return false;
            }

            var txtMaxDiscount = $scope.max_discount;
            if (txtMaxDiscount == null || txtMaxDiscount == "") {
                alert('Please Enter Max Discount');
                return false;
            }

            var txtPromoStartDate = $scope.start_date_val;
            if (txtPromoStartDate == null || txtPromoStartDate == "") {
                alert('Please Choose Start Date');
                return false;
            }

            var txtPromoEndDate = $scope.end_date_val;
            if (txtPromoEndDate == null || txtPromoEndDate == "") {
                alert('Please Choose End Date');
                return false;
            }

            if (DateComparison(txtPromoStartDate, txtPromoEndDate) == false) {
                alert('Start Date Should Be Less Than End Date.');
                return false;
            }

            var PromoStartDate = getDateFormat(txtPromoStartDate);
            var PromoEndDate = getDateFormat(txtPromoEndDate);

            var promo_data = { promo_id: Promotion_ID, promo_name: txtPromoName, company: Comp, min_purchase: txtMinPurchase, disc_type: ddlDiscType, max_discount: txtMaxDiscount, start_date: PromoStartDate, end_date: PromoEndDate };
            promoMgmtService.save_promo($http,$q,promo_data)
                .then(function (data) {
                    if (Promotion_ID == "0") {
                        alert('Record Inserted Successfully');
                    }
                    else {
                        alert('Record Updated Successfully');
                    }

                    $scope.btnTransferPromo_disable = false;
                    $scope.sel_promo = 0;
                    getPromoDetails();
                },
                function (error) {
                    alert('Error');
                    $scope.error = error;

                });


        }

        function DateComparison(Date1, Date2) {

            var FistDate = new Date(Date1);
            var SecondDate = new Date(Date2);

            if (FistDate > SecondDate) {

                return false;

            }
            else {

                return true;

            }

        }

        function getDateFormat(dateVal){
            var FistDate = new Date(dateVal);
            var dd = FistDate.getDate();
            var mm = FistDate.getMonth()+1; //January is 0!

            var yyyy = FistDate.getFullYear();
            if(dd<10){
                dd='0'+dd
            }
            if(mm<10){
                mm='0'+mm
            }
            var today =mm+'/' + dd+'/'+yyyy;
            return today;
        }

        $scope.CancelPromo = function(){
            $scope.promo_name = '';
            $scope.min_purchase = '';
            $scope.max_discount = '';
            $scope.start_date_val='';
            $scope.end_date_val = '';

            $scope.sel_promo = 0;
            $scope.sel_cust = 0;
            $scope.disc_type = 0;
        }

        $scope.TransferPromotion = function(){
            var ddlPromotion = $scope.sel_promo;
            if (ddlPromotion == null || ddlPromotion == "0") {
                alert('Please Select Promotion');
                return false;
            }

            var ddlCustomerStore = $scope.sel_cust;
            if (ddlCustomerStore == null || ddlCustomerStore == "0") {
                alert('Please Select Customer/Store');
                return false;
            }

            var Comp = ''
            for(var i=0; i<$scope.listcust.length; i++){
                if($scope.listcust[i].id == ddlCustomerStore){
                    Comp = $scope.listcust[i].name;
                    break;
                }
            }

            var Curren_Date = new Date();
            var End_Date =  $scope.end_date_val;
            End_Date = new Date(End_Date);
            if (Curren_Date > End_Date) {
                alert("End Date Cannot Be Less Than Today's Date.");
                return false;

            }
//
            promoMgmtService.transfer_promo($http,$q,ddlPromotion,Comp)
                .then(function (data) {
                    alert("Transfer Points To Store Successfully.");
                },
                function (error) {
                    alert('Error');
                    $scope.error = error;

                });
        }
    });