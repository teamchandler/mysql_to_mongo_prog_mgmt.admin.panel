/**
 * Created by developer7 on 9/10/2014.
 */
'use strict';

angular.module('annectosadmin')
    .controller('e_gift_voucherCtrl', function ($scope,$rootScope,$location,$http,$q,eGiftService,commonService,localStorageService,$state) {


        var RootStart = "http://";
        var RootEnd = ".annectos.net";
""
        var eGiftVou_data = {
                            NoOfVou: "",
                            amount: '',
                            validity: '',
                            giver_email: '',
                            rcv_email: '',
                            company: '',
                            event_desc: ''
                            };

        init();
        function init() {
            PopStoreCompany();
            $scope.eGiftVou = eGiftVou_data;
        }

        function PopStoreCompany(){
            commonService.postPopulateCompany($http,$q)

                .then(function (data) {
                    if(data.length>0){
                        data[0].name = "Select Company";
                    }
                    $scope.vou_comp_list = data;
                    $scope.sel_voucomp = 0;
                },
                function () {
                    //Display an error message
                    $scope.error = error;

                });
        }

        $scope.isNumber=function(charCode){

            if (charCode > 31 && (charCode <= 45 || charCode == 47 || charCode > 57)) {
                event.preventDefault();
            }
        }

        $scope.SaveeGift = function(){
            var ddlVouComp = $scope.sel_voucomp;
            var ddlVouCompText = null;

            for(var i=0; i<$scope.vou_comp_list.length; i++){
                if($scope.vou_comp_list[i].id == ddlVouComp){
                    ddlVouCompText = $scope.vou_comp_list[i].name;
                    break;
                }
            }

            if (ddlVouComp == 0 || ddlVouComp == null) {
                alert('Please Select Store Or Company');
                return false;
            }

            if ($scope.eGiftVou.amount == '' || $scope.eGiftVou.amount == null) {
                alert('Please Enter Amount');
                return false;
            }

            var txtNoOfVou = $scope.eGiftVou.NoOfVou;
            if (txtNoOfVou == '' || txtNoOfVou == null) {
                alert('Please Enter No Of Voucher');
                return false;
            }

            var txtEvent = $scope.eGiftVou.event_desc;
            if (txtEvent == '' || txtEvent == null) {
                alert('Please Enter Event/Label');
                return false;
            }

            var txtValidity = $scope.eGiftVou.validity;
            if (txtValidity == '' || txtValidity == null) {
                alert('Please Enter Validity');
                return false;
            }

            var txtFromEmail = $scope.eGiftVou.giver_email;
            if (txtFromEmail == '' || txtFromEmail == null) {
                alert('Please Enter From Email');
                return false;
            }

            var txtToEmail = $scope.eGiftVou.rcv_email;
            if (txtToEmail == '' || txtToEmail == null) {
                alert('Please Enter To Email');
                return false;
            }

            eGiftVou_data = $scope.eGiftVou;
            eGiftVou_data.company = ddlVouCompText;

            eGiftService.SaveGift($http, $q, eGiftVou_data)
                .then(function (data) {
                    alert('Record Saved Successfully.');
                },
                function (error) {
                    alert('Error');
                    $scope.error = error;
                });


        }

        $scope.CancelGift = function(){
            $scope.sel_voucomp = 0;
            $scope.eGiftVou.amount = '';
            $scope.eGiftVou.NoOfVou = '';
            $scope.eGiftVou.event_desc = '';
            $scope.eGiftVou.giver_email = '';
            $scope.eGiftVou.rcv_email = '';
        }

    });