/**
 * Created by Dipanjan on 9/9/2014.
 */

'use strict';

angular.module('annectosadmin')
    .controller('brandCtrl', function ($scope,$rootScope,$location,$http,$q,commonService,$state,brandService,localStorageService,UtilService) {
        $scope.awesomeThings = [
            'HTML5 Boilerplate',
            'AngularJS',
            'Karma'
        ]


        var user_info;
        init()
        function init(){

            UtilService.change_header($state.current.data.header_label);

            user_info=localStorageService.get("admin_user_info").user_name;
            pop_brand_list()
        }

        $scope.$watch('sel_brand', function () {
            get_brand_details();
        });




        function  pop_brand_list() {

            UtilService.show_loader();

            //loading_add();

            commonService.get_all_brand($http,$q)

                .then(function (data) {


                    $scope.brand_list = data;

                   // loading_cut();

                    UtilService.hide_loader();

                },
                function () {
                    //Display an error message
                    $scope.error = error;

                });

        }


        var collection_name = "";

        function get_audit_data(collection_name,previous_data,new_data) {

            var log_info = {
                "app_id": "dalmia_admin_data",
                "secret": "a1844750-accf-451b-bf6f-0bc997b974cc",
                "db_name": "annectos_prod",
                "collection_name": collection_name,
                "field_name": "",
                "business_id": "dalmia_admin",
                "business_id_name": "dalmia",
                "old_data":previous_data,
                "new_data":new_data ,
                "user": user_info
            };

            return log_info;
        }


        function get_brand_details(){
            var Brand_name=$scope.sel_brand;
            if(Brand_name == 'New Brand'|| Brand_name== null ) {

                $scope.brand_name="";
            }
            else{


                brandService.details_by_brand_name($http, $q, Brand_name)
                    .then(function (data) {

                        $scope.brand_name = data[0].name;

                        var brand = $scope.brand_name;

                        localStorageService.set('brand_details',{});
                        localStorageService.add('brand_details',brand);
                    },
                    function () {
                        //Display an error message
                        $scope.error = error;

                    });
            }
        }

        $scope.save_brand=function() {

            var txtBrandName=$scope.brand_name;
            if (txtBrandName == '' || txtBrandName == null) {
                alert('Please Enter Brand Name');
                return false;
            }
            var ddlBrand=$scope.sel_brand;

            if (ddlBrand == "New Brand") {

//                var previous_data = "";
//                var new_data = txtBrandName;
//                collection_name = "brand";
//
//                var log_info = get_audit_data(collection_name,previous_data,new_data);

                brandService.save_brand($http, $q,txtBrandName)
                    .then(function (data) {
                        if (data == "Brand Name Already Exists") {
                            alert(data);
                        }
                        else {

//                            brandService.insert_audit_data($http, $q,log_info)
//                                .then(function (data) {
//
//                                },
//                                function () {
//                                    //Display an error message
//                                    $scope.error = error;
//
//                                });

                            alert('Record Saved Successfully.');
                            $scope.brand_name="";
                            var BrandList=$scope.sel_brand;
                            pop_brand_list("N");
                        }
                    },
                    function () {
                        //Display an error message
                        $scope.error = error;

                    });

            }

            else
            {

                $scope.s ={_id:$scope.sel_brand,name:$scope.brand_name};
                var brand_data = $scope.s;

                var collection_name = "brand";

                var previous_data =  localStorageService.get('brand_details');

                var new_data = brand_data;

                var info_data = get_audit_data(collection_name,previous_data,new_data);

                brandService.update_brand($http,$q,$scope.s)
                    .then(function (data) {
                        if (data == "Brand Name Already Exists") {
                            alert(data);
                        }
                        else {
                            brandService.insert_audit_data($http, $q,info_data)
                                .then(function (data) {

                                },
                                function () {
                                    //Display an error message
                                    $scope.error = error;

                                });
                            alert('Record Updated Successfully.');
                            $scope.brand_name="";
                            //var BrandList=$scope.sel_brand;
                            pop_brand_list("N");
                        }

                    },
                    function () {
                        //Display an error message
                        $scope.error = error;

                    });

            }

        }
        $scope.cancel_brand=function(){
            pop_brand_list("N");
            $scope.brand_name="";
        }
    });
