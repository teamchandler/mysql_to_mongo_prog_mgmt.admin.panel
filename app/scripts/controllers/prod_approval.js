/**
 * Created by Dipanjan on 9/10/2014.
 */


'use strict';

angular.module('annectosadmin')
    .controller('product_approvalCtrl', function ($scope,$rootScope,$location,$http,$q,prod_approvalService,UtilService,$state) {
        $scope.awesomeThings = [
            'HTML5 Boilerplate',
            'AngularJS',
            'Karma'
        ];

        init();
        function init(){
            UtilService.change_header($state.current.data.header_label);
            pop_brand_list();
        }





        $scope.selectcolumn = function(row) {
            $scope.selectedRow = row;
        };


        function  pop_brand_list() {

           // loading_add();

            UtilService.show_loader();

            var flag= "L";
            prod_approvalService.get_all_brand($http,$q,flag)

                .then(function (data) {
                    $scope.prod_brand_list = data;
                //    loading_cut();

                    UtilService.hide_loader();

                },
                function () {
                    //Display an error message
                    $scope.error = error;

                });


        }
        $scope.search_product=function(){
           // loading_add();

            UtilService.show_loader();

            var prod_name=$scope.prod_name;
            var brand = $scope.select_brand;
            var c={Name:prod_name,brand:brand};
            prod_approvalService.search_product_detail($http,$q,c)
                .then(function (data) {
                    $scope.prod_details_data=data;
                    for(var j=0;j<$scope.prod_details_data.length;j++)
                    {
                    $scope.prod_details_data[j].cat_url = cat_service_url+'/'+$scope.prod_details_data[j].id;
                    }

                  //  loading_cut();

                    UtilService.hide_loader();

                },
                function () {
                    //Display an error message
                    $scope.error = error;

                });
        }


        $scope.selected_id=[];

        $scope.select_data = function(id)
        {

            var idx = $scope.selected_id.indexOf(id);

            // is currently selected

            if (idx > -1) {

                $scope.selected_id.splice(idx, 1);

            }

           // is newly selected

            else {

            $scope.selected_id.push(id);

        }


        }

        function get_product_id(){
            try {
                // var ids = jQuery("#ProductGrid").getGridParam('selarrrow');

                if (typeof ids === "undefined") {
                    alert('Please Select Product');
                    return false;
                }
                else {
                    var count = ids.length;
                    if (count == 0) {
                        alert('Please Select Product');
                        return false;
                    }
                    else {
                        var PRODUCT_ID = new Array();
                        for (var i = 0; i < count; i++) {
                            PRODUCT_ID[i] = $('#ProductGrid').jqGrid('getCell', ids[i], 'id');
                        }
                        return PRODUCT_ID;
                    }
                }
            }
            catch (e) {
                return 0;
            }
        }

        function search_product_data(){

            var prod_name=$scope.prod_name;
            var brand = $scope.select_brand;
            var c={Name:prod_name,brand:brand};
            prod_approvalService.search_product_detail($http,$q,c)
                .then(function (data) {
                    $scope.prod_details_data=data;
               //     $scope.prod_details_data.cat_url = cat_service_url;

                },
                function () {
                    //Display an error message
                    $scope.error = error;

                });

        }
        $scope.product_approval=function(){
//            if (get_product_id() == false) {
//
//                return false;
//            }
           // var PRODUCT_IDs = get_product_id();
            var Products = [];
            for (var i = 0; i < $scope.selected_id.length; i++) {
                var prod_ids = {
                    id: $scope.selected_id[i]
                }
                Products[i] = prod_ids;
            }


            prod_approvalService.product_for_approval($http,$q,Products)
                .then(function (data) {
                    alert('Product Approval Successfully');
                  //  $scope.selected_id = "";
                search_product_data();



                },
                function () {
                    //Display an error message
                    $scope.error = error;

                });

        }





    })



