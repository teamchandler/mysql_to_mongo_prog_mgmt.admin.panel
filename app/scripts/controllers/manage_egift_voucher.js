/**
 * Created by developer7 on 9/10/2014.
 */

'use strict';

angular.module('annectosadmin')
    .controller('manage_egift_voucherCtrl', function ($scope,$rootScope,$location,$http,$q,eGiftService,commonService) {


        var RootStart = "http://";
        var RootEnd = ".annectos.net";


        init();
        function init() {
            PopStoreCompany();
            $scope.sel_validity = 0;
        }

        function PopStoreCompany(){
            commonService.postPopulateCompany($http,$q)

                .then(function (data) {
                    if(data.length>0){
                        data[0].name = "Select Company";
                    }
                    $scope.VouSchComp_list = data;
                    $scope.sel_vouComp = 0;
                },
                function () {
                    //Display an error message
                    $scope.error = error;

                });
        }

        $scope.SearchEGiftVoucher = function(){
            var ddlEGiftCompany = $scope.sel_store;
            var StoreName = null;
            for(var i=0; i<$scope.egiftCompany_list.length; i++){
                if($scope.egiftCompany_list[i].id == ddlEGiftCompany){
                    StoreName = $scope.egiftCompany_list[i].name;
                    break;
                }
            }
            if (ddlEGiftCompany == 0 || ddlEGiftCompany == null) {
                alert('Please Select Store Or Company');
                return false;
            }

            var fromdt = "";
            var todt = "";

            eGiftService.get_GV_Voucher_Details($http, $q, StoreName,fromdt,todt)
                .then(function (data) {
                    $scope.vouDtlsList = data;
                },
                function (error) {
                    alert('Error');
                    $scope.error = error;
                });


        }


        $scope.FindVoucherDetail = function(){
            var ddlVouSchComp = $scope.sel_vouComp;
            var StoreName = null;
            for(var i=0; i<$scope.VouSchComp_list.length; i++){
                if($scope.VouSchComp_list[i].id == ddlVouSchComp){
                    StoreName = $scope.VouSchComp_list[i].name;
                    break;
                }
            }
            if (ddlVouSchComp == 0 || ddlVouSchComp == null) {
                alert('Please Select Store Or Company');
                return false;
            }

            var txtEVouCode = $scope.gv_code;
            if (txtEVouCode == '' || txtEVouCode == null) {
                alert('Please Enter An E-Voucher Code');
                return false;
            }

            eGiftService.get_find_voucher_code($http, $q, StoreName,txtEVouCode)
                .then(function (result) {
                    if (result.gift_voucher_code == null) {
                        $scope.voucher_amount = '';
                        $scope.store_name = '';
                        $scope.created_date = '';
                        $scope.vou_validity = '';
                        $scope.vou_status = '';
                    }
                    else {
                        var date = new Date(result.create_date);
                        var Valid_Date = new Date(result.valid_date);
                        var monthNames = ["Jan", "Feb", "Mar", "Apr", "May", "Jun",
                            "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];
                        var VoucherValidity = "Upto" + ' ' + Valid_Date.getDate() + ' ' + monthNames[Valid_Date.getMonth()] + "," + ' ' + Valid_Date.getFullYear();
                        var VoucherCreateDate = date.getDate() + ' ' + monthNames[date.getMonth()] + "," + ' ' + date.getFullYear();

                        $scope.voucher_amount = result.amount;
                        $scope.store_name = result.company;
                        $scope.vou_validity = VoucherValidity;
                        $scope.created_date = VoucherCreateDate;
                        $scope.vou_status =  result.status;
                        $scope.btnVouRedeem_disable = false;
                        $scope.btnChangeValidity_disable = false;
                        if (result.status == "redeemed") {
                            $scope.btnVouRedeem_disable =  true;
                            $scope.btnChangeValidity_disable = true;
                        }
                        $scope.sel_validity = 0;
                    }
                },
                function (error) {
                    alert('Error');
                    $scope.error = error;
                });

        }

        $scope.VoucherRedeem = function(CallFrom){
            var MoreValidity = 0;
            var txtEVouCode = $scope.gv_code;
            if (txtEVouCode == '' || txtEVouCode == null) {
                alert('Please Enter An E-Voucher Code');
                return false;
            }

            if (CallFrom == 2) {
                var ddlMorevalidity = $scope.sel_validity;
                if (ddlMorevalidity == 0 || ddlMorevalidity == null) {
                    alert('Please Select Add More Validity');
                    return false;
                }
                MoreValidity = 30 * ddlMorevalidity;
            }


            eGiftService.redeem_voucher_code($http, $q, txtEVouCode,MoreValidity)
                .then(function (result) {
                    if (CallFrom == 1) {
                        $scope.btnVouRedeem_disable =  true;
                        $scope.btnChangeValidity_disable = true;
                        alert('Voucher Redeemed Successfully.');

                    } else {
                        alert('Changed Validity Successfully.');
                    }
                },
                function (error) {
                    alert('Error');
                    $scope.error = error;
                });

        }

        $scope.CancelSearch = function(){
            $scope.sel_store = 0;
            $scope.vouDtlsList = null;
        }

    });