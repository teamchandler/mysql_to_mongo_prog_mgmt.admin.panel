/**
 * Created by DRIPL_1 on 09/08/2014.
 */

/**
 * Created by developer1 on 7/1/2014.
 */

'use strict';

angular.module('annectosadmin')
    .controller('prod_mainCtrl', function ($scope,$http,$q,prod_mainService,$state,commonService,localStorageService,UtilService) {
        $scope.awesomeThings = [
            'HTML5 Boilerplate',
            'AngularJS',
            'Karma'
        ];

        var rootProdurl = 'http://54.251.138.26/index.html#/prod/';
        var ID;
        var PRODUCT_NAME;
        var search;
        var user_info;


        init();
        function init(){

            UtilService.change_header($state.current.data.header_label);
            user_info=localStorageService.get("admin_user_info").user_name;
            PopulateBrandList();
            PopulateProductList();
            $scope.ProductLink_show = false;
            ClearProdFields();


        }
        $scope.$watch('sel_prod_name', function () {

            if($scope.sel_prod_name != "0")
            {
                get_prod_details();

            }
            else
            {
                ClearProdFields();

            }

        });

        $scope.$watch('selected_brand', function () {
            search_product_data();
        });


        $scope.search_prod=function(){

            //   $state.go("Search-Product");
        }

        $scope.isNumber=function(charCode){

            if (charCode.which > 31 && (charCode.which <= 45 || charCode.which == 47 || charCode.which > 57)) {
                alert('No special character is Allowed');
               // return false;
               event.preventDefault();
            }
        }

        function CalDiscount(){
            var MRP=($scope.mrp)*1;
            var  LIST_PRICE=($scope.list)*1;
            var  CAL=LIST_PRICE/MRP;
            var  DISCOUNT=100*(1-CAL);
            $scope.discount(Math.round(DISCOUNT*100)/100);

        }
        function CalListPrice(){
            var MRP=($scope.mrp)*1;
            var DISCOUNT=($scope.discount)*1;
            var  CAL_AMT=(MRP*DISCOUNT)/100;
            var LIST_PRICE=MRP-CAL_AMT;
            $scope.list(Math.round(LIST_PRICE*100)/100);
        }

        function PopulateBrandList(){
          //  loading_add();

            UtilService.show_loader();

            var Flag= "L";
            prod_mainService.get_all_brand($http,$q,Flag)
                .then(function (data) {
                    $scope.brand_list=data;

                  //  loading_cut();

                    UtilService.hide_loader();

                },
                function () {
                    //Display an error message
                    $scope.error = error;

                });
        }
        function PopulateProductList(){
          //  loading_add();

            UtilService.show_loader();

            prod_mainService.get_all_product($http,$q)
                .then(function (data) {
                    $scope.prod_data=data;
                  //  loading_cut();

                    UtilService.hide_loader();

                },
                function () {
                    //Display an error message
                    $scope.error = error;

                });

        }



        $scope.get_all_clear = function()
        {
            ClearProdFields();

        }




        function ClearProdFields  (){
            $scope.id="";
            $scope.sku="";
            $scope.Name="";
            $scope.sel_brand_name="";
            $scope.description="";
            $scope.shortdesc="";
            $scope.mrp="";
            $scope.list="";
            $scope.discount="";
            $scope.stock="";
            $scope.msp="";
            $scope.reorder="";
            $scope.status=false;
            $scope.child_prod=false;
            $scope.express=false;
            $scope.chkExpress_disable=false;
            $scope.prodsku_disable = false;
            $scope.prodId_disable = false;
            $scope.ProductLink_show=true;
            $scope.btnUpldPrdImg_disable=true;
            $scope.btnBulkProdPrice_disable=true;
            $scope.btnBulkProdStock_disable=true;

        }


        function get_field_clear()
        {

            $scope.id = '';
            $scope.sku = '';
            $scope.Name = '';
            $scope.sel_brand_name = '';
            $scope.description = '';
            $scope.shortdesc = '';
            $scope.reorder = '';
            $scope.prodsku_disable = false;
            $scope.prodId_disable = false;
            $scope.mrp = '';
            $scope.list = '';
            $scope.discount = '';
            $scope.msp = '';
            $scope.stock = '';
            $scope.status = false;
            $scope.child_prod = false;
            $scope.btnBulkProdStock_disable = false;
            $scope.express = false;
            $scope.express = false;


        }

        function get_prod_details(){

          //  loading_add();

            UtilService.show_loader();

            var Product_ID = $scope.sel_prod_name;
            $scope.prod_id = Product_ID;

            if(Product_ID !=0){
              //  child_prod_exist(Product_ID);
                var url=rootProdurl+Product_ID;
                $scope.ProductLink_show=true;
                $scope.btnUpldPrdImg_disable=true;
                $scope.btnBulkProdPrice_disable=true;
                $scope.btnBulkProdStock_disable=true;
                prod_mainService.get_product_details($http,$q,$scope.prod_id)
                    .then(function (data) {
                        var prod_details = data[0];
                        localStorageService.set('prod_details',{});
                        localStorageService.add('prod_details',prod_details);

                        if(data[0] != null) {
                            $scope.id = data[0].id;
                            $scope.sku = data[0].sku;
                            $scope.Name = data[0].Name;
                            $scope.sel_brand_name = data[0].brand;
                            $scope.description = data[0].description;
                            $scope.shortdesc = data[0].shortdesc;
                            $scope.reorder = data[0].Reorder_Level;
                            $scope.prodsku_disable = true;
                            $scope.prodId_disable = true;
                            $scope.mrp = data[0].price.mrp;
                            $scope.list = data[0].price.list;
                            $scope.discount = data[0].price.discount;
                            $scope.msp = data[0].price.min;
                            $scope.stock = data[0].stock;
                            if (data[0].active == 0) {
                                $scope.status = true;
                            }
                            else {
                                $scope.status = false;
                            }
                            if (data[0].have_child == "0") {
                                $scope.child_prod = false;
                                $scope.btnBulkProdStock_disable = false;
                                $scope.express = false;
                            }
                            else {
                                $scope.child_prod = true;
                                $scope.btnBulkProdStock_disable = true;
                                $scope.express = true;
                            }
                            if (data[0].Express == "0" || data[0].Express == "BsonNull" || data[0].Express == null) {
                                $scope.express = false;
                            }
                            else {
                                $scope.express = true;
                            }
                        }


                      //  get_field_clear();

                      //  loading_cut();

                        UtilService.hide_loader();




                    },
                    function () {
                        //Display an error message
                        $scope.error = error;

                    });
            }
            else{

               // loading_cut();

                UtilService.hide_loader();
                ClearProdFields();
                $scope.prodId_disable=false;
                $scope.prodsku_disable=false;
                $scope.child_prod=false;

            }

        }

//           function child_prod_exist(Product_ID){
//            prod_mainService.child_prod_exist($http,$q,Product_ID)
//                .then(function (data) {
//                    if(data==true){
//                        $scope.chkHaveChild_disable=true;
//                    }
//                    else{
//                        $scope.chkHaveChild_disable=false;
//
//                    }
//                },
//                function () {
//                    //Display an error message
//                    $scope.error = error;
//
//                });
//        }



        function search_product_data(){

            var prod_name=$scope.prod_name;
            var brand = $scope.selected_brand;
            var c={Name:prod_name,brand:brand};
            prod_mainService.search_product_detail($http,$q,c)
                .then(function (data) {
                    $scope.prod_details_data=data;
                    //     $scope.prod_details_data.cat_url = cat_service_url;

                },
                function () {
                    //Display an error message
                    $scope.error = error;

                });

        }


        $scope.search = function()
        {

            $scope.search_popup = true;

        }

        $scope.search_prod_data = function()
        {

            var txtSrchProdSKU = $scope.prod_sku;
            var txtSrchProdId = $scope.prod_id;
            var txtSrchProdName = $scope.prod_name;
            var ddlSrchBrand = $scope.prod_brand;

            if(txtSrchProdSKU == undefined && txtSrchProdId == undefined && txtSrchProdName == undefined  )
            {
                txtSrchProdSKU = "";
                txtSrchProdId = "";
                txtSrchProdName = "";
            }

            //   var prod_search = {prodsku:txtSrchProdSKU,prodid:txtSrchProdId,prodName:txtSrchProdName,prodbrand:ddlSrchBrand};

            prod_mainService.getProdSearchList($http,$q,txtSrchProdSKU,txtSrchProdId,txtSrchProdName,ddlSrchBrand)
                .then(function (data) {

                    $scope.search_data = data;
                    search = data;

                },
                function () {
                    //Display an error message
                    $scope.error = error;

                });

        }


        $scope.get_details = function(id)
        {
            var ID = id;

            for(var j=0;j<search.length;j++)
            {
                if(ID == j) {
                    $scope.prod_id = search[j].id;

                    prod_mainService.get_product_details($http, $q, $scope.prod_id)
                        .then(function (data) {

                            $scope.sel_prod_name = data[0].Name;
                            $scope.id = data[0].id;
                            $scope.sku = data[0].sku;
                            $scope.Name = data[0].Name;
                            $scope.sel_brand_name = data[0].brand;
                            $scope.description = data[0].description;
                            $scope.shortdesc = data[0].shortdesc;
                            $scope.reorder = data[0].Reorder_Level;
                            $scope.prodsku_disable = true;
                            $scope.prodId_disable = true;
                            $scope.mrp = data[0].price.mrp;
                            $scope.list = data[0].price.list;
                            $scope.discount = data[0].price.discount;
                            $scope.msp = data[0].price.min;
                            $scope.stock = data[0].stock;
                            if (data[0].active == 0) {
                                $scope.status = true;
                            }
                            else {
                                $scope.status = false;
                            }
                            if (data[0].have_child == "0") {
                                $scope.child_prod = false;
                                $scope.btnBulkProdStock_disable = false;
                                $scope.express = false;
                            }
                            else {
                                $scope.child_prod = true;
                                $scope.btnBulkProdStock_disable = true;
                                $scope.express = true;
                            }
                            if (data[0].Express == "0" || data[0].Express == "BsonNull" || data[0].Express == null) {
                                $scope.express = false;
                            }
                            else {
                                $scope.express = true;
                            }


                        },
                        function () {
                            //Display an error message
                            $scope.error = error;

                        });
                }
            }


            $scope.search_popup = false;

        }



        function get_product(){

            ID=$scope.id;
            var SKU=$scope.sku;
            PRODUCT_NAME=$scope.Name;
            var BRAND=$scope.sel_brand_name;
            var PROD_DESC=$scope.description;
            var PROD_SHRT_DESC=$scope.shortdesc;
            var MIN_ORD_QTY= $scope.msp;
            var txtReorderLevel=$scope.reorder;
            var EXPRESS=$scope.express;
            var HAVE_CHILD=$scope.child_prod;
            var Stock = $scope.stock;
            var MRP = $scope.mrp;
            var List_Price = $scope.list;
            var Discount = $scope.discount;
            var Min = $scope.msp;
            var Final_offer = 0;
            var Final_discount = 0;
            var Price = {

                mrp:MRP,
                list:List_Price,
                discount:Discount,
                min :Min,
                final_offer:Final_offer,
                final_discount:Final_discount


            };

            if (ID == '' || ID == '0') {
                alert('Please provide the Product ID');
                return false;
            }
            else if (PRODUCT_NAME == '') {
                alert('Please provide the Product Name');
                return false;
            }
            else if (MRP == '' || MRP == null) {
                alert('Please provide MRP');
                return false;
            }
            else if (List_Price == '' || List_Price == null) {
                alert('Please provide List Price');
                return false;
            }

            else if (BRAND == '' || BRAND == null) {
                alert('Please select a brand');
                return false;
            }
            else if (MIN_ORD_QTY == '' || MIN_ORD_QTY == null) {
                alert('Please enter minimum sales price');
                return false;
            }
            else if (PROD_SHRT_DESC == '' || PROD_SHRT_DESC == null) {
                alert('Please enter Product Short Description');
                return false;
            }
            else if (PROD_DESC == '' || PROD_DESC == null) {
                alert('Please enter Product Description');
                return false;
            }

            else if (PROD_DESC.length < 100) {
                alert('Product Description Must Be Atleast 500 Characters');
                return false;

            }
            else if (PROD_DESC.length > 1500) {
                alert('Product Description Must Be Less Than 1500 Characters');
                return false;
            }
            else if(Stock == '' || Stock == null){

                alert('Please enter Stock');
                return false;

            }
            var EXPRESS="";
            var HAVE_CHILD="";
            if($scope.child_prod==true){
                var HAVE_CHILD=1;
                EXPRESS=1;
            }
            else{
                var HAVE_CHILD=0;
                if($scope.express==true){
                    EXPRESS=1;
                }
                else{
                    EXPRESS=0;
                }
            }
            if (txtReorderLevel == '' || txtReorderLevel == null) {
                alert('Please Enter Re-Order Level');
                return false;
            }
            var prod_id=$scope.sel_prod_name;
            if(prod_id==0){
                var i=0;
                var vProductName=$scope.Name;
                if (vProductName.toString().toLowerCase() == $(this).text().toString().toLowerCase()) {
                    i++;
                }
                if (i != 0) {
                    alert('You Have Already Entered This Product Name');
                    return false;
                }
            }
            var Prod_Status="";
            if($scope.status==true){
                Prod_Status=0;
            }
            else{
                Prod_Status=2;
            }
            return{
                id: ID,
                sku:SKU,
                Name: PRODUCT_NAME,
                active: Prod_Status,
                brand: BRAND,
                price:Price,
                min_order_qty: MIN_ORD_QTY,
                description: PROD_DESC,
                shortdesc: PROD_SHRT_DESC,
                have_child: HAVE_CHILD,
                Express: EXPRESS,
                Reorder_Level: txtReorderLevel,
                stock:Stock,
                parent_prod: "0" }
        }


        $scope.Open_img_popup = function()
        {
            ID = $scope.id;
            PRODUCT_NAME = $scope.Name;
            if(PRODUCT_NAME == "" || PRODUCT_NAME == null || typeof PRODUCT_NAME === "undefined" )
            {
                alert('Please Select a Product');
                return false;

            }

           $state.go('main.prod_upload',{'product_id':ID , 'product_name':PRODUCT_NAME});
        }



        $scope.CalDiscount =  function () {

            var MRP = ($scope.mrp) * 1;
            var LIST_PRICE = ($scope.list) * 1;
            var CAL = LIST_PRICE / MRP;
            var DISCOUNT = 100 * (1 - CAL);
            $scope.discount = (Math.round(DISCOUNT * 100) / 100) * 1;



        }
        $scope.CalListPrice =  function (){

            var MRP = ($scope.mrp) * 1;
            var DISCOUNT = ($scope.discount) * 1;
            var CAL_AMT = (MRP * DISCOUNT) / 100;
            var LIST_PRICE = MRP - CAL_AMT;
            $scope.list = (Math.round(LIST_PRICE * 100) / 100) * 1;



        }


        $scope.on_mouse = function()
        {
            this.hoverEdit = true;

        }


        $scope.off_mouse = function()
        {
            this.hoverEdit = false;

        }

        $scope.search = function()
        {

           // $state.go('main.search_product');
            $scope.search_popup = true;

        }

        $scope.go_back = function()
        {
            $scope.search_popup = false;


        }


        var collection_name = "";

        function get_audit_data(collection_name,previous_data,new_data) {

            var log_info = {
                "app_id": "dalmia_admin_data",
                "secret": "a1844750-accf-451b-bf6f-0bc997b974cc",
                "db_name": "annectos_prod",
                "collection_name": collection_name,
                "field_name": "",
                "business_id": "dalmia_admin",
                "business_id_name": "dalmia",
                "old_data":previous_data,
                "new_data":new_data ,
                "user": user_info
            };

            return log_info;
        }



        var collection_name = "product";

        $scope.SaveProduct=function(){
            if(get_product()==false) {
                return false;
            }
            var prod_details=get_product();
            var prod_id=$scope.sel_prod_name;
            if(prod_id==0) {

//                var previous_data = "";
//                var new_data = prod_details;
//
//                var log_info = get_audit_data(collection_name,previous_data,new_data);

                prod_mainService.prod_view_insert($http, $q, prod_details)
                    .then(function (data) {

//                        prod_mainService.insert_audit_data($http, $q,log_info)
//                            .then(function (data) {
//
//                            },
//                            function () {
//                                //Display an error message
//                                $scope.error = error;
//
//                            });

                        alert("Record Saved Successfully");
                        ClearProdFields();
                        PopulateProductList();

                        $scope.prodId_disable = false;
                        $scope.prodsku_disable = false;
                    },
                    function () {
                        //Display an error message
                        $scope.error = error;

                    });
            }
            else{


              //  var company_name = $scope.list_company.name;


                var previous_data = localStorageService.get('prod_details');

                var new_data = prod_details;

                var prod_data = get_audit_data(collection_name,previous_data,new_data);


                prod_mainService.prod_view_update($http,$q,prod_details)
                    .then(function(data){

                        prod_mainService.insert_audit_data($http, $q,prod_data)
                            .then(function (data) {

                            },
                            function () {
                                //Display an error message
                                $scope.error = error;

                            });

                        alert("Record Updated Successfully");
                        ClearProdFields();
                        PopulateProductList();
                        $scope.prodId_disable = false;
                        $scope.prodsku_disable = false;

                    },
                    function () {
                        //Display an error message
                        $scope.error = error;

                    }
                );
            }




        }

    })
