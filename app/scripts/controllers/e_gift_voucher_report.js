/**
 * Created by developer7 on 9/10/2014.
 */

'use strict';

angular.module('annectosadmin')
    .controller('e_gift_voucher_reportCtrl', function ($scope,$rootScope,$location,$http,$q,eGiftService,commonService) {


        var RootStart = "http://";
        var RootEnd = ".annectos.net";


        init();
        function init() {
            PopStoreCompany();
        }

        function PopStoreCompany(){
            commonService.postPopulateCompany($http,$q)

                .then(function (data) {
                    if(data.length>0){
                        data[0].name = "Select Company";
                    }
                    $scope.egiftCompany_list = data;
                    $scope.sel_store = 0;
                },
                function () {
                    //Display an error message
                    $scope.error = error;

                });
        }

        $scope.SearchEGiftVoucher = function(){
            var ddlEGiftCompany = $scope.sel_store;
            var StoreName = null;
            for(var i=0; i<$scope.egiftCompany_list.length; i++){
                if($scope.egiftCompany_list[i].id == ddlEGiftCompany){
                    StoreName = $scope.egiftCompany_list[i].name;
                    break;
                }
            }
            if (ddlEGiftCompany == 0 || ddlEGiftCompany == null) {
                alert('Please Select Store Or Company');
                return false;
            }

            var fromdt = "";
            var todt = "";

            eGiftService.get_GV_Voucher_Details($http, $q, StoreName,fromdt,todt)
                .then(function (data) {
                    $scope.vouDtlsList = data;
                },
                function (error) {
                    alert('Error');
                    $scope.error = error;
                });


        }

        $scope.CancelSearch = function(){
            $scope.sel_store = 0;
            $scope.vouDtlsList = null;
        }

    });