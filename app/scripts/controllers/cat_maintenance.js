/**
 * Created by DRIPL_1 on 09/05/2014.
 */

'use strict';

/**
 * Created by developer1 on 7/1/2014.
 */
angular.module('annectosadmin')
    .controller('cat_maintenanceCtrl', function ($scope,$rootScope,$location,$http,$q,cat_maintenanceService,localStorageService,$state,UtilService) {
        $scope.awesomeThings = [
            'HTML5 Boilerplate',
            'AngularJS',
            'Karma'
        ];

        var rootCaturl = 'http://54.209.87.32/index.html#/cat/';
        var cat_id;
        var category_name;
        var user_info;


        init();
        function init(){

            UtilService.change_header($state.current.data.header_label);
            PopulateCategoryList();

            user_info=localStorageService.get("admin_user_info").user_name;

        }


        function getDateFormat(dateVal){
            var FistDate = new Date(dateVal);
            var dd = FistDate.getDate();
            var mm = FistDate.getMonth()+1; //January is 0!

            var yyyy = FistDate.getFullYear();
            if(dd<10){
                dd='0'+dd
            }
            if(mm<10){
                mm='0'+mm
            }
            var today =mm+'/' + dd+'/'+yyyy;
            return today;
        }



     //////////////// Disable weekend selection


        $scope.disabled = function(date, mode) {
            return ( mode === 'day' && ( date.getDay() === 0 || date.getDay() === 6 ) );
        };

        $scope.toggleMin = function() {
            $scope.minDate = $scope.minDate ? null : new Date();
        };
        $scope.toggleMin();

        $scope.openStart = function($event) {
            $event.preventDefault();
            $event.stopPropagation();
            $scope.opened_start = true;
        };

        $scope.openEnd = function($event) {
            $event.preventDefault();
            $event.stopPropagation();
            $scope.opened_end = true;
        };

        $scope.dateOptions = {
            formatYear: 'yy',
            startingDay: 1
        };

        $scope.initDate = new Date('2016-15-20');
        $scope.formats = ['dd-MMMM-yyyy', 'yyyy/MM/dd', 'dd.MM.yyyy', 'shortDate'];
        $scope.format = $scope.formats[0];


//////////////////////////////////////////////////////////



        $scope.$watch('sel_cat_name', function () {
            getCategoryDetails();
        });


        $scope.$watch('sel_ParentId', function () {
            var ParentId=$scope.sel_ParentId;


        });


        var parent_category_id;

        function PopParentCatList(){


            var val=$scope.level;
          //  loading_add();

            UtilService.show_loader();
            cat_maintenanceService.PopParentCatList($http,$q,val)

                .then(function(data){

                   // $scope.list_cat=data[0];
//                    if(data.length>0){
//
//                        data[0].Name="";
//                    }

                    $scope.list_cat=data;

                    $scope.sel_ParentId = parent_category_id;

                    UtilService.hide_loader();
                  //  loading_cut();

                    if(data.length>0){
                        //$scope.cat_detail_data.ParentId = data[0].id;
                    }
                },
                function () {
                    //Display an error message
                    $scope.error = error;

                })

        }

        function PopParentCatLevelWise(){


            var val=$scope.level;
            //  loading_add();

            UtilService.show_loader();
            cat_maintenanceService.PopParentCatList($http,$q,val)

                .then(function(data){

                    // $scope.list_cat=data[0];
//                    if(data.length>0){
//
//                        data[0].Name="";
//                    }

                    $scope.list_cat=data;

                    UtilService.hide_loader();
                    //  loading_cut();

                    if(data.length>0){
                        //$scope.cat_detail_data.ParentId = data[0].id;
                    }
                },
                function () {
                    //Display an error message
                    $scope.error = error;

                })

        }
        $scope.chkSplStore_click=function(){
            if($scope.chkSplStore_checked=true){
                $scope.SpecialStore_disable=false;
            }
            else{
                $scope.SpecialStore_disable=true;
            }

        }

        $scope.cat_level_click=function(val){
            if(val=="3"){
                $scope.txtShipping_disable=false;
                $scope.txtMinShipDays_disable=false;
                $scope.txtMaxShipDays_disable=false;
                $scope.ddl_parent_cat_disable = false;
                var data_cat_detail_data = {Shipping: "3-5 days",
                    min_ship: "",
                    max_ship: ""};
                $scope.cat_detail_data = data_cat_detail_data;

            }

            else if(val =="1")
            {
               $scope.ddl_parent_cat_disable = true;

            }
            else{

                $scope.txtShipping_disable=true;
                $scope.txtMinShipDays_disable=true;
                $scope.txtMaxShipDays_disable=true;
                $scope.ddl_parent_cat_disable = false;
                var data_cat_detail_data = {Shipping: "",
                    min_ship: "",
                    max_ship: ""};
                $scope.cat_detail_data = data_cat_detail_data;

            }
            PopParentCatLevelWise();
        }


        function PopCatData(data){
            $scope.cat_detail_data.Name=data[0].Name;
            $scope.cat_detail_data.displayname=data[0].displayname;
            $scope.cat_detail_data.description=data[0].txtCatDesc;
            if(data[0].Shipping!="BsonNull"){
                $scope.cat_detail_data.shipping=data[0].Shipping
            }
            else{
                $scope.cat_detail_data.shipping="";
            }
            if(data[0].min_ship!="0"){
                $scope.cat_detail_data.min_ship=data[0].min_ship;
            }
            else{
                $scope.cat_detail_data.min_ship="";
            }
            if(data[0].max_ship!="0"){
                $scope.cat_detail_data.max_ship=data[0].max_ship;
            }
            else{
                $scope.cat_detail_data.max_ship="";
            }
            if(data[0].active==0){

                $scope.deactivate_check=true;
            }
            else
            {
                $scope.deactivate_check=false;
            }

            if(data[0].special!="BsonNull"){
                $scope.chkSplStore_checked=true;
                $scope.cat_detail_data.SpecialStore=data[0].special;
                $scope.cat_detail_data.Str_start_date=data[0].StoreStartDate;
                $scope.cat_detail_data.Str_end_date=data[0].StoreEndDate;
            }
            else{
                $scope.chkSplStore_checked=false;
                $scope.cat_detail_data.SpecialStore="";
                $scope.cat_detail_data.Str_start_date="";
                $scope.cat_detail_data.Str_end_date="";
            }
            $scope.cat_detail_data.ParentId=data[0].ParentId;
            $scope.sel_ParentId=data[0].ParentId;
            $scope.chkSplStore_disable=true;



        }

        function PopulateCategoryList(){
           // loading_add();
            UtilService.show_loader();
            cat_maintenanceService.get_all_category($http,$q)

                .then(function (data) {
                    $scope.listData = data;
                    $scope.btnAddAnotherCat_disable=true;
                    $scope.txtShipping_disable=true;
                    $scope.txtMinShipDays_disable=true;
                    $scope.txtMaxShipDays_disable=true;
                    $scope.ddl_parent_cat_disable=false;
                    $scope.btnUpldImgVid_disable=true;
                    $scope.SpecialStore_disable = true;
                    clear_fields();

                  //  loading_cut();

                    UtilService.hide_loader();

                },
                function () {
                    //Display an error message
                    $scope.error = error;

                });


        }

        function clear_fields(){
            $scope.sel_cat_name="0";
            $scope.level="";
            var data_cat_detail_data = {Name: "",
                displayname:"",
                description:"",
                special:"",
                Str_start_date: "",
                Str_end_date: "",
                min_ship: ""};
            $scope.cat_detail_data = data_cat_detail_data;
            $scope.ddl_parent_cat_disable=false;
            $scope.deactivate_check=false;
            $scope.chkSplStore_checked=false;
            $scope.chkSplStore_disable=false;
        }
        function getCategoryDetails(){

            $scope.btnCatSave_disable=false;
            $scope.btnAddAnotherCat_disable=true;
            var Category_ID=$scope.sel_cat_name;
            if (Category_ID == 0 || Category_ID == null) {
                $scope.CatLink_show=false;
            }
            else{

                $scope.CatLink_URL=rootCaturl + Category_ID;
                $scope.CatLink_show=true;

            }

            if (Category_ID != 0 && Category_ID != null) {


                $scope.btnUpldImgVid_disable=false;

                cat_maintenanceService.details_by_category_name($http,$q,Category_ID)

                    .then(function (data) {
                        $scope.cat_detail_data=data[0];
                        var cat_details =  $scope.cat_detail_data;
                        localStorageService.set('cat_details',{});
                        localStorageService.add('cat_details',cat_details);
                        var level_value=data[0].ParentId;
                        parent_category_id = data[0].ParentId;
                        var active = data[0].active;
                        if(active == 1)
                        {
                            $scope.deactivate_check = false;
                        }
                        else
                        {
                            $scope.deactivate_check = true;

                        }
                        $scope.txtShipping_disable=true;
                        $scope.txtMinShipDays_disable=true;
                        $scope.txtMaxShipDays_disable=true;

                        if(level_value=="0"){
                            $scope.level= "1";
                            PopParentCatList();
                        }
                        else{
                            var arr=data[0].ParentId.split('.');
                            if(arr[1]=="0"){
                                $scope.level="2";
                                PopParentCatList();
                            }
                            else{
                                $scope.level="3";
                                $scope.txtShipping_disable=false;
                                $scope.txtMinShipDays_disable=false;
                                $scope.txtMaxShipDays_disable=false;
                                $scope.cat_detail_data.shipping = data[0].Shipping;
                                PopParentCatList();
                            }
                        }
                      //  setTimeout(function(){PopCatData(data)},4000);

                        //PopCatData(data);
                        // $('#addressSection input[type=radio]').attr('disabled', false);
                        $scope.level1_disable=true;
                        $scope.level2_disable=true;
                        $scope.level3_disable=true;

                        $scope.ddl_parent_cat_disable=true;


                    },
                    function () {
                        //Display an error message
                        $scope.error = error;

                    });
            }
            else{
                $scope.level1_disable=false;
                $scope.level2_disable=false;
                $scope.level3_disable=false;
                $scope.ddl_parent_cat_disable=false;
                $scope.level="";
                $scope.sel_ParentId = "";
                if($scope.cat_detail_data != undefined){
                    $scope.cat_detail_data.ParentId="";
                }

                $scope.btnUpldImgVid_disable=true;
                clear_fields();
            }
        }




        function getCategoryData(){
             cat_id=$scope.sel_cat_name;
             category_name=$scope.cat_detail_data.Name;
             if(category_name=="" || category_name==null){
                alert('Please Select a Category');
                return false;
            }
            var cat_display_name= $scope.cat_detail_data.displayname;
            if(cat_display_name=="" || cat_display_name==null){
                alert('Please Enter Category Display Name');
                return false;
            }
            var txtCatDesc= $scope.cat_detail_data.description;
            if(txtCatDesc=="" || txtCatDesc==null){
                alert('Please Enter Category Description');
                return false;
            }
            //var ddlParentCat=$scope.cat_detail_data.ParentId;
            var parent_cat= $scope.sel_ParentId;

            if(typeof $scope.level === 'undefined' || $scope.level == ""){
                alert("Please Select Category Level");
                return false;
            }
            else{
                var val=$scope.level;
                if(val=="2" || val=="3"){
                    if(parent_cat=="" || parent_cat=="0" || typeof parent_cat ==='undefined'){
                        alert('Please Select Parent Category');
                        return false;
                    }
                }
            }


            if($scope.level=="1"){
                parent_cat= "0";
            }


            if($scope.deactivate_check == true)
            {

                var cat_active = "0";

            }
            else
            {
                var cat_active = "1";

            }

          //  $scope.cat_detail_data.SpecialStore="";
         //   $scope.cat_detail_data.Str_start_date="";
          //  $scope.cat_detail_data.Str_end_date="";

            if($scope.chkSplStore_checked == true){

                var txtSpecialStore= $scope.cat_detail_data.SpecialStore;

                if(txtSpecialStore=="" || txtSpecialStore==null){
                    alert('Please Enter Special Store Name');
                    return false;
                }
                var StoreStartDate= $scope.cat_detail_data.Str_start_date;
                var start_date = getDateFormat(StoreStartDate);
                $scope.cat_detail_data.Str_start_date = start_date;
                if(start_date=="" || start_date==null){
                    alert('Please Select Start Date');
                    return false;
                }
                var StoreEndDate= $scope.cat_detail_data.Str_end_date;
                var end_date = getDateFormat(StoreEndDate);
                $scope.cat_detail_data.Str_end_date = end_date;

                if(end_date=="" || end_date==null){
                    alert('Please Select End Date');
                    return false;
                }
          //      parent_cat = "0";
            }
      //      $scope.cat_detail_data.shipping="";
          //  $scope.cat_detail_data.min_ship="0";
           // $scope.cat_detail_data.max_ship="0";

            if(val=="3"){
                var txtShipping= $scope.cat_detail_data.shipping;
                if(txtShipping=="" || txtShipping==null){
                    alert('Please Enter Shipping');
                    return false;
                }
                var txtMinShipDays= $scope.cat_detail_data.min_ship;
                if(txtMinShipDays=="" || txtMinShipDays==null){
                    alert('Please Enter Minimum Shipping Days');
                    return false;
                }
                var txtMaxShipDays= $scope.cat_detail_data.max_ship;
                if(txtMaxShipDays=="" || txtMaxShipDays==null){
                    alert('Please Enter Maximum Shipping Days');
                    return false;
                }
                if ((txtMinShipDays * 1) > (txtMaxShipDays * 1)) {
                    alert('Minimum Shipping Days Should Be Less Than Maximum Shipping Days');
                    return false;
                }
            }

            var number_items = "";
            var store_date = [];
            var store_data = {
                end_date: start_date,
                start_date: end_date
            }
            store_date[0] = store_data;

            return {
                id: cat_id,
                Name: category_name,
                displayname: cat_display_name,
                description: txtCatDesc,
                ParentId: parent_cat,
              //  special: self.special,
                active: cat_active,
                number_items: number_items,
                special: txtSpecialStore,
                store_defn: store_date,
                Shipping: txtShipping,
                min_ship: txtMinShipDays,
                max_ship: txtMaxShipDays
            };

        }

        var collection_name = "";

        function get_audit_data(collection_name,previous_data,new_data) {

            var log_info = {
                "app_id": "dalmia_admin_data",
                "secret": "a1844750-accf-451b-bf6f-0bc997b974cc",
                "db_name": "annectos_prod",
                "collection_name": collection_name,
                "field_name": "",
                "business_id": "dalmia_admin",
                "business_id_name": "dalmia",
                "old_data":previous_data,
                "new_data":new_data ,
                "user": user_info
            };

            return log_info;
        }




        $scope.save_category=function(){
            if(getCategoryData()==false) {
                return false;
            }
            var cat_name= $scope.cat_detail_data.Name;
           // var ParentCatName= $scope.cat_detail_data.ParentId;



            var i=0;
            var cat_id=$scope.sel_cat_name;

            if (cat_id == 0) {

                for(var j=0; j<$scope.listData.length; j++){
                    var category_names = $scope.listData[j];
                    var category_names_part = category_names.toString().split('-');
                    var LevelVal = $scope.level;
                    if (LevelVal == 1) {
                        if (cat_name.toString().toLowerCase() == category_names_part[0].toString().toLowerCase()) {
                            i++;
                        }

                    } else if (LevelVal == 2) {

                        if (category_names_part.length == 2) {
                            if (parent_cat_name.toString().toLowerCase() == (category_names_part[0].toString().toLowerCase()) && cat_name.toString().toLowerCase() == (category_names_part[1].toString().toLowerCase())) {
                                i++;
                            }
                        }

                    } else if (LevelVal == 3) {
                        if (category_names_part.length == 3) {
                            if (parent_cat_name.toString().toLowerCase() == (category_names_part[0].toString().toLowerCase()) + '-' + (category_names_part[1].toString().toLowerCase()) && cat_name.toString().toLowerCase() == (category_names_part[2].toString().toLowerCase())) {
                                i++;
                            }
                        }
                    }
                }



                if (i != 0) {
                    alert('You Have Already Entered This Category Name');
                    return false;
                }
            }
            $scope.btnCatSave_disable=true;
            $scope.btnAddAnotherCat_disable=false;

            var category_details=getCategoryData();

            var collection_name = "category";

            if(cat_id==0){

//                var previous_data = "";
//                var new_data = category_details;
//
//                var log_info = get_audit_data(collection_name,previous_data,new_data);
//
//
//                cat_maintenanceService.insert_audit_data($http, $q,log_info)
//                    .then(function (data) {
////                             alert('Record Saved Successfully');
////                             get_all_company();
////                             CompanyNewView();
//                    },
//                    function () {
//                        //Display an error message
//                        $scope.error = error;
//
//                    });




                cat_maintenanceService.Insert_Category($http,$q,category_details)
                    .then(function (data) {
                        alert('Record Saved Successfully');
                        PopulateCategoryList();

                    },
                    function () {
                        //Display an error message
                        $scope.error = error;

                    });
            }
            else{


//                var company_name = $scope.list_company.name;

                var previous_data =  localStorageService.get('cat_details');

                var new_data = category_details;

                var info_data = get_audit_data(collection_name,previous_data,new_data);


                cat_maintenanceService.Update_Category($http,$q,category_details)
                    .then(function (data) {

                        cat_maintenanceService.insert_audit_data($http, $q,info_data)
                            .then(function (data) {

                            },
                            function () {
                                //Display an error message
                                $scope.error = error;

                            });

                        alert('Record Updated Successfully');


                    },
                    function () {
                        //Display an error message
                        $scope.error = error;

                    });
            }

        }

        $scope.add_another=function(){
            $scope.sel_cat_name="0";
            getCategoryDetails();
            $scope.btnCatSave_disable=false;
            $scope.btnAddAnotherCat_disable=true;
        }
        $scope.cancel_category=function(){
            clear_fields();
        }
        $scope.link_cfs=function(){

            var Category_Type=$scope.sel_cat_name;
            if (Category_Type == 0) {
                alert('Please Select A Category.');
                return false;
            }
            else {
                var arr = Category_Type.split('.');
                if (arr.length == 2) {
                    alert("You Can't Select A Level 1 Category ");
                    return false;
                }
            }
            $state.go("main.catfilter");
        }

        $scope.link_pca=function(){
            $state.go("main.prodcatassoc");
        }

        $scope.link_bsc=function(){
            $state.go("store-catalog");
        }


         $scope.OpenUploadImagePage = function()
         {
            var data =  getCategoryData();

             if(data.Name == "" || data.Name == null || typeof data.Name === "undefined" )
             {

                 return false;
             }
             else{
             $state.go('main.upload',{'category_id':cat_id , 'category_name':category_name});
             }


         }

         $scope.prod_assoc = function()
         {
             $state.go('main.prod_cat_association',{'cat_id':cat_id},{'category_name':category_name});


         }

        $scope.cat_filter = function()
        {
            $state.go('main.cat_filter',{'category_id':cat_id},{'category_name':category_name});


        }


        $scope.on_mouse = function()
        {
            this.hoverEdit = true;

        }


        $scope.off_mouse = function()
        {
            this.hoverEdit = false;

        }






    })

