/**
 * Created by DRIPL_1 on 09/09/2014.
 */

/**
 * Created by developer1 on 7/1/2014.
 *

 */

'use strict';

angular.module('annectosadmin')
    .controller('productfeaturemgmntCtrl', function ($scope,$rootScope,$location,$http,$q,prodfeatureService,localStorageService,UtilService,$state) {
        $scope.awesomeThings = [
            'HTML5 Boilerplate',
            'AngularJS',
            'Karma'
        ];
        var rootProdurl='http://54.209.87.32/index.html#/prod/';
        var user_info;

        $scope.name;
        $scope.value;
        init();
        function init(){

            UtilService.change_header($state.current.data.header_label);
            user_info = localStorageService.get("admin_user_info").user_name;
            pop_parent_product_list();
            $scope.prod_name="";

        }


        $scope.selectcol = function(row) {
            $scope.selectedRow = row;
        };


        $scope.$watch('sel_prod_feature', function (newValue , oldValue ) {
            if(newValue !== oldValue)
            {
                get_prod_features();
            }


        });
        function pop_parent_product_list(){

          //  loading_add();

            UtilService.show_loader();


            prodfeatureService.get_parent_list($http,$q)
                .then(function (data) {
                    $scope.prod_feature_detail=data;
                  //  loading_cut();

                    UtilService.hide_loader();

                },
                function () {
                    //Display an error message
                    $scope.error = error;

                });
        }

        function get_prod_features() {
            var Product_ID = $scope.sel_prod_feature;

            if(Product_ID!=0){
                $scope.ProdLink_URL = rootProdurl + Product_ID;
                $scope.ProdLink_show = true;
                var feature_grid_array = [];
                prodfeatureService.get_prod_feature_detail($http, $q, Product_ID)
                    .then(function (data) {
                        $scope.prod_name = data[0].Name;
                        if (data[0].parent_cat_id != "BsonNull") {
                            get_features_grid(data[0].parent_cat_id);
                        }
                        if (data[0].feature.length == 0) {

                            var Features = [];
                            $scope.prod_feature_data = Features;
                        }
                        else {
                            var addedfeaturegrid = [];
                            for (var j = 0; j < data[0].feature.length; j++) {

                                var addedfeaturegrid = {
                                    name: data[0].feature[j].name,
                                    Value: data[0].feature[j].values
                                }
                                addedfeaturegrid[j] = addedfeaturegrid;
                                feature_grid_array.push(addedfeaturegrid);
                            }
                            $scope.prod_feature_data = feature_grid_array;

                        }


                    },
                    function () {
                        //Display an error message
                        $scope.error = error;

                    });
            }

            else
            {
                $scope.ProdLink_show = false;
                $scope.prod_name = "";

            }
        }

        function get_features_grid(Parent_Cat_ID){
            prodfeatureService.get_features_grid_data($http,$q,Parent_Cat_ID)
                .then(function (data) {

                    $scope.prod_feature_list=data[0].filters;

                },
                function () {
                    //Display an error message
                    $scope.error = error;

                });
          }

        var collection_name = "";

        function get_audit_data(collection_name,previous_data,new_data) {

            var log_info = {
                "app_id": "dalmia_admin_data",
                "secret": "a1844750-accf-451b-bf6f-0bc997b974cc",
                "db_name": "annectos_prod",
                "collection_name": collection_name,
                "field_name": "",
                "business_id": "dalmia_admin",
                "business_id_name": "dalmia",
                "old_data":previous_data,
                "new_data":new_data ,
                "user": user_info
            };

            return log_info;
        }


        $scope.save_feature=function(){
            var Product_ID=$scope.sel_prod_feature;
            var grid=$scope.prod_feature_data;
          //  $scope.pop_feature={};
            var GridData=grid;
            var FeatureArray = [];
            if (GridData.length == 0) {
                alert('No Features Available!');
                return false;
            }
            else {
                for (var i = 0; i < GridData.length; i++) {
                    var Features_Data = {
                        name: GridData[i].name,
                        values: GridData[i].Value
                    }
                    FeatureArray[i] = Features_Data;
                }
            }
            var a={id:Product_ID,feature:FeatureArray}

            var previous_data = "";
            var new_data = a;

            collection_name = "product";

            var log_info = get_audit_data(collection_name,previous_data,new_data);

            prodfeatureService.insert_feature($http,$q,a)
                .then(function (data) {
                    prodfeatureService.insert_audit_data($http, $q,log_info)
                        .then(function (data) {

                        },
                        function () {
                            //Display an error message
                            $scope.error = error;

                        });
                    alert("Record Saved Successfully");

                },
                function () {
                    //Display an error message
                    $scope.error = error;

                });
        }
        $scope.add_feature=function(FeatureName,FeatureValues){
            $scope.feature_name=FeatureName;
            //var arrFeatureValues = FeatureValues.split(',');
            var arrFeatureValues = FeatureValues;
            $scope.prod_feature_details=arrFeatureValues;
        }
        $scope.add=function(feature_name){

            var val = $scope.feature_data[0];
            var FeatureName=feature_name;
            var FeatureVal= val;
            if (FeatureVal == null) {
                alert('Please Select A Value!');
                return false;
            }
            var grid=$scope.prod_feature_data;
          //  var pop_feature=[];
           // pop_feature.push(grid);
            var GridData=grid;
            var match = 0;

//            for (var i = 0; i < GridData.length; i++) {
//                if (GridData[i].name == FeatureName) {
//                    GridData[i].Value = FeatureVal;
//                    match++;
//                }
//            }
            var NewGridData = GridData;
            if (match == 0) {
                if (FeatureName != null && FeatureName != "") {

                    var NewFeatureData = {
                        name: FeatureName,
                        Value: FeatureVal
                    }


                    if(NewGridData.length != null)
                    {

                        for(var i=0;i<NewGridData.length;i++)
                        {
                            var name = NewGridData[i].name;
                            if(name == NewFeatureData.name )
                            {
                                NewGridData.splice(i,1);

                            }


                        }

                    }
//                    var feature = [];
//
//                        feature[0] = NewFeatureData;

                  //  NewGridData[GridData.length] = NewFeatureData;

                    NewGridData.push(NewFeatureData);
                }
            }

            $scope.prod_feature_data=NewGridData;
            alert("Record Saved Successfully");

        }
        $scope.delete_row=function(FilterName,FilterValue) {
            var del_confirm_val = confirm("Are You Sure You Want To Delete?");
            if (del_confirm_val == true) {
                var Product_ID = $scope.sel_prod_feature;
                var FeatureArr = [];
                var Features_Data = {
                    name: FilterName,
                    values: FilterValue
                }
                FeatureArr[0] = Features_Data;
                var p={id:Product_ID,feature:FeatureArr}
                prodfeatureService.delete_prod_feature($http,$q,p)
                    .then(function (data) {
                        alert('Delete Successfully!');

                        get_prod_features();

                    },
                    function () {
                        //Display an error message
                        $scope.error = error;

                    });
            }
        }


        $scope.go_to_prod_popup = function(FeatureName,FeatureValues)
        {

            $scope.feature_name=FeatureName;
            //var arrFeatureValues = FeatureValues.split(',');
            var arrFeatureValues = FeatureValues;
            $scope.prod_feature_details=arrFeatureValues;

              $scope.prod_feat_popup = true;

        }

        $scope.cancel_pop = function()
        {

            $scope.prod_feat_popup = false;

        }

        $scope.close_pop_up = function()
        {
            $scope.prod_feat_popup = false;

        }

        $scope.close_add_pop_up = function()
        {
            $scope.add_feat_popup = false;

        }


        $scope.save_additional_feature=function(){
            var txtAddFeature= $scope.name;
            if (txtAddFeature == "" || txtAddFeature == null) {
                alert('Please Enter Additional Feature Name!');
                return false;
            }
            var txtAddFeatureValue= $scope.value;
            if (txtAddFeatureValue == "" || txtAddFeatureValue == null) {
                alert('Please Enter Additional Feature Value!');
                return false;
            }
            var NewAddFeatureData = {
                name: txtAddFeature,
                Value: txtAddFeatureValue
            }
            var grid=$scope.prod_feature_data;
          //  $scope.pop_feature={};
            var GridData=grid;
            if (GridData == 0 ||GridData == null || typeof GridData ===  'undefined' ) {
                var AdFeatureArray = [];
                AdFeatureArray[0] = NewAddFeatureData;
                $scope.prod_feature_data=AdFeatureArray;
            }
            else {
                for ( var i = 0; i < GridData.length; i++) {
                    if (GridData[i].name == txtAddFeature) {
                        alert('Feature Name Already Added.');
                        return false
                    }
                }
//                var NewGridData = GridData;
//                NewGridData[GridData.length] = NewAddFeatureData;

                  var new_grid = [];
                  new_grid = GridData;
                  new_grid.push(NewAddFeatureData);

                $scope.prod_feature_data=new_grid;
            }
            $scope.name="";
            $scope.value="";
        }

          $scope.add_features = function()
          {

              $scope.add_feat_popup = true;

          }

        $scope.cancel_add_pop = function()
        {
            $scope.add_feat_popup = false;

        }


        $scope.clear_field = function()
        {

           $scope.sel_prod_feature = "";
           $scope.prod_name = "";
           $scope.prod_feature_data = "";
           $scope.prod_feature_list ="";


        }



    })

