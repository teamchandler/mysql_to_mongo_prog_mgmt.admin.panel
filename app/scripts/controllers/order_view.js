/**
 * Created by developer7 on 9/11/2014.
 */
'use strict';

angular.module('annectosadmin')
    .controller('orderDetailCtrl', function ($scope,$rootScope,$location,$http,$q, $state, $stateParams,orderService,commonService,UtilService) {


        var RootStart = "http://";
        var RootEnd = ".annectos.net";
        var GlobalOrderId = 0;
        /*var sel_status;
        var sel_order;
        var from_date;
        var to_date;*/

        init();
        function init() {


            UtilService.change_header($state.current.data.header_label);
            $scope.sel_status= -1;
            console.log('data passed>>' +$stateParams.order_id);
            /*sel_status = $stateParams.sel_status;
            sel_order = $stateParams.sel_order;
            from_date = $stateParams.from_date;
            to_date = $stateParams.to_date;*/


            ViewOrder($stateParams.order_id);
        }

        $scope.$watch('sel_OrdStatus', function () {



        });

        function ViewOrder(OrderId) {

            GlobalOrderId = OrderId;

            $scope.lblOrder_id = "Order # " + OrderId;
            $scope.dispute_orderNo = OrderId;

            $scope.en_dispute_orderNo = true;
            $scope.en_dispute_notes = true;

            orderService.get_order_data($http, $q, OrderId)
                .then(function (result) {
                    $scope.order_date=result.order_date;
                    $scope.sel_OrdStatus = result.status;
                    $scope.Purchased_From = result.store;
                    $scope.Cust_Name=result.Cust_Name;
                    //$scope.billing_address = result.billing_address;
                    $scope.email_id = result.email_id;
                    $scope.contact_number = result.contact_number;
                    $scope.bal_points = result.bal_points;
                    $scope.points = result.points;

                    $scope.egift_vou_amt = result.egift_vou_amt;
                    $scope.contact_number = result.contact_number;
                    $scope.bal_points = result.bal_points;
                    $scope.points = result.points;
                    $scope.address = result.billing_address;

                    if (result.egift_vou_amt > 0)
                        $scope.egift_voucher_no = result.egift_vou_no;
                    else
                        $scope.egift_voucher_no = '';

                    $scope.paid_amount  = result.paid_amount;

                    if (result.paid_amount >= 500 || result.paid_amount == 0) {
                        $scope.shipping_charges = "Free Rs 0.00";
                    } else {
                        $scope.shipping_charges = " Rs 99.00";
                    }

                    $scope.shipping_info = result.shipping_info;
                    $scope.courier_track_no = result.courier_track_no;
                    $scope.courier_track_link= result.courier_track_link;
                    $scope.shipping_date = result.shipping_date;
                    $scope.courier_cost = result.courier_cost;
                  // $scope.dispute_notes = result.dispute_notes;
                    $scope.dispute_notes = result.dispute_data.dispute_notes;

                    if (result.status != 5) {
                        $scope.send_email = 1;
                    }
                    else {
                        $scope.send_email = 0;
                    }
                    $scope.itemsOrderedList = result.cart_data;
                },
                function (error) {
                    //Display an error message
                    $scope.error = error;
                });



        }


        $scope.OrdStatus_update= function(){
            var UpdOrdStatus = $scope.sel_OrdStatus;
            $scope.Delay_Days = '';
            $scope.OrdUpdInfo = '';

            if (UpdOrdStatus == -1) {
                alert('Please Select An Order Status.');
                return false;
            }

            if (UpdOrdStatus == 5) {
                $scope.showOrderShipInfo = true;
            }
            else if (UpdOrdStatus == 6) {
                $scope.trNoOfDaysDelay = true;
                $scope.OrdUpdStatus = 'Delayed Information';
                $scope.OrdUpdCap = 'Enter Delayed Note';
                $scope.showOrderStatusUpdInfo = true;
            }
            else if (UpdOrdStatus == 7) {
                $scope.trNoOfDaysDelay = false;
                $scope.OrdUpdStatus = 'Cancellation Information';
                $scope.OrdUpdCap = 'Enter Cancellation Note';
                $scope.showOrderStatusUpdInfo = true;
            }
            else if (UpdOrdStatus == 13) {
                $scope.showDisputeInfo = true;
            }
            else {
                var update_OrderData = { order_id: GlobalOrderId, status: $scope.sel_OrdStatus };
                orderService.update_Order($http, $q,update_OrderData)
                    .then(function (result) {
                        alert('Record Updated Successfully.');
                    },
                    function (error) {
                        alert('Error');
                        $scope.error = error;
                    });

            }
        }

        $scope.AddCourierInfo = function(){
            var ddlOrdStatus = $scope.sel_OrdStatus;

            var txtShippingDate = $scope.shipping_date;
            if (txtShippingDate == null || txtShippingDate == "") {
                alert('Please Choose Shipping Date');
                return false;
            }


            var txtCourServProvider = $scope.shipping_info;
            if (txtCourServProvider == null || txtCourServProvider == "") {
                alert('Please Enter Courier Service Provider');
                return false;
            }

            var txtCourierTrackNo = $scope.courier_track_no;
            if (txtCourierTrackNo == null || txtCourierTrackNo == "") {
                alert('Please Enter Courier Track No');
                return false;
            }

            var txtCourierTrackLink = $scope.courier_track_link;
            if (txtCourierTrackLink == null || txtCourierTrackLink == "") {
                alert('Please Enter Courier Track Link');
                return false;
            }

            var txtCourierCost = $scope.courier_cost + '';
            if (txtCourierCost == null || txtCourierCost == "") {
                alert('Please Enter Courier Cost');
                return false;
            }

            var Send_Email_Criteria = 0;

            if ($scope.send_email == 1) {
                Send_Email_Criteria = 1;
            }

            var orderCourierInfo = {order_id: GlobalOrderId, status: ddlOrdStatus, shipping_info: txtCourServProvider, courier_track_no: txtCourierTrackNo, courier_track_link: txtCourierTrackLink, courier_cost: txtCourierCost, shipping_date: txtShippingDate, send_email: Send_Email_Criteria};
            orderService.update_Order($http, $q,orderCourierInfo)
                .then(function (result) {
                    alert('Record Updated Successfully.');
                },
                function (error) {
                    alert('Error');
                    $scope.error = error;
                });
        }

        $scope.CancelCourierInfo = function(){
            $scope.shipping_info = '';
            $scope.shipping_date = '';
            $scope.courier_cost = '';
            $scope.courier_track_no = '';
            $scope.courier_track_link = '';
            $scope.send_email = 0;
            $scope.showOrderShipInfo = false;
        }

        $scope.AddOrdStatNote = function(){
            var OrdStatusNote = $scope.OrdUpdInfo;
            var ddlOrdStatus = $scope.sel_OrdStatus;

            var txtDaysDelayed = $scope.Delay_Days;

            if (OrdStatusNote == '' || OrdStatusNote == null) {

                if (ddlOrdStatus == 5) {
                    alert('Please Enter Shipping Information');
                    return false;
                }
                else if (ddlOrdStatus == 6) {
                    alert('Please Enter Delayed Note');
                    return false;
                }
                else if (ddlOrdStatus == 7) {
                    alert('Please Enter Cancellation Note');
                    return false;
                }

            }

            if (ddlOrdStatus == 6) {
                if (txtDaysDelayed == '' || txtDaysDelayed == null) {
                    alert('Please Enter No. Of Days Delayed.');
                    return false;
                }
            }

            var OrdStatNoteData = {order_id: GlobalOrderId, Delay_Days: txtDaysDelayed, status: ddlOrdStatus, shipping_info: OrdStatusNote, delayed_note: OrdStatusNote, cancellation_note: OrdStatusNote };
            orderService.update_Order($http, $q,OrdStatNoteData)
                .then(function (result) {
                    $scope.showOrderStatusUpdInfo = false;
                    alert('Record Updated Successfully.');
                },
                function (error) {
                    alert('Error');
                    $scope.error = error;
                });
        }

        $scope.CancelOrdStatNote= function(){
            $scope.OrdUpdInfo = '';
            $scope.showOrderStatusUpdInfo = false;
        }


        $scope.AddDisputeInfo = function(){
            var ddlOrdStatus = $scope.sel_OrdStatus;

            var txtDisputeNotesAct = $scope.dispute_notes_act;
            if (txtDisputeNotesAct == null || txtDisputeNotesAct == "") {
                alert('Please Enter Dispute Notes');
                return false;
            }


            var PrevNote = $scope.dispute_notes;
            var CurrNote = $scope.dispute_notes_act;
            var DisputeNote = "";
            if (PrevNote == null || PrevNote == "") {
                DisputeNote = CurrNote;
            }
            else {
                DisputeNote = PrevNote + " ." + CurrNote;
            }
            $scope.dispute_notes = DisputeNote;

            var dispute = {

                dispute_notes:DisputeNote

            };

            //var DisputeInfoData = { order_id: GlobalOrderId, status: ddlOrdStatus, dispute_note: DisputeNote };
            var DisputeInfoData = { order_id: GlobalOrderId, status: ddlOrdStatus, dispute_data: dispute };

            orderService.update_Order($http, $q,DisputeInfoData)
                .then(function (result) {
                    alert('Record Updated Successfully.');
                },
                function (error) {
                    alert('Error');
                    $scope.error = error;
                });

        }

        $scope.CancelDisputeInfo = function(){
            $scope.dispute_notes_act = '';
            $scope.showDisputeInfo = false;
        }

        $scope.closeOrderShipInfoPanel = function(){
            $scope.showOrderShipInfo= false;
        }

        $scope.closeOrderStatusUpdInfoPanel = function(){
            $scope.showOrderStatusUpdInfo= false;
        }

        $scope.closeDisputeInfoPanel = function(){
            $scope.showDisputeInfo= false;
        }

        $scope.BackOrdView = function(){
            $state.go('main.orderview');
            //$window.history.back();
        }

    });