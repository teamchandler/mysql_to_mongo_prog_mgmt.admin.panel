/**
 * Created by DRIPL_1 on 09/08/2014.
 */

'use strict';

angular.module('annectosadmin')
    .controller('child_prod_mainCtrl', function ($scope,$rootScope,$location,$http,$q,localStorageService,child_prod_mainService,$state,UtilService) {
        $scope.awesomeThings = [
            'HTML5 Boilerplate',
            'AngularJS',
            'Karma'
        ];
        var ChildProdId = "";
        var user_info;

        init();
        function init(){

            UtilService.change_header($state.current.data.header_label);
            user_info=localStorageService.get("admin_user_info").user_name;
            pop_parent_prod();

            // $scope.divLoadBusy = true;
        }



        $scope.selectcolumn = function(row) {
            $scope.selectedRow = row;
        };


        $scope.$watch("sel_prod_parent",function(){

            if($scope.sel_prod_parent != null || $scope.sel_prod_parent != "" || typeof $scope.sel_prod_parent != "undefined" )
            {

            get_child_product();
            }
            //$scope.stock;
        })
        function pop_parent_prod(){

           // loading_add();
            // $scope.divLoadBusy = true;

            UtilService.show_loader();


            child_prod_mainService.get_parent_product($http,$q)
                .then(function (data) {

                    $scope.prod_parent=data;
                  //  loading_cut();

                    UtilService.hide_loader();


                },
                function () {
                    //Display an error message
                    $scope.error = error;

                });

        }

        function get_child_product(){

           // loading_add();

            UtilService.show_loader();

            var ddlParentProduct=$scope.sel_prod_parent;
            if(ddlParentProduct!="0"){
                child_prod_mainService.get_child_product($http,$q,ddlParentProduct)
                    .then(function (data) {
                        $scope.prod_child=data;
                        var prod_child = data;

                        localStorageService.set('child_prod_details',{});
                        localStorageService.add('child_prod_details',prod_child);

                        UtilService.hide_loader();
                    },
                    function () {
                        //Display an error message

                        UtilService.hide_loader();
                        $scope.error = error;

                    });

            }
            else{
                $scope.child_data.prod_sku="";
                $scope.child_data.prod_size="";
            }
        }

        var collection_name = "";

        function get_audit_data(collection_name,previous_data,new_data) {

            var log_info = {
                "app_id": "dalmia_admin_data",
                "secret": "a1844750-accf-451b-bf6f-0bc997b974cc",
                "db_name": "annectos_prod",
                "collection_name": collection_name,
                "field_name": "",
                "business_id": "dalmia_admin",
                "business_id_name": "dalmia",
                "old_data":previous_data,
                "new_data":new_data ,
                "user": user_info
            };

            return log_info;
        }


        function clear_data()
        {
            $scope.child_data.size = "";
            $scope.child_data.sku = "";
            $scope.child_data.stock = "";
            $scope.child_data.Express = 0 ;

        }


        $scope.add_child_product=function(){
           // if($scope.child_data!=undefined ||$scope.child_data!="" ||$scope.child_data!=null) {


             //   var ddlParentProduct = $scope.sel_prod_parent;


//                $scope.child_data.parent_prod = ddlParentProduct;


                if ($scope.sel_prod_parent == 0 || $scope.sel_prod_parent == null || typeof $scope.sel_prod_parent ===  "undefined") {
                    alert('Please Select Parent Product');
                    return false;
                }

               else if ($scope.child_data == 0 || $scope.child_data == null || typeof $scope.child_data ===  "undefined") {
                alert('Some required fields are missing');
                return false;
                }
              //  var txtChildProdSize = $scope.child_data.size;
                else  if ($scope.child_data.size == null || $scope.child_data.size == "" || typeof $scope.child_data.size ===  "undefined") {
                    alert('Please Enter Size');
                    return false;
                }
              //  var txtChildProdSKU = $scope.child_data.sku;
                else  if ($scope.child_data.sku == null || $scope.child_data.sku == "" ||  typeof  $scope.child_data.sku === "undefined") {
                    alert('Please Enter SKU');
                    return false;
                }
              // var txtChildProdStock = $scope.child_data.stock;
                else  if ($scope.child_data.stock == null || $scope.child_data.stock == "" || typeof $scope.child_data.stock ===  "undefined") {
                    alert('Please Enter Stock');
                    return false;
                }
              //  var EXP = "";
                else  if ($scope.child_data.Express == true) {
                   // EXP = 1;

                    $scope.child_data.Express = 1;
                }
                else {
                   // EXP = 0;

                    $scope.child_data.Express = 0;
                }
         //   }


            $scope.child_data.parent_prod = $scope.sel_prod_parent;



            child_prod_mainService.insert_child_product($http,$q,$scope.child_data)
                .then(function (data) {
                  //  $scope.child_data=data[0];
                    alert('Record Saved Successfully');
                    clear_data();
                    get_child_product();

                },
                function () {
                    //Display an error message
                    $scope.error = error;

                });

        }

        $scope.delete_child_prod=function(ChildProductId){
            var del_confirm_val = confirm("Are You Sure You Want To Delete?");
            if(del_confirm_val==true){

                 $scope.child_prod = {};
                $scope.child_prod.id = ChildProductId;


                child_prod_mainService.delete_child($http,$q,$scope.child_prod)
                    .then(function (data) {
                        alert('Record Deleted Successfully');
                        get_child_product();

                    },
                    function () {
                        //Display an error message
                        $scope.error = error;

                    });
          }
        }



        $scope.go_to_popup = function(child_id,stock,express)
        {

            $scope.stock = stock;
            if(express == "1")
            {
            $scope.express = true;
            }
            else{

                $scope.express = false;

            }
            $scope.child_popup=true;
            localStorageService.add('child_prod_id', child_id);
         //  $state.go('main.edit_child_prod');

        }

        $scope.close_pop_up = function()
        {
            $scope.child_popup=false;
            get_child_product();

        }

        $scope.cancel_data = function()
        {
            $scope.child_popup=false;
            $scope.child_data.stock = "";
            get_child_product();

        }


        $scope.edit_data = function()
        {
            $scope.child_data = {};

            ChildProdId = localStorageService.get('child_prod_id');
            $scope.child_data.id = ChildProdId;
            $scope.child_data.stock= $scope.stock;
             var express = $scope.express;
            if(express == true)
            {

                $scope.child_data.express = 1;
            }
            else
            {
                $scope.child_data.express = 0;


            }


            var collection_name = "company";

         //   var company_name = $scope.list_company.name;

            var previous_data =  localStorageService.get('child_prod_details');

            var new_data = $scope.list_company;

            var info_data = get_audit_data(collection_name,previous_data,new_data);

            child_prod_mainService.update_data($http,$q,$scope.child_data)
                .then(function (data) {


                    child_prod_mainService.insert_audit_data($http, $q,info_data)
                        .then(function (data) {

                        },
                        function () {
                            //Display an error message
                            $scope.error = error;

                        });

                    alert('Record Updated Successfully');
                   // get_child_product();
                    $state.go('main.child_prod_main');

                },
                function () {
                    //Display an error message
                    $scope.error = error;

                });




        }



    })
