/**
 * Created by developer7 on 9/10/2014.
 */

'use strict';

angular.module('annectosadmin')
    .controller('ordertrackingCtrl', function ($scope,$rootScope,$location,$http,$q,orderService,commonService,localStorageService,$state,UtilService) {


        var RootStart = "http://";
        var RootEnd = ".annectos.net";

        init();
        function init() {

            UtilService.change_header($state.current.data.header_label);
            PopCompanyUI();
            $scope.sel_order_status= 1;

        }





        function  PopCompanyUI() {

           // loading_add();

            UtilService.show_loader();

            commonService.postPopulateCompany($http,$q)

                .then(function (data) {
                    if(data.length>0){
                        data[0].name = "Select Company";
                    }
                    $scope.order_track_list = data;
                    $scope.sel_storetrack = 0;

                  //  loading_cut();

                    UtilService.hide_loader();

                },
                function () {
                    //Display an error message
                    $scope.error = error;

                });

        }

        $scope.check_order = function() {
            var OrdType = $scope.sel_order_status;

            if (OrdType == "1") {
                getOrderTrackRecord(OrdType);
            }
            else {

                getWillShipTrackRecord(OrdType);
            }
        }
        function getOrderTrackRecord(OrdTyp) {
            var OrdStore = $scope.sel_storetrack;
            if (OrdStore == 0) {
                OrdStore = null;
            }
            else {
                for(var i=0; i<$scope.order_track_list.length; i++){
                    if($scope.order_track_list[i].id == OrdStore){
                        OrdStore = $scope.order_track_list[i].name;
                        break;
                    }
                }
            }

            orderService.getOrderTrackRecord($http,$q,OrdStore,OrdTyp)
                .then(function (data) {
                    $scope.orderTrackList = data;
                },
                function () {
                    //Display an error message
                    $scope.error = error;

                });
        }

        function getWillShipTrackRecord(OrdTyp) {
            var OrdStore = $scope.sel_storetrack;
            if (OrdStore == 0) {

                OrdStore = null;
            }
            else {
                for(var i=0; i<$scope.order_track_list.length; i++){
                    if($scope.order_track_list[i].id == OrdStore){
                        OrdStore = $scope.order_track_list[i].name;
                        break;
                    }
                }
            }

            orderService.getWillShipTrackReport($http,$q,OrdStore,OrdTyp)
                .then(function (data) {
                    $scope.orderTrackList = data;
                },
                function () {
                    //Display an error message
                    $scope.error = error;

                });
        }

        $scope.SendTrackReport = function() {
            var OrdStore = $scope.sel_storetrack;
            if (OrdStore == 0) {
                OrdStore = null;
            }
            else {
                for(var i=0; i<$scope.order_track_list.length; i++){
                    if($scope.order_track_list[i].id == OrdStore){
                        OrdStore = $scope.order_track_list[i].name;
                        break;
                    }
                }
            }

            var OrdTyp = 1;

            orderService.getSendTrackReport($http,$q,OrdStore,OrdTyp)
                .then(function (data) {
                    alert('Report Send Successfully');
                },
                function () {
                    $scope.error = error;
                    alert('Error');
                });
        }

        $scope.will_ship_track_report = function(){
            var OrdStore = $scope.sel_storetrack;
            if (OrdStore == 0) {
                OrdStore = null;
            }
            else {
                for(var i=0; i<$scope.order_track_list.length; i++){
                    if($scope.order_track_list[i].id == OrdStore){
                        OrdStore = $scope.order_track_list[i].name;
                        break;
                    }
                }
            }

            var OrdTyp = 4;
            orderService.getWillShipReport($http,$q,OrdStore,OrdTyp)
                .then(function (data) {
                    alert('Report Send Successfully');
                },
                function (error) {
                    $scope.error = error;
                    alert('Error');
                });

        }

        $scope.WillShipReportAftMaxDays = function(){
            var OrdStore = $scope.sel_storetrack;
            if (OrdStore == 0) {
                OrdStore = null;
            }
            else {
                for(var i=0; i<$scope.order_track_list.length; i++){
                    if($scope.order_track_list[i].id == OrdStore){
                        OrdStore = $scope.order_track_list[i].name;
                        break;
                    }
                }
            }

            var OrdTyp = 4;

            orderService.getWillShipReportAfterMaxShip($http,$q,OrdStore,OrdTyp)
                .then(function (data) {
                    alert('Report Send Successfully');
                },
                function (error) {
                    $scope.error = error;
                    alert('Error');
                });

        }

    });