'use strict';

/**
 * @ngdoc function
 * @name annectosadminApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the annectosadminApp
 */
angular.module('annectosadmin')
  .controller('MainCtrl', function ($scope) {
    $scope.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });
