/**
 * Created by DRIPL_1 on 09/10/2014.
 */


/**
 * Created by DRIPL_1 on 09/05/2014.
 */

'use strict'

angular.module('annectosadmin')
    .controller('upload_img_prodCtrl', function ($scope,$rootScope,$location,$http,$q,upload_img_prodService,localStorageService,$stateParams,$state ,UtilService) {
        $scope.awesomeThings = [
            'HTML5 Boilerplate',
            'AngularJS',
            'Karma'
        ];


        var prod_id = $stateParams.product_id;
        var prod_name = $stateParams.product_name;

        var UploadType  = 0;

        $scope.name = prod_name;
        $scope.id = prod_id;

        $scope.prod_name_disable = true;
        $scope.prod_id_disable = true;
        var store_data;

        init();

        function init()
        {

            UtilService.change_header($state.current.data.header_label);
            PopImgVidAdUrls(prod_id);

        }

//        $scope.$watch('all_url', function () {
//            PopulateImg();
//        });
//
//
//        $scope.$watch('all_video_url', function () {
//            PopulateVideo();
//        });
        $scope.$watch('all_url',function(newValue, oldValue)
            {
                if(newValue !== oldValue)
                {
                    PopulateImg();
                }
            }

        );

        $scope.$watch('all_video_url',function(newValue, oldValue)
            {
                if(newValue !== oldValue)
                {
                    PopulateVideo();
                }
            }

        );


        function PopulateImgVidAd(result) {

            if (result[0].image_urls.length > 0) {

                $scope.img_url_list = result[0].image_urls;

                // PopulateImg();

            }
            if (result[0].video_urls.length > 0) {

                $scope.video_url_list =  result[0].video_urls;
            }
            if (result[0].ad_urls.length > 0) {

                $scope.ad_all_url = result[0].ad_urls;
            }
        }

        function PopulateImg()
        {
            UploadType = 1;

            var url = $scope.all_url;

            for(var i=0;i<store_data[0].image_urls.length;i++)
            {
                if(url == store_data[0].image_urls[i].display_name)
                {

                    $scope.display_name = store_data[0].image_urls[i].display_name;
                    $scope.link = store_data[0].image_urls[i].link;
                    $scope.thumb_link  = store_data[0].image_urls[i].thumb_link;
                    $scope.zoom_link = store_data[0].image_urls[i].zoom_link;

                }

            }

        }

        function PopulateVideo()
        {

            UploadType = 1;

            var v_url = $scope.all_video_url;

            for(var i=0;i<store_data[0].video_urls.length;i++)
            {

                if(v_url == store_data[0].video_urls[i].display_name)
                {

                    $scope.video_display_name = store_data[0].video_urls[i].display_name;
                    $scope.video_link = store_data[0].video_urls[i].link;
                    //   $scope.thumb_link  = store_data[0].video_urls[i].thumb_link;
                    //  $scope.zoom_link = store_data[0].video_urls[i].zoom_link;

                }


            }

        }

        function PopAdDtls() {

            UploadType = 1;
            var ddlAdUrls = $scope.all_url ;


            for (var i = 0; i < store_data[0].ad_urls.length; i++) {

                if (ddlAdUrls == store_data[0].ad_urls[i].href) {
                    $scope.href = store_data[0].ad_urls[i].href;
                    $scope.a_link = store_data[0].ad_urls[i].link;

                }

            }

        }

        function PopImgVidAdUrls(prod_id) {

            upload_img_prodService.get_img_url($http,$q,prod_id)
                .then(function (data) {
                    //  $scope.img_url_list=data;
                    PopulateImgVidAd(data);
                    store_data = data;


                },
                function () {
                    //Display an error message
                    $scope.error = error;

                });

        }
        $scope.add_more = function()
        {
            UploadType  = 0;
           // $scope.all_url = "";
            $scope.display_name = "";
            $scope.link = "";
            $scope.thumb_link = "";
            $scope.zoom_link = "";
           // $scope.all_video_url = "";
            $scope.video_display_name = "";
            $scope.video_link= "";
            $scope.href = "";
            $scope.a_link = "";

        }

        $scope.back_to_prod = function()
        {
            $state.go('main.prod_maintenance');

        }

        $scope.on_mouse = function()
        {
            this.hoverEdit = true;

        }


        $scope.off_mouse = function()
        {
            this.hoverEdit = false;

        }


        $scope.UploadImgVidLink = function () {


            if($scope.display_name == "" || $scope.display_name == null || typeof $scope.display_name === "undefined")
            {
                alert('Please Enter New URL');
                return false;

            }
            if($scope.link == "" || $scope.link == null || typeof $scope.link === "undefined")
            {
                alert('Please Enter Link');
                return false;

            }
            if($scope.thumb_link == "" || $scope.thumb_link == null || typeof $scope.thumb_link === "undefined")
            {
                alert('Please Enter Thumb Link');
                return false;

            }
            if($scope.zoom_link == "" || $scope.zoom_link == null || typeof $scope.zoom_link === "undefined")
            {
                alert('Please Enter Zoom Link');
                return false;

            }


            var ImageDisplayName = $scope.display_name;
            var ImageLink = $scope.link;
            var ImageThumbLink = $scope.thumb_link;
            var ImageZoomLink =  $scope.zoom_link;

            var VideoDisplayName =  $scope.video_display_name;

            var VideoLink =  $scope.video_link;
            var Adhref =  $scope.href;

            var AdLink =   $scope.a_link;



            var image_urls = {
                display_name: ImageDisplayName,
                link: ImageLink,
                thumb_link: ImageThumbLink,
                zoom_link: ImageZoomLink
            }
            var video_urls = {
                display_name: VideoDisplayName,
                link: VideoLink
            }
            var ad_urls = {
                href: Adhref,
                link: AdLink
            }

            var ImageArray = [];
            var VideoArray = [];
            var AdArray = [];
            ImageArray[0] = image_urls;
            VideoArray[0] = video_urls;
            AdArray[0] = ad_urls;


            var Category_Id = $scope.id;

            var data = { id: Category_Id, image_urls: ImageArray, video_urls: VideoArray, ad_urls: AdArray };

            if ( UploadType == 0) {

                upload_img_prodService.upload_cat_img_vid($http,$q,data)
                    .then(function (data) {

                        alert('Uploaded Successfully!');

                        PopImgVidAdUrls(prod_id);

                    },
                    function () {
                        //Display an error message
                        $scope.error = error;

                    });



//                 $.support.cors = true;
//                 $.ajax({
//
//                     url: rooturl + '/category/admin/upload_cat_imgvid/',
//                     type: 'POST',
//                     data: { id: Category_Id, image_urls: ImageArray, video_urls: VideoArray, ad_urls: AdArray },
//                     dataType: 'json',
//                     success: function (result) {
//                         loadingcut();
//                         alert('Uploaded Successfully!');
//                        // PopImgVidAdUrls(1, $('#ddlCategory :selected').val());
//                     },
//                     error: function (req, status, errorObj) {
//                         alert('Error');
//                     }
//                 });
            }
            else {


                upload_img_prodService.update_cat_img_vid($http,$q,data)
                    .then(function (data) {

                        alert('Updated Successfully!');

                        PopImgVidAdUrls(prod_id);




                    },
                    function () {
                        //Display an error message
                        $scope.error = error;

                    });






//                 $.support.cors = true;
//                 $.ajax({
//
//                     url: rooturl + '/category/admin/update_cat_imgvid/',
//                     type: 'POST',
//                     data: { id: Category_Id, image_urls: ImageArray, video_urls: VideoArray, ad_urls: AdArray },
//                     dataType: 'json',
//                     success: function (result) {
//                         loadingcut();
//                         alert('Updated Successfully!');
//                      //   PopImgVidAdUrls(1, $('#ddlCategory :selected').val());
//                     },
//                     error: function (req, status, errorObj) {
//                         alert('Error');
//                     }
//                 });


            }





        }

//            if (From == 1) {
//                $.support.cors = true;
//                $.ajax({
//                    url: rooturl + '/category/get_cat_imgvidurls/{cat_id}',
//                    type: 'GET',
//                    data: { cat_id: ID },
//                    dataType: 'json',
//                    success: function (result) {
//                        PopulateImgVidAd(result);
//                        Store_result = result;
//                    },
//                    error: function (req, status, errorObj) {
//                        alert('Error');
//                    }
//                });








    })

